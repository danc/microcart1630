﻿angular.module('app.controllers', []);
angular.module('app.directives', []);
angular.module('app.filters', []);

angular.module("app", [
		'app.controllers',
		'app.directives',
		'app.filters',
		'ngRoute'
	])
	.config(['$routeProvider', function ($routeProvider) {
		$routeProvider.when('/', {
			templateUrl: './Angular/Pages/Home/Home.html',
			controller: 'Home.Controller'
		});

		$routeProvider.when('/About', {
			templateUrl: './Angular/Pages/About/About.html'
		});

		$routeProvider.when('/Documents', {
			templateUrl: './Angular/Pages/Documents/Documents.html',
			controller: 'Documents.Controller'
		});

		$routeProvider.when('/Videos', {
			templateUrl: './Angular/Pages/Videos/Videos.html'
		});


		$routeProvider.otherwise({
			redirectTo: '/'
		});
	}]);
