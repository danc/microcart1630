function[] = compfilt(filename)
%compfilt(filename) takes a log file and analyzes the average and maximum
%roll and pitch with various gyro constants
%
%filename = name of file to use, 'example.txt'

%initialization
c = zeros(1,1001);
ap = zeros(1,1001);
mp = zeros(1,1001);
ar = zeros(1,1001);
mr = zeros(1,1001);
%read the log file
[dataT,~,dataN]=readdata(filename);
Time = dataT(:,1);
AccelR_Angle = dataN(:,10);
AccelP_Angle = dataN(:,11);
Gyro_r_vel = dataN(:,12);
Gyro_p_vel = dataN(:,13);
CamRoll_A = dataN(:,20);
CamPitch_A = dataN(:,21);
%more initialization
Gyro_p_pos=zeros(1,length(dataN));
Gyro_r_pos=zeros(1,length(dataN));
pitch_code=zeros(1,length(dataN));
roll_code=zeros(1,length(dataN));
deltap=0.00;
averagep = 0.00;
maximump = 0.00;
deltar=0.00;
averager = 0.00;
maximumr = 0.00;
t=1;
%begin analyzing gyro constants
for n = 0:.001:1    %gyro constant
    x = round((n*1000)+1);
    c(x) = n;   %x axis for plot
    for S = 1:length(dataN)     %analyze for a single gyro constant
        Gyro_r_pos(S) = roll_code(t)+(Gyro_r_vel(S)*(Time(S)-Time(t)));
        Gyro_p_pos(S) = pitch_code(t)+(Gyro_p_vel(S)*(Time(S)-Time(t)));
        if(S==1)
            pitch_code(S)=AccelP_Angle(S);
            roll_code(S)=AccelR_Angle(S);
        else
            pitch_code(S) = n*(Gyro_p_pos(S))+((1-n)*AccelP_Angle(S));
            roll_code(S) = n*(Gyro_r_pos(S))+((1-n)*AccelR_Angle(S));
        end
        deltap = CamPitch_A(S)-pitch_code(S);   %error
        deltar = CamRoll_A(S)-pitch_code(S);
        averagep = averagep + abs(deltap/double(length(dataN)));
        averager = averager + abs(deltar/double(length(dataN)));
        if(abs(deltap)>maximump)
            maximump = abs(deltap);
        end
        if(abs(deltar)>maximumr)
            maximumr = abs(deltar);
        end
        t=S;
    end
    %store and reset
    ap(x)=averagep;
    ar(x)=averager;
    mp(x)=maximump;
    mr(x)=maximumr;
    averager=0.00;
    averagep=0.00;
    maximumr=0.00;
    maximump=0.00;
    t=1;
end
%plots
figure();
plot(c,ap)
hold on
plot(c,mp)
legend('avg pitch','max pitch');
xlabel('gyro constant');
ylabel('radians');
hold off;
figure();
plot(c,ar)
hold on
plot(c,mr)
legend('avg roll','max roll');
xlabel('gyro constant');
ylabel('radians');
hold off;
end