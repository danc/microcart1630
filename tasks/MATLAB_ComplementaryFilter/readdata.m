function[dataT, dataID, dataN] = readdata(filename)
    f = fopen(filename);
    header = textscan(f,'%s',8,'Delimiter',{' ','\n','\r','\t'});
    sampsize = cell2mat(textscan(f,'%d',1,'Delimiter',{' ','\n','\r','\t'}));
    axis_names = textscan(f,'%s',25,'Delimiter',{' ','\n','\r','\t'});
    axis_units = textscan(f,'%s',25,'Delimiter',{' ','\n','\r','\t'});
    dataT=zeros(sampsize,2);
    dataID=zeros(sampsize,1);
    dataN=zeros(sampsize,22);
    for S = 1:sampsize
        dataT(S,:) = cell2mat(textscan(f,'%.04f',2,'Delimiter',{' ','\n','\r','\t'}));
        dataID(S,:) = cell2mat(textscan(f,'%d',1,'Delimiter',{' ','\n','\r','\t'}));
        dataN(S,:) = cell2mat(textscan(f,'%.04f',22,'Delimiter',{' ','\n','\r','\t'}));
    end
    fclose(f);
end