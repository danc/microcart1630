function [ averagep, maximump, averager, maximumr ] = plotquaddata( filename , a )
%UNTITLED3 Summary of this function goes here
%   Uses quadlog file to plot the error for using a complementary filter
%   filename = name of file
%   a = constant for gyroscope data
    f = fopen(filename);
    header = textscan(f,'%s',8,'Delimiter',{' ','\n','\r','\t'});
    sampsize = cell2mat(textscan(f,'%d',1,'Delimiter',{' ','\n','\r','\t'}));
    axis_names = textscan(f,'%s',25,'Delimiter',{' ','\n','\r','\t'});
    axis_units = textscan(f,'%s',25,'Delimiter',{' ','\n','\r','\t'});
    Time=zeros(1,sampsize);
    AccelR_Angle=zeros(1,sampsize);
    AccelP_Angle=zeros(1,sampsize);
    Gyro_r_vel=zeros(1,sampsize);
    Gyro_p_vel=zeros(1,sampsize);
    Gyro_p_pos=zeros(1,sampsize);
    CamRoll_A=zeros(1,sampsize);
    CamPitch_A=zeros(1,sampsize);
    Gyro_r_pos=zeros(1,sampsize);
    pitch_code=zeros(1,sampsize);
    pitch_quad=zeros(1,sampsize);
    deltap=zeros(1,sampsize);
    averagep = 0.00;
    maximump = 0;
    roll_code=zeros(1,sampsize);
    roll_quad=zeros(1,sampsize);
    deltar=zeros(1,sampsize);
    averager = 0.00;
    maximumr = 0;
    t=1;
    for S = 1:sampsize
        dataT = cell2mat(textscan(f,'%.04f',2,'Delimiter',{' ','\n','\r','\t'}));
        dataID = cell2mat(textscan(f,'%d',1,'Delimiter',{' ','\n','\r','\t'}));
        dataN = cell2mat(textscan(f,'%.04f',22,'Delimiter',{' ','\n','\r','\t'}));
        Time(S) = dataT(1);
        AccelR_Angle(S) = dataN(10);
        AccelP_Angle(S) = dataN(11);
        Gyro_r_vel(S) = dataN(12);
        Gyro_p_vel(S) = dataN(13);
        %Gyro_y_vel(S) = dataN(14);
        roll_quad(S) = dataN(15);
        pitch_quad(S) = dataN(16);
        CamRoll_A(S) = dataN(20);
        CamPitch_A(S) = dataN(21);
        %CamYaw_A(S) = dataN(22);
        Gyro_r_pos(S) = roll_code(t)+(Gyro_r_vel(S)*(Time(S)-Time(t)));
        Gyro_p_pos(S) = pitch_code(t)+(Gyro_p_vel(S)*(Time(S)-Time(t)));
        if(S==1)
            pitch_code=AccelP_Angle(S);
            roll_code=AccelR_Angle(S);
        else
            pitch_code(S) = a*(Gyro_p_pos(S))+((1-a)*AccelP_Angle(S));
            roll_code(S) = a*(Gyro_r_pos(S))+((1-a)*AccelR_Angle(S));
        end
        deltap(S) = CamPitch_A(S)-pitch_code(S);
        deltar(S) = CamRoll_A(S)-pitch_code(S);
        averagep = averagep + abs(deltap(S)/double(sampsize));
        averager = averager + abs(deltar(S)/double(sampsize));
        if(abs(deltap(S))>maximump)
            maximump = abs(deltap(S));
        end
        if(abs(deltar(S))>maximumr)
            maximumr = abs(deltar(S));
        end
        t=S;
    end
    %subplot(2,2,1);
    %figure()
    %plot(Time,pitch_code)
    %hold on
    %plot(Time,CamPitch_A)
    %plot(Time,deltap)
    %plot(Time,Gyro_p_pos)
    %plot(Time,AccelP_Angle)
    %plot(Time,pitch_quad)
    %legend('code pitch','camera pitch','gyro','acc pitch','quad pitch');
    %legend('error');
    %xlabel('time (s)');
    %ylabel('radians');
    %hold off;
    %figure()
    %plot(Time,roll_code)
    %hold on
    %plot(Time,CamRoll_A)
    %plot(Time,deltar)
    %plot(Time,Gyro_r_pos)
    %plot(Time,AccelR_Angle)
    %plot(Time,roll_quad)
    %legend('code roll','camera roll','gyro','acc roll','quad roll');
    %legend('error');
    %xlabel('time (s)');
    %ylabel('radians');
    %hold off;
    fclose(f);
end