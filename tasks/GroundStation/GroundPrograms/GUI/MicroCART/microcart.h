#ifndef MICROCART_H
#define MICROCART_H

#include <QMainWindow>

#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"

namespace Ui {
class MicroCART;
}

class MicroCART : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MicroCART(QWidget *parent = 0);
    ~MicroCART();

private slots:
    void on_startButton_clicked();
    void update();
    void updateGUI();

private:
    Ui::MicroCART *ui;
    vrpn_Connection *connection;
    vrpn_Tracker_Remote *tracker;
    QTimer *timer;

};

#endif // MICROCART_H
