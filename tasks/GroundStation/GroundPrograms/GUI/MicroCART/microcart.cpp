#include <stdio.h>
#include <sstream>
#include <QTimer>
#include <QtDeclarative/QDeclarativeView>
#include "microcart.h"
#include "ui_microcart.h"

typedef struct {
    float north; //north
    float east; //east
    float alt; //altitude

    float roll;
    float pitch;
    float yaw;
} vrpnInfo_t;

// boo will be passed as a void pointer. It can be used for anything, but it isn't necessary.
// In this example, it is not used.
vrpnInfo_t *boo;

// Tells whether a callback has been made or not
int dataReceived = 0;

// Tests if the update function is called
float testValue1 = 0;

// Tests if the updateGUI function is called
float testValue2 = 0;

// x, y, z positions and time from VRPN
double xxx = 0.0;
double yyy = 0.0;
double zzz = 0.0;
int timez = 0;

static void VRPN_CALLBACK handle_pos (void *boo, const vrpn_TRACKERCB t) {
    // The coordinate system is based off the orientation of the sensor board.
    // SparkFun MPU9150

    // +Y is East
    yyy = t.pos[0];

    // +X is North
    xxx = -t.pos[1];

    // +Z is Down
    zzz = t.pos[2];

    // Time
    timez = t.msg_time.tv_sec;

    //
    dataReceived++;
}

MicroCART::MicroCART(QWidget *parent) : QMainWindow(parent), ui(new Ui::MicroCART) {
    ui->setupUi(this);

    /*QDeclarativeView *qmlView = new QDeclarativeView;
    qmlView->setSource(QUrl::fromLocalFile("Visuals/main.qml"));

    QWidget *container = QWidget::create(qmlView, this);
    container->setMinimumSize(400, 300);
    container->setMaximumSize(400, 300);
    container->setFocusPolicy(Qt::TabFocus);

    ui->widget->set(qmlView);*/

    connection = vrpn_get_connection_by_name("192.168.0.120:3883");
    tracker = new vrpn_Tracker_Remote("UAV", connection);
    tracker->register_change_handler((void *)boo, handle_pos);

    usleep(10000);

    if(connection->mainloop() == 0) {
        ui->textEdit_7->setText("Connected");
    }
    tracker->mainloop();
}

MicroCART::~MicroCART() {
    delete ui;
}

void MicroCART::on_startButton_clicked() {
    // Ideally, we would check if dataReceived is true here.
    // For the sake of seeing that the update functions run, it is not part of the program.

    ui->startButton->setEnabled(false);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateGUI()));
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(5);
}

void MicroCART::update() {
    connection->mainloop();
    tracker->mainloop();
    testValue1++;
}

void MicroCART::updateGUI() {
    QString displayValue;

    //dataReceived
    displayValue = displayValue.setNum(dataReceived);
    ui->textEdit->setText(displayValue);

    //update
    displayValue = displayValue.setNum(testValue1, 'g', 6);
    ui->textEdit_2->setText(displayValue);

    //updateGUI
    displayValue = displayValue.setNum(testValue2, 'g', 6);
    ui->textEdit_3->setText(displayValue);
    testValue2++;


    //x, y, z Position
    displayValue = displayValue.setNum(xxx, 'g', 6);
    ui->textEdit_4->setText(displayValue);

    displayValue = displayValue.setNum(yyy, 'g', 6);
    ui->textEdit_5->setText(displayValue);

    displayValue = displayValue.setNum(zzz, 'g', 6);
    ui->textEdit_6->setText(displayValue);

    //Time
    displayValue = displayValue.setNum(timez);
    ui->textEdit_8->setText(displayValue);

    connection->mainloop();
    tracker->mainloop();
}
