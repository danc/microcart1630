#ifndef VISUALS_H
#define VISUALS_H

#include <QtOpenGL/QGLWidget>

class visuals : public QGLWidget
{
    Q_OBJECT
public:
    explicit visuals(QWidget *parent = 0);
    void initalizeGL();
    void paintGL();
    void resizeGL();

signals:
    
public slots:
    
};

#endif // VISUALS_H
