#-------------------------------------------------
#
# Project created by QtCreator 2015-10-28T21:31:03
#
#-------------------------------------------------

QT       += core gui

TARGET = MicroCART_May1630
TEMPLATE = app


SOURCES += main.cpp\
        microcart.cpp

HEADERS  += microcart.h

FORMS    += microcart.ui


unix|win32: LIBS += -lvrpn

OTHER_FILES +=

