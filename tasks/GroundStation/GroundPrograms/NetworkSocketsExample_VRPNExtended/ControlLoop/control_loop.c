#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "../microcart_cli.h"

FILE *testlog;

void signal_callback_handler(int signal) {
	fclose(testlog);
	exit(0);
}

int main(int argc, char *argvp[]) {
	char buffer[256];
	int n, sockfd = 0;

	signal(SIGINT, signal_callback_handler);
	
	testlog = fopen("control_loop_test_log.txt", "w+");
	fprintf(testlog, "testing socket \n");
	
	sockfd = init_socket();
	while(1) {
		// Clear the buffer and read the message from the socket (from the server)
		bzero(buffer, 256);
		n = read(sockfd, buffer, 255);
		
		// If there was an error reading from the socket, throw an error
		if(n < 0) {
			//error("ERROR reading from socket");
			fprintf(testlog, "ERROR reading from socket\n");
		}
		if(strcmp(buffer, "") == 0) {
			break;
		}
		fprintf(testlog, "%s\n", buffer);
	}
	
	
	fclose(testlog);
	return 0;
}

int init_socket() {
	int sockfd, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;

	// Check command format
	//char buffer[256];
	//if(argc < 3) {
	//	fprintf(stderr, "usage %s hostname port\n", argv[0]);
	//	exit(0);
	//}

	// Get the port number from the command arguments
	//portno = atoi(argv[2]);
	// Create socket
	fprintf(testlog, "Connection to socket.\n");
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	// If socket not created, throw error
	if(sockfd < 0) { 
		//error("ERROR opening socket");
		fprintf(testlog, "ERROR opening socket\n");
	}

	// Get the server name from the command arguments
	server = gethostbyname("127.0.0.1");
	// If the server name is not valid, throw error.
	if (server == NULL) {
		fprintf(testlog, "ERROR, no such host\n");
		exit(0);
	}

	// Set memory to zero and initialize the server information
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(SOCKET_PORT_2);

	fprintf(testlog, "Attempting to connect...\n");
	while(connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		fprintf(testlog, "ERROR connecting");
		exit(0);
	}
	fprintf(testlog, "Connected!\n");
	
	return sockfd;
}
