#include <stdio.h> /* Standard input/output definitions */
#include <stdlib.h>
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <errno.h> /* Error number definitions */
#include <ctype.h>
#include <fcntl.h> /* File control definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
#include <pthread.h>
#include <signal.h>

#include "vrpnhandler.h"
#include "../microcart_cli.h"

int socketfd = 0, newsockfd = 0;
int portNo = 0;
volatile vrpnInfo_t mostRecentUpdate;
volatile int gotFirstUpdate = 0;

// The rate at which packets are sent to the quad, in Hz
const int PACKET_RATE_PER_SEC = 50;

void* sendCamUpdateFunc(void* param);
//void releasePort();


void signal_callback_handler(int signal) {
	printf("VRPN Handler: Caught signal %d.\n", signal);
	//printf("Exiting the program safely. Releasing port %d.\n", SOCKET_PORT);

	printf("VRPN: freeing %d and %d\n", newsockfd, socketfd);

	close(newsockfd);
	close(socketfd);
	
	
	printf("VRPN: %d and %d freed\n", newsockfd, socketfd);
	exit(signal);
}

int main(int argc, char *argv[]) {	
	socketfd = 0;
	newsockfd = 0;
	
	
	
	init_socket(&socketfd, &newsockfd);

	signal(SIGQUIT, signal_callback_handler);	// Ctrl + backslash (\)
	signal(SIGINT, signal_callback_handler); 	// Ctrl + C
	//signal(SIGSEGV, signal_callback_handler);	// Segmentation fault

	// initialize vrpn
	vrpn_init("192.168.0.120:3883", handle_pos);
	
	// create a thread to send cam updates via uart
	pthread_t sendCamUpdateThread;
	if(pthread_create(&sendCamUpdateThread, NULL, sendCamUpdateFunc, NULL)) {
		fprintf(stderr, "Error creating thread\n");
		return 1;
	}
	
	// go into vrpn loop, this uses the main thread completely 
	vrpn_go();
	
	close(newsockfd);
	close(socketfd);
	
	return 0;
}


void* sendCamUpdateFunc(void* param) {
	// wait for the first update to come in
	while(!gotFirstUpdate) {
		usleep(10000);
	}
	
	// send off PACKET_RATE_PER_SEC packets every second
	while(1) {
		sendVrpnPacket(mostRecentUpdate);
		
		// sleep until the next packet
		usleep(1e6 / PACKET_RATE_PER_SEC);
	}
}

// initialize network socket
int init_socket(int *sockfd, int *newsockfd) {
	//int sockfd, newsockfd;
	socklen_t clilen;
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	int n, yes = 1;

	// Check the command format
	/*if(argc < 2) {
		fprintf(stderr, "ERROR, no port provided\n");
		exit(1);
	}*/

	// Get the port number from the command arguments
	//portno = atoi(argv[1]);
	// Create the socket
	*sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(*sockfd < 0) {
		error("ERROR opening socket");
	}

	// Initialize the server information
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(SOCKET_PORT);

	// Allow program to reuse TCP port if in use
	if (setsockopt(*sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		perror("setsockopt");
		exit(1);
	}

	// Bind to the socket
	if(bind(*sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		error("ERROR on binding");
	}

	// Start listening for connections on the socket
	// Backlog queue (number of connections waiting) is limited to 5
	listen(*sockfd, 5);

	
	//printf("Waiting for client to connect.\n");
	// Blocks the process until a client connects
	clilen = sizeof(cli_addr);
	*newsockfd = accept(*sockfd, (struct sockaddr *) &cli_addr, &clilen);
	if(*newsockfd < 0) {
		error("ERROR on accept");
	}
	
	return *newsockfd;
}



// this function is called by VRPN whenever a camera update comes in
void VRPN_CALLBACK handle_pos(void* unused, const vrpn_TRACKERCB t) {
	// the tracker info comes in as a quaternion, but we want euler angles
	q_vec_type euler;
	q_to_euler(euler, t.quat);
	
	// euler is [yaw, pitch, roll]
	// t.pos is [x, y, z]

	/*
	* These are the x-y-z values reported from the camera system.
	* Due to its configuration, CURRENTLY positive X points east,
	* positive Y points south, and positive Z points into the ground.
	*/
	float x = (float) t.pos[0];
	float y = (float) t.pos[1];
	float z = (float) t.pos[2];

	/*
	* Therefore, north is -y, east is x, and altitude is -z
	*/
	mostRecentUpdate.north = -y;
	mostRecentUpdate.east  = x;
	mostRecentUpdate.alt   = -z;

	/*
	 * Roll, pitch, and yaw come directly as the camera system says
	 */
	mostRecentUpdate.roll  = (float) euler[2];
	mostRecentUpdate.pitch = (float) euler[1];
	mostRecentUpdate.yaw   = (float) euler[0];

	// set the flag that says we can start sending packets
	gotFirstUpdate = 1;
}

void sendVrpnPacket(vrpnInfo_t info) {
	int n;
	
	char *packet = (char *)malloc(sizeof(char) * 256);
	
	sprintf(
		packet,
		"[n:%.2f e:%.2f a:%.2f r:%.2f p:%.2f y:%.2f]\n",
		info.north,
		info.east,
		info.alt,
		info.roll,
		info.pitch,
		info.yaw
	);
	
	n = write(newsockfd, packet, sizeof(char) * 47);
	if(n < 0) {
		error("ERROR writing to socket");
	}
}

