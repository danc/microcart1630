#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MSGSZ 1024

struct msgbuf {
	long mtype;    /* message type */
	char mtext[MSGSZ]; /* message text of length MSGSZ */
}

int main() {
	int msgflg; /* message flags for the operation */
	struct msgbuf *msgp; /* pointer to the message buffer */
	int msgsz; /* message size */
	long msgtyp; /* desired message type */
	int msqid; /* message queue ID to be used */
	int maxmsgsz = 1024;

	key_t key; /* key to be passed to msgget() */ 
	//int msgflg /* msgflg to be passed to msgget() */ 
	//int msqid; /* return value from msgget() */ 


	// Initialize the Message Queue
	key = 1;
	msgflg = 7;
	if ((msqid = msgget(key, msgflg)) == -1) {
		perror("msgget: msgget failed");
		exit(1);
	} else {
		(void) fprintf(stderr, "msgget succeeded");
	}

	// Create message struct
	msgp = (struct msgbuf *)malloc((unsigned)(sizeof(struct msgbuf) - sizeof(msgp->mtext + maxmsgsz)));
	if (msgp == NULL) {
		(void) fprintf(stderr, "msgop: %s %d byte messages.\n",
		"could not allocate message buffer for", maxmsgsz);
		exit(1);
	}


	// Send message to message queue
	msgsz = MSGSZ;
	//msgflg = 7;
	if (msgsnd(msqid, msgp, msgsz, msgflg) == -1) {
		perror("msgop: msgsnd failed");
	}

	// Receive message from message queue
	//msgsz = ...
	msgtyp = first_on_queue;
	//msgflg = ...
	if (rtrn = msgrcv(msqid, msgp, msgsz, msgtyp, msgflg) == -1) {
		perror("msgop: msgrcv failed");
	}
	
	printf("%s\n", msgp->mtext);
	
	return 0;
}

