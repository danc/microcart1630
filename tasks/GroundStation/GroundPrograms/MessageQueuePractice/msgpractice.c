#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <pthread.h>

#define MSGSZ 1
#define MSGTYP 1
#define MSGFLG 00004000


void* msgserver_snd(void* param);
void* msgreceiver_rcv(void* param);

int msqid = 0;

struct msgbuf {
	long mtype;    /* message type */
	char mtext[1024]; /* message text of length MSGSZ */
};

int main() {
	int msgflg = IPC_CREAT;
	msqid = msgget(ftok("/tmp", '1'), msgflg);
	
	if(msqid == -1) {
		printf("msgget failed.\n");
		exit(1);
	} else {
		printf("msgget succeeded. msqid: %d\n", msqid);
	}
	
	pthread_t msgserver;
	pthread_create(&msgserver, NULL, msgserver_snd, NULL);
	
	usleep(1000);
	
	pthread_t msgreceiver;
	pthread_create(&msgreceiver, NULL, msgreceiver_rcv, NULL);
	
	while(1);
	
	return 0;
}

void* msgserver_snd(void* param) {
	int i, rtrn;
	char a[1024];
	struct msgbuf		*msgp;
	
	
	msgp = (struct msgbuf *)malloc((unsigned)(sizeof(struct msgbuf) - sizeof(msgp->mtext)  + MSGSZ));
		
	
	for(i = 0; i < 100; i++) {
		printf("Writing %d to the message queue\n", i);
		
		msgp->mtype = MSGTYP;
		sprintf(msgp->mtext, "The value is: %d\n", i);
		printf("%s\n", msgp->mtext);
		
		rtrn = msgsnd(msqid, msgp, MSGSZ, MSGFLG);
		if (rtrn == -1) {
			perror("msgop: msgsnd failed");
		} else {
			(void) fprintf(stderr, "msgop: msgsnd returned %d\n", rtrn);
		}
		
		usleep(10);
	}
}

void* msgreceiver_rcv(void* param) {
	sleep(5);
	int i, rtrn;
	struct msgbuf		*msgp;
	msgp = (struct msgbuf *)malloc((unsigned)(sizeof(struct msgbuf) - sizeof(msgp->mtext)  + MSGSZ));
	rtrn = msgrcv(msqid, msgp, MSGSZ, MSGTYP, MSGFLG);
	
	(void) fprintf(stderr, "msgop: %s %d\n", "msgrcv returned", rtrn);
	(void) fprintf(stderr, "msgp->mtype = %ld\n", msgp->mtype);
	(void) fprintf(stderr, "msgp->mtext is: \"");
		printf("%s\n", msgp->mtext);
	/*for (i = 0; i < 3; i++) {
		(void) fputc(msgp->mtext[i], stderr);
	}*/
	(void) fprintf(stderr, "\"\n\n");
	
	
	/*for(i = 0; i < 100; ) {
		rtrn = msgrcv(msqid, msgp, MSGSZ, MSGTYP, MSGFLG);
		if (rtrn == -1) {
			perror("msgop: msgrcv failed");
			printf("%d\n", i);
		} else {
			printf("%d\n", i);
			(void) fprintf(stderr, "msgop: %s %d\n", "msgrcv returned", rtrn);
			(void) fprintf(stderr, "msgp->mtype = %ld\n", msgp->mtype);
			(void) fprintf(stderr, "msgp->mtext is: \"");
			for (i = 0; i < strlen(msgp->mtext); i++) {
				(void) fputc(msgp->mtext[i], stderr);
			}
			(void) fprintf(stderr, "\"\n\n");
			i++;
		}
	}*/
}

