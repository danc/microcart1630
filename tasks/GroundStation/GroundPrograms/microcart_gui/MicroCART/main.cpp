#include "mainwindow.h"
#include <QApplication>
#include <signal.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    a.exec();

    raise(SIGINT);
    kill(w.connect_zybo, SIGINT);
    kill(w.control_loop, SIGINT);

    return 0;
}
