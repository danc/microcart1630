#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

typedef struct {
    int packetId;

    float x_pos; //north
    float y_pos; //east
    float z_pos; //altitude

    float roll;
    float pitch;
    float yaw;
} vrpnInfo_t;

//----------------------------------------------------------------------------------------------
//     index||     0    |     1    |      2      |  3 & 4 |      5 & 6       |  7+  |   end    |
//---------------------------------------------------------------------------------------------|
// msg param|| beg char | msg type | msg subtype | msg id | data len (bytes) | data | checksum |
//-------------------------------------------------------------------------------------------- |
//     bytes||     1    |     1    |      1      |    2   |        2         | var  |    1     |
//----------------------------------------------------------------------------------------------
typedef struct {
    char begin_char;
    char msg_type;
    char msg_subtype;
    int msg_id;
    int data_len;
    char checksum;
} metadata_t;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    pid_t connect_zybo, control_loop;

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void update_vrpn();
    void on_send_command_button_clicked();

    void on_start_button_clicked();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
    int test_counter;
    vrpnInfo_t vInfo;
};

#endif // MAINWINDOW_H
