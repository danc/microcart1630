#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/wait.h>

#define SOCKET_PORT_2 50010
#define SOCKET_PORT_4 50015

/*
 * This function will initialize a socket used mainly for writing to the socket.
 * A separate socket will be set up in init_socket_client to read from the socket.
 *
 * RETURNS: The new socket file descriptor
 */
int init_socket_server(int portNumber, int socket_fd) {
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;
    int yes = 1, newsocket_fd;


    // Create the socket
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0) {
        perror("ERROR opening socket");
    }

    // Initialize the server information
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portNumber);


    // Allow program to reuse TCP port if in use
    if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
        perror("getsockopt");
    }


    // Bind to the socket
    if(bind(socket_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        fprintf(stderr, "On port: %d,\n", portNumber);
        perror("ERROR on binding");
    }


    // Start listening for connections on the socket
    // Backlog queue (number of connections waiting) is limited to 5
    listen(socket_fd, 5);


    // Blocks the process until a client connects
    clilen = sizeof(cli_addr);
    newsocket_fd = accept(socket_fd, (struct sockaddr *) &cli_addr, &clilen);
    if(newsocket_fd < 0) {
        perror("ERROR on accept");
        fprintf(stderr, "ERROR on accept");
    }

    return newsocket_fd;
}

// returns the length of the data in bytes (datalen from packet) and filles data and metadata with the packet information
// use as follows:
//
//		packet is the entire packet message (formatted)
//		data is an unallocated (char *) (pass it to this function as &data)
//		meta_data is a pointer to an instance of metadata_t
//
int parse_packet(unsigned char * packet, unsigned char ** data, metadata_t * meta_data)
{
    //----------------------------------------------------------------------------------------------
    //     index||     0    |     1    |      2      |  3 & 4 |      5 & 6       |  7+  |   end    |
    //---------------------------------------------------------------------------------------------|
    // msg param|| beg char | msg type | msg subtype | msg id | data len (bytes) | data | checksum |
    //-------------------------------------------------------------------------------------------- |
    //     bytes||     1    |     1    |      1      |    2   |        2         | var  |    1     |
    //----------------------------------------------------------------------------------------------

    // first byte must be the begin char
    if(packet[0] != 0xBE)
        return -1;

    // receive metadata
    meta_data->begin_char = packet[0];
    meta_data->msg_type = packet[1];
    meta_data->msg_subtype = packet[2];
    meta_data->msg_id = (packet[4] << 8) | (packet[3]);
    meta_data->data_len = (packet[6] << 8) | (packet[5]);
    meta_data->checksum = packet[7+meta_data->data_len];

    int i;

    // receive data
    *data = (unsigned char *) malloc(meta_data->data_len);
    for(i = 0; i < meta_data->data_len; i++)
    {
        (*data)[i] = packet[7+i];
    }

    // calculate checksum
    char data_checksum = 0;
    for(i = 0; i < meta_data->data_len + 7; i++)
    {
        data_checksum ^= packet[i];
    }

    // compare checksum
    if(meta_data->checksum != data_checksum)
        fprintf(stderr, "Checksums did not match (Quadlog): 0x%02x\t0x%02x\n", meta_data->checksum, data_checksum);

    return meta_data->data_len;
}

float getFloat(unsigned char* str, int pos) {
    union {
        float f;
        int i;
    } x;
    x.i = ((str[pos+3] << 24) | (str[pos+2] << 16) | (str[pos+1] << 8) | (str[pos]));
    return x.f;
}

int getInt(unsigned char* str, int pos) {
    int i = ((str[pos+3] << 24) | (str[pos+2] << 16) | (str[pos+1] << 8) | (str[pos]));
    return i;
}

int control_loop_cmds2 = 0, control_loop_vrpn2 = 0, control_loop_cmds = 0, control_loop_vrpn = 0;

void *thread_receive_vrpn_data(void *pVrpn) {
    unsigned char buffer[255];
    vrpnInfo_t * vInfo = (vrpnInfo_t *) pVrpn;

    while(1) {
        // Clear the buffer and read the message from the socket (from the server)
        memset(buffer, 0, 255);

        // If there was an error reading from the socket, throw an error
        if(read(control_loop_vrpn, buffer, 255) <= 0) {
            fprintf(stderr, "CLI VRPN: ERROR reading from socket.\n");
            fflush(stderr);
            continue;
        }

        unsigned char * vrpn_data;
        metadata_t metadata;

        parse_packet(buffer, &vrpn_data, &metadata);

        // store the vrpn data into the vrpn struct so the CLI/GUI can display it
        vInfo->packetId = getInt(vrpn_data, 0);
        vInfo->x_pos = getFloat(vrpn_data, 4);
        vInfo->y_pos = getFloat(vrpn_data, 8);
        vInfo->z_pos = getFloat(vrpn_data, 12);
        vInfo->roll = getFloat(vrpn_data, 16);
        vInfo->pitch = getFloat(vrpn_data, 20);
        vInfo->yaw = getFloat(vrpn_data, 24);

        //display the vrpn data
        /*fprintf(stderr, "[id: %d n:%.2f e:%.2f a:%.2f r:%.2f p:%.2f y:%.2f]\n",
            vInfo->packetId,
            vInfo->x_pos,
            vInfo->y_pos,
            vInfo->z_pos,
            vInfo->roll,
            vInfo->pitch,
            vInfo->yaw
        );*/
    }

    pthread_exit(NULL);
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_start_button_clicked()
{
    test_counter = 0;
    this->ui->x_position_display->setText("Button Pushed!");

    // Establish connection to quad
    connect_zybo = fork();
    if(connect_zybo == 0) {
            execl("/local/ucart/Desktop/MicroCART_May1630/tasks/GroundStation/GroundPrograms/MicroCART_GroundStation_Modular/connect_zybo_bt.sh", "connect_zybo_bt.sh", NULL);
    }

    sleep(5);

    fprintf(stderr, "GUI: Connecting to zybo (please be patient)...\n");
    //wait for connect zybo to run (times out at 10 seconds)
    int failed_connection = 0;
    clock_t start = clock();
    while(access("/dev/rfcomm0", F_OK) != 0)
    {
        //restart connect zybo if it already failed
        int status;
        pid_t alive = waitpid(connect_zybo, &status, WNOHANG);
        if(alive != 0)
        {
            // Establish connection to quad
            connect_zybo = fork();
            if(connect_zybo == 0) {
                    execl("/local/ucart/Desktop/MicroCART_May1630/tasks/GroundStation/GroundPrograms/MicroCART_GroundStation_Modular/connect_zybo_bt.sh", "connect_zybo_bt.sh", NULL);
            }
        }

        // if timeout has been reached then break
        if((((float)(clock() - start)) / CLOCKS_PER_SEC) >= 10.0)
        {
            failed_connection = 1;
            break;
        }
    }

    // notify user of failed connection
    if(failed_connection)
    {
        fprintf(stderr, "GUI: Connection to quad failed.");
        kill(connect_zybo, SIGINT);
    }

    // control loop execution
    control_loop = fork();
    if(control_loop == 0)
        execl("/local/ucart/Desktop/MicroCART_May1630/tasks/GroundStation/GroundPrograms/MicroCART_GroundStation_Modular/ControlLoop/main", "main",NULL);

    //set up sockets

    // Set up socket for communication with control_loop for user commands
    if((control_loop_cmds = init_socket_server(SOCKET_PORT_2, control_loop_cmds2)) <= 0)
        fprintf(stderr, "CLI: Failed to connect to control_loop on port %d\n", SOCKET_PORT_2);
    else
        fprintf(stderr, "CLI: Connected to control_loop socket on port %d\n", SOCKET_PORT_2);

    // Set up socket for communication with control_loop for vrpn data receiving
    if((control_loop_vrpn = init_socket_server(SOCKET_PORT_4, control_loop_vrpn2)) <= 0)
        fprintf(stderr, "CLI: Failed to connect to control_loop on port %d\n", SOCKET_PORT_4);
    else
        fprintf(stderr, "CLI: Connected to control_loop socket on port %d\n", SOCKET_PORT_4);

    pthread_t thread;
    // Retrieve VRPN data from the control loop
    fprintf(stderr, "CLI: Starting VRPN receiving thread...\n");
    pthread_create(&thread, NULL, thread_receive_vrpn_data, &vInfo);
    fprintf(stderr, "CLI: Thread VRPN receiving started.\n");

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update_vrpn()));
    timer->start(10);
}

void MainWindow::on_send_command_button_clicked()
{
    this->ui->y_position_display->setText(this->ui->command_line->text());
}

void MainWindow::update_vrpn() {
    test_counter++;
    char msg[100] = {};
    sprintf(msg, "%.2f", this->vInfo.z_pos);
    this->ui->z_position_display->setText(msg);
}
