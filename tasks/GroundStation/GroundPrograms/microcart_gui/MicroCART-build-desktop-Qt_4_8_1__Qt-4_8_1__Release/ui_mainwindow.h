/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Fri Apr 15 01:45:38 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTextBrowser>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGroupBox *quadlog_group;
    QPushButton *send_command_button;
    QLineEdit *command_line;
    QTextBrowser *quadlog_display;
    QGroupBox *vrpn_group;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLineEdit *x_position_display;
    QLineEdit *y_position_display;
    QLineEdit *z_position_display;
    QLineEdit *pitch_display;
    QLineEdit *roll_display;
    QLineEdit *yaw_display;
    QPushButton *start_button;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(618, 432);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        quadlog_group = new QGroupBox(centralWidget);
        quadlog_group->setObjectName(QString::fromUtf8("quadlog_group"));
        quadlog_group->setGeometry(QRect(250, 10, 361, 351));
        send_command_button = new QPushButton(quadlog_group);
        send_command_button->setObjectName(QString::fromUtf8("send_command_button"));
        send_command_button->setGeometry(QRect(280, 320, 75, 23));
        command_line = new QLineEdit(quadlog_group);
        command_line->setObjectName(QString::fromUtf8("command_line"));
        command_line->setGeometry(QRect(10, 320, 261, 20));
        quadlog_display = new QTextBrowser(quadlog_group);
        quadlog_display->setObjectName(QString::fromUtf8("quadlog_display"));
        quadlog_display->setGeometry(QRect(10, 21, 341, 291));
        vrpn_group = new QGroupBox(centralWidget);
        vrpn_group->setObjectName(QString::fromUtf8("vrpn_group"));
        vrpn_group->setGeometry(QRect(20, 10, 201, 261));
        label = new QLabel(vrpn_group);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 30, 47, 13));
        label_2 = new QLabel(vrpn_group);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 60, 47, 13));
        label_3 = new QLabel(vrpn_group);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 90, 47, 13));
        label_4 = new QLabel(vrpn_group);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 120, 47, 13));
        label_5 = new QLabel(vrpn_group);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(10, 150, 47, 13));
        label_6 = new QLabel(vrpn_group);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 180, 47, 13));
        x_position_display = new QLineEdit(vrpn_group);
        x_position_display->setObjectName(QString::fromUtf8("x_position_display"));
        x_position_display->setGeometry(QRect(70, 30, 113, 20));
        y_position_display = new QLineEdit(vrpn_group);
        y_position_display->setObjectName(QString::fromUtf8("y_position_display"));
        y_position_display->setGeometry(QRect(70, 60, 113, 20));
        z_position_display = new QLineEdit(vrpn_group);
        z_position_display->setObjectName(QString::fromUtf8("z_position_display"));
        z_position_display->setGeometry(QRect(70, 90, 113, 20));
        pitch_display = new QLineEdit(vrpn_group);
        pitch_display->setObjectName(QString::fromUtf8("pitch_display"));
        pitch_display->setGeometry(QRect(70, 120, 113, 20));
        roll_display = new QLineEdit(vrpn_group);
        roll_display->setObjectName(QString::fromUtf8("roll_display"));
        roll_display->setGeometry(QRect(70, 150, 113, 20));
        yaw_display = new QLineEdit(vrpn_group);
        yaw_display->setObjectName(QString::fromUtf8("yaw_display"));
        yaw_display->setGeometry(QRect(70, 180, 113, 20));
        start_button = new QPushButton(centralWidget);
        start_button->setObjectName(QString::fromUtf8("start_button"));
        start_button->setGeometry(QRect(20, 320, 181, 23));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 618, 25));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        quadlog_group->setTitle(QApplication::translate("MainWindow", "Quadlog Real Time", 0, QApplication::UnicodeUTF8));
        send_command_button->setText(QApplication::translate("MainWindow", "Send", 0, QApplication::UnicodeUTF8));
        vrpn_group->setTitle(QApplication::translate("MainWindow", "VRPN Real Time", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "x position", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "y position", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "z position", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "pitch", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "roll", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "yaw", 0, QApplication::UnicodeUTF8));
        start_button->setText(QApplication::translate("MainWindow", "Start", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
