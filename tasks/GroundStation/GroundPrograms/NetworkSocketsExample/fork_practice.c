
#include <stdio.h>
#include <unistd.h>
#include <signal.h>


int main() {
	pid_t child;
	child = fork();

	if(child == 0) {
		printf("This is the child: %d\n", getpid());
		kill(getpid(), SIGTERM);
	} else {
		printf("This is the parent: %d\n", getpid());
		while(1);
	}

	return 0;
}
