/*
The purpose of this program is to simplify the process for connecting to the MicroCART quadcopter via Bluetooth.
This program will ultimately be the middle-man between the VRPN data and the program which communicates with the quadcopter.
There will be communication such that:
	a.) The VRPN Handler program will act as a server of VRPN data via a network socket.
		 This program will act as a client and receive the VRPN data.

	b.) This program will act as a server of VRPN Data and User Input to the Ground Station oftware
		 The ground station program will act as a client and receive the information sent from this program.

Coincidentally, this program, "microcart_cli", will
	1.) Start the bash script to connect the Bluetooth to the quadcopter. (connect_zybo_bt.sh)
	2.) Start the VRPN Handler program once the script has successfully connected.
	3.) Start the Ground Station program, which communicates directly with 
	4.) (Optional) Start the quadlog program.

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "microcart_cli.h"

int sockfd, returnToMenu = 0;
pid_t vrpnhandler;

void signal_callback_handler(int signal) {
	printf("MicroCART: Quitting vrpnhandler safely. Freeing socket file descriptor. Signal: %d\n", signal);
	
	kill(vrpnhandler, SIGINT);
	
	printf("Successfully killed vrpnhandler.\n");
	
	if(sockfd != 0) {
		close(sockfd);
		sockfd = 0;
	}
	
	returnToMenu = 1;
	printf("Returnin to menu.\n");
	//exit(signal);
}

void signal_callback_handler2(int signal) {
	printf("MicroCART: Quitting safely. Freeing socket file descriptor. Signal: %d\n", signal);
	
	kill(vrpnhandler, SIGINT);
	printf("Successfully killed vrpnhandler.\n");
	printf("Quitting MicroCART Program.\n");
	
	if(sockfd != 0) {
		close(sockfd);
		sockfd = 0;
	}

	exit(signal);
}

int main(int argc, char *argvp[]) {
	char buffer[256];
	int n, i = 0;
	char a = 0;
	sockfd = 0;
	vrpnhandler = 0;
	
	signal(SIGINT, signal_callback_handler);
	signal(SIGQUIT, signal_callback_handler2);
	//signal(SIGSEGV, signal_callback_handler);
	
	// Start vrpnhandler process
	vrpnhandler = fork();
	if(vrpnhandler == 0) {
		execv("./VRPN/vrpnhandler", NULL);
	}
	
	sockfd = init_socket();
	printf("%d\n", sockfd);
	// Retrieve VRPN data until the process is killed
	while(returnToMenu == 0) {
		// Clear the buffer and read the message from the socket (from the server)
		bzero(buffer, 256);
		n = read(sockfd, buffer, 255);
		// If there was an error reading from the socket, throw an error
		if(n < 0) {
			error("ERROR reading from socket");
		}
		if(strcmp(buffer, "") == 0) {
			break;
		}
		printf("%s\n", buffer);
	}
	//printf("Exiting the MicroCART Program.\n");
	
	while(1) {
		printf("Boo yah! This works. Now, Ctrl+\\ to end the program\n");
		usleep(1000000);
	}

	
	
	// Close the socket if not already closed
	if(sockfd != 0) {
		close(sockfd);
	}
	return 0;
}


int init_socket() {
	int sockfd, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;

	// Check command format
	//char buffer[256];
	//if(argc < 3) {
	//	fprintf(stderr, "usage %s hostname port\n", argv[0]);
	//	exit(0);
	//}

	// Get the port number from the command arguments
	//portno = atoi(argv[2]);
	// Create socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	// If socket not created, throw error
	if(sockfd < 0) { 
		error("ERROR opening socket");
	}

	// Get the server name from the command arguments
	server = gethostbyname("127.0.0.1");
	// If the server name is not valid, throw error.
	if (server == NULL) {
		fprintf(stderr, "ERROR, no such host\n");
		exit(0);
	}

	// Set memory to zero and initialize the server information
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(SOCKET_PORT);

	printf("Attempting to connect...\n");
	while(connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		//error("ERROR connecting");
	}
	printf("Connected!\n");
	
	return sockfd;
}

