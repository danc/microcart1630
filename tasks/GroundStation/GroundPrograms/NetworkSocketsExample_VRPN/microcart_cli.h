#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define SOCKET_PORT 50003


int init_socket();

void error(const char *msg) {
	perror(msg);
	exit(1);
}
