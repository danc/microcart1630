// use this function for reading a packet in the format agreed on
// updated with fixed variable naming errors

char * read_packet()
{
	char buf[2000] = {};
	
	//----------------------------------------------------------------------------------------------
	//     index||     0    |     1    |      2      |  3 & 4 |      5 & 6       |  7+  |   end    |
	//---------------------------------------------------------------------------------------------|
	// msg param|| beg char | msg type | msg subtype | msg id | data len (bytes) | data | checksum |
	//-------------------------------------------------------------------------------------------- |
	//     bytes||     1    |     1    |      1      |    2   |        2         | var  |    1     |
	//----------------------------------------------------------------------------------------------	

	// read the meta data
	uartRead(buf, 7);

	if(buf[0] != 0xBE)
		return NULL;

	// data length bytes 5 and 6
	int datalen = (buf[6] << 8) | buf[5];
	
	int i;
	char * data = malloc(datalen + 1);
	char data_checksum = 0;

	// receive data and calculate checksum
	for(i = 0; i < datalen; i++)
	{
		data[i] = buf[7+i];
		data_checksum ^= data[i];
	}
	
	// receive network checksum
	char packet_checksum = buf[7+datalen];

	// compare checksum
	if(packet_checksum != data_checksum)
		fprintf(stderr, "Checksums did not match (Quadlog)\n");

	return data;
}
