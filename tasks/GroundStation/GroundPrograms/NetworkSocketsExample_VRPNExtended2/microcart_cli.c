/*
The purpose of this program is to simplify the process for connecting to the MicroCART quadcopter via Bluetooth.
This program will ultimately be the middle-man between the VRPN data and the program which communicates with the quadcopter.
There will be communication such that:
	a.) The VRPN Handler program will act as a server of VRPN data via a network socket.
		 This program will act as a client and receive the VRPN data.

	b.) This program will act as a server of VRPN Data and User Input to the Ground Station oftware
		 The ground station program will act as a client and receive the information sent from this program.

Coincidentally, this program, "microcart_cli", will
	1.) Start the bash script to connect the Bluetooth to the quadcopter. (connect_zybo_bt.sh)
	2.) Start the VRPN Handler program once the script has successfully connected.
	3.) Start the Ground Station program, which communicates directly with 
	4.) (Optional) Start the control loop program.

*/

#include "microcart_cli.h"

int socket_quadlog;
int socket_vrpnhandler;
int socket_control_loop = 0, newsocket_control_loop = 0;

pthread_mutex_t socket_lock;

void *thread_forward_vrpn_data(void *thread_id) {
	unsigned char buffer[255];
	int n;
	
	pthread_mutex_lock(&socket_lock);
	write(newsocket_control_loop, "hi", sizeof(char) * 3);
	pthread_mutex_unlock(&socket_lock);

	while(1) {
		// Clear the buffer and read the message from the socket (from the server)
		bzero(buffer, 255);
		n = read(socket_vrpnhandler, buffer, 255);

		// If there was an error reading from the socket, throw an error
		if(n < 0) {
			fprintf(stderr, "ERROR reading from socket.\n");
			fflush(stderr);
			break;
		}

		// Write to the socket so that control_loop can retrieve the buffer.
		pthread_mutex_lock(&socket_lock);
		n = write(newsocket_control_loop, buffer, sizeof(char) * 255);
		pthread_mutex_unlock(&socket_lock);
		if(n < 0) {
			fprintf(stderr, "ERROR writing to socket 2");
			fflush(stderr);
			break;
		}
	}

	pthread_exit(NULL);
}

int main(int argc, char *argvp[]) {
	int i = 0, thread_vrpn_data = 0;
	char buffer[256];
	
	pthread_t thread;
	
	pid_t quadlog = 0;
	pid_t vrpnhandler = 0;
	pid_t control_loop = 0;
	socket_quadlog = 0;
	socket_vrpnhandler = 0;
	socket_control_loop = 0;
	newsocket_control_loop = 0;
	if (pthread_mutex_init(&socket_lock, NULL) != 0) {
		fprintf(stderr, "Failed to initialize mutex.\n");
		return 1;
	}

	// Start quadlog process
	quadlog = fork();
	if(quadlog == 0) {
		execv("./Quadlog/quadlog", NULL);
	}
	
	// Set up socket for communication with vrpnhandler
	socket_quadlog = init_socket_client(SOCKET_PORT_3);
	fprintf(stderr, "Connected to quadlog socket on port %d\n", SOCKET_PORT_3);

	// Start vrpnhandler process
	vrpnhandler = fork();
	if(vrpnhandler == 0) {
		execv("./VRPN/vrpnhandler", NULL);
	}
	
	// Set up socket for communication with vrpnhandler
	socket_vrpnhandler = init_socket_client(SOCKET_PORT_1);
	fprintf(stderr, "Connected to vrpnhandler socket on port %d\n", SOCKET_PORT_1);

	// Start control_loop process
	control_loop = fork();
	if(control_loop == 0) {
		execv("./ControlLoop/control_loop", NULL);
	}

	// Set up socket for communication with control_loop
	newsocket_control_loop = init_socket_server(SOCKET_PORT_2, socket_control_loop);
	//fcntl(newsocket_control_loop, F_SETFL, O_NONBLOCK);
	fprintf(stderr, "Connected to control_loop socket on port %d\n", SOCKET_PORT_2);

	// Retrieve VRPN data until the process is killed
	fprintf(stderr, "Starting VRPN forwarding thread...\n");
	thread_vrpn_data = pthread_create(&thread, NULL, thread_forward_vrpn_data, NULL);
	fprintf(stderr, "Thread VRPN forwarding started.\n");
	
	// Wait to display prompt to user
	// (this is not necessary, but gives VRPN a chance to get started)
	usleep(1000000);

	char userCommand[CMD_MAX_LENGTH];
	char response[CMD_MAX_LENGTH];
	int n;
	userCommand[0] = 0;
	fprintf(stderr, "$microcart> ");
	while(fgets(userCommand, sizeof(userCommand), stdin) != NULL) {
		userCommand[strlen(userCommand) - 1] = 0;
		fprintf(stderr, "This was the command: %s\n", userCommand);
		fflush(stderr);
		
		// Write the command to the control_loop socket
		pthread_mutex_lock(&socket_lock);
		n = write(newsocket_control_loop, userCommand, sizeof(char) * strlen(userCommand));
		if(n < 0) {
			fprintf(stderr, "ERROR writing to socket\n");
		}
		pthread_mutex_unlock(&socket_lock);


		userCommand[0] = 0;

		// Read the response from the quad received in quadlog
		read(socket_quadlog, response, sizeof(char) * strlen(response));
		if(n < 0) {
			fprintf(stderr, "ERROR reading from socket\n");
		}
		fprintf(stderr, "Response: %s\n", response);
		
		fprintf(stderr, "$microcart> ");
	}
	fprintf(stderr, "Exiting the MicroCART Program. (2)\n");

	return 0;
}

