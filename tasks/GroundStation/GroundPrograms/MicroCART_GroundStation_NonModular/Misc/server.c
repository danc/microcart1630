/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

void error(const char *msg) {
	perror(msg);
	exit(1);
}

int main(int argc, char *argv[]) {
	int sockfd, newsockfd, portno, i = 1;
	socklen_t clilen;
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	int n;

	// Check the command format
	if(argc < 2) {
		fprintf(stderr, "ERROR, no port provided\n");
		exit(1);
	}

	// Get the port number from the command arguments
	portno = atoi(argv[1]);
	// Create the socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0) {
		error("ERROR opening socket");
	}

	// Initialize the server information
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	// Bind to the socket
	if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		error("ERROR on binding");
	}

	// Start listening for connections on the socket
	// Backlog queue (number of connections waiting) is limited to 5
	listen(sockfd, 5);

	// Blocks the process until a client connects
	clilen = sizeof(cli_addr);
	newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
	if(newsockfd < 0) {
		error("ERROR on accept");
	}

	while(i == 1) {
		// Clear the buffer and read from the socket
		bzero(buffer,256);
		n = read(newsockfd, buffer, 255);
		if(n < 0) {
			error("ERROR reading from socket");
		}

		if(strncmp(buffer, "quit", 4) == 0) {
			// Acknowledge receipt of the message back to the client
			printf("Shutting down the server.\n");
			n = write(newsockfd, "Shutting down the server.", 25);
			if(n < 0) {
				error("ERROR writing to socket");
			}
			i = 0;
		} else {
			// Acknowledge receipt of the message back to the client
			printf("Here is the message: %s\n", buffer);
			n = write(newsockfd, "I got your message", 18);
			if(n < 0) {
				error("ERROR writing to socket");
			}
		}
	}


	close(newsockfd);
	close(sockfd);

	return 0; 
}
