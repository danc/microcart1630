#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

void error(const char *msg) {
	perror(msg);
	exit(0);
}

int main(int argc, char *argv[]) {
	int sockfd, portno, n, i = 1;
	struct sockaddr_in serv_addr;
	struct hostent *server;

	// Check command format
	char buffer[256];
	if(argc < 3) {
		fprintf(stderr, "usage %s hostname port\n", argv[0]);
		exit(0);
	}

	// Get the port number from the command arguments
	portno = atoi(argv[2]);
	// Create socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	// If socket not created, throw error
	if(sockfd < 0) { 
		error("ERROR opening socket");
	}

	// Get the server name from the command arguments
	server = gethostbyname(argv[1]);
	// If the server name is not valid, throw error.
	if (server == NULL) {
		fprintf(stderr, "ERROR, no such host\n");
		exit(0);
	}

	// Set memory to zero and initialize the server information
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(portno);

	if(connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		error("ERROR connecting");
	}


	while(i == 1) {
		// Write the message to the socket
		printf("Please enter the message: ");
		bzero(buffer, 256);
		fgets(buffer, 255, stdin);
		if(strncmp(buffer, "quit", 4) == 0) {
			i = 0;
		}
		n = write(sockfd, buffer, strlen(buffer));
		// If unable to write to socket, throw error
		if(n < 0) {
			error("ERROR writing to socket");
		}

		// Clear the buffer and read the message from the socket (from the server)
		bzero(buffer, 256);
		n = read(sockfd, buffer, 255);
		// If there was an error reading from the socket, throw an error
		if(n < 0) {
			error("ERROR reading from socket");
		}
		printf("%s\n", buffer);
	}


	// Close the socket
	close(sockfd);

	return 0;
}
