#include <stdio.h> /* Standard input/output definitions */
#include <stdlib.h>
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <errno.h> /* Error number definitions */
#include <ctype.h>
#include <fcntl.h> /* File control definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
#include <pthread.h>
#include <signal.h>

#include "vrpnhandler.h"

FILE *vrpntestlog;
int newsockfd;

volatile vrpnInfo_t mostRecentUpdate;
volatile int gotFirstUpdate = 0;

// The rate at which packets are sent to the quad, in Hz
const int PACKET_RATE_PER_SEC = 10;

void* sendCamUpdateFunc(void* param);

int main(int argc, char *argv[]) {
	int socketfd = 0;
	newsockfd = 0;

	// Set up VRPN Log file
	vrpntestlog = fopen("logs/vrpn_test_log.txt", "w+");
	fprintf(vrpntestlog, "testing vrpn \n");
	fflush(vrpntestlog);
	
	// Set up socket for sending VRPN data
	newsockfd = init_socket_server(SOCKET_PORT_1, socketfd);
	fprintf(vrpntestlog, "Initialized socket on port %d.\n", SOCKET_PORT_1);

	// initialize vrpn
	vrpn_init("192.168.0.120:3883", handle_pos);
	
	// create a thread to send cam updates via uart
	pthread_t sendCamUpdateThread;
	if(pthread_create(&sendCamUpdateThread, NULL, sendCamUpdateFunc, NULL)) {
		fprintf(vrpntestlog, "Error creating thread\n");
		return 1;
	}
	
	// go into vrpn loop, this uses the main thread completely 
	vrpn_go();
	
	close(newsockfd);
	close(socketfd);
	
	return 0;
}

void* sendCamUpdateFunc(void* param) {
	// wait for the first update to come in
	while(!gotFirstUpdate) {
		usleep(10000);
	}
	
	// send off PACKET_RATE_PER_SEC packets every second
	while(1) {
		sendVrpnPacket(mostRecentUpdate);
		
		// sleep until the next packet
		usleep(1e6 / PACKET_RATE_PER_SEC);
	}
}

// this function is called by VRPN whenever a camera update comes in
void VRPN_CALLBACK handle_pos(void* unused, const vrpn_TRACKERCB t) {
	// the tracker info comes in as a quaternion, but we want euler angles
	q_vec_type euler;
	q_to_euler(euler, t.quat);
	
	// euler is [yaw, pitch, roll]
	// t.pos is [x, y, z]

	/*
	* These are the x-y-z values reported from the camera system.
	* Due to its configuration, CURRENTLY positive X points east,
	* positive Y points south, and positive Z points into the ground.
	*/
	float x = (float) t.pos[0];
	float y = (float) t.pos[1];
	float z = (float) t.pos[2];

	/*
	* This is the ID of the VRPN Packet to be sent. This can be checked to
	* see whether duplicate packets are being received by the quadcopter.
	*/
	mostRecentUpdate.packetId++;

	/*
	* Therefore, north is -y, east is x, and altitude is -z
	*/
	mostRecentUpdate.north = -y;
	mostRecentUpdate.east  = x;
	mostRecentUpdate.alt   = -z;

	/*
	 * Roll, pitch, and yaw come directly as the camera system says
	 */
	mostRecentUpdate.roll  = (float) euler[2];
	mostRecentUpdate.pitch = (float) euler[1];
	mostRecentUpdate.yaw   = (float) euler[0];

	// set the flag that says we can start sending packets
	gotFirstUpdate = 1;
}

void sendVrpnPacket(vrpnInfo_t info) {
	int pSize = sizeof(info) + 3;
	int n;
	unsigned char packet[pSize];
	packet[0] = PACKET_START_BYTE;
	packet[1] = 'U'; // U for vrpn camera update, C for command
	memcpy(&packet[2], &info, sizeof(info));
	packet[pSize - 1] = PACKET_END_BYTE;
	
	/*char *packet2 = (char *)malloc(sizeof(char) * 256);
	sprintf(
		packet2,
		"[id: %d n:%.2f e:%.2f a:%.2f r:%.2f p:%.2f y:%.2f]\n",
		info.packetId,
		info.north,
		info.east,
		info.alt,
		info.roll,
		info.pitch,
		info.yaw
	);
	fprintf(vrpntestlog, "%s", packet2);
	fflush(vrpntestlog);*/

	// file descriptor is invalid or closed (not sure if this actually works)
	if(fcntl(newsockfd, F_GETFL) >= 0 && errno != EBADF) {
		n = write(newsockfd, packet, pSize);
		if(n < 0) {
			error("vrpnhandler: ERROR writing to socket");
		}
	}
}

