#include <stdio.h>		/* Standard input/output definitions */
#include <string.h>		/* String function definitions */
#include <unistd.h>		/* UNIX standard function definitions */
#include <fcntl.h>		/* File control definitions */
#include <errno.h>		/* Error number definitions */
#include <ctype.h>
#include <termios.h>	/* POSIX terminal control definitions */

#include "../microcart_cli.h"
#include "../uartutils.h"

int uart_fd;
FILE *quadlog_test;

int main(int argc, char *argv[]) {
	int socketfd = 0;
	int newsockfd = 0;

	// Open the log file
	quadlog_test = fopen("logs/quadlog_test.txt", "w+");

	// Set up socket for sending quad responses to microcart_cli
	newsockfd = init_socket_server(SOCKET_PORT_3, socketfd);
	fprintf(quadlog_test, "Initialized socket on port %d.\n", SOCKET_PORT_3);

	fprintf(quadlog_test, "Starting...\r\n");
	int ok = uartInit("/dev/rfcomm0");
	if(!ok) { 
		fprintf(quadlog_test, "Error connecting.. Aborting.\r\n"); 
		return 1; 
	}
	fprintf(quadlog_test, "Connected OK\r\n");
	fprintf(quadlog_test, "Begin log\r\n\r\n");
	fflush(quadlog_test);

	char c;
	int i;
int n;
	char buffer[1024];
	while(1) {
		// read char
		uartRead(&c, 1);
				
		if (isprint(c) || isspace(c)) {
			fprintf(quadlog_test, "%c", c);
			fflush(quadlog_test);
		}

		// Need to write back the received data to the main function
		// ...
		buffer[i] = c;
		if(buffer[i] == '+') {
			buffer[i] = 0;
			i = 0;
			// Print the received buffer to the log
			fprintf(quadlog_test, "\n---BBLAHHHH %s----\n", buffer);
			fflush(quadlog_test);

			// Send the response back to the CLI to be displayed to the user
			n = write(newsockfd, buffer, sizeof(buffer));
			if(n < 0) {
				fprintf(quadlog_test, "ERROR writing to socket\n");
			}

			buffer[0] = 0;
		} else {
			i++;
		}
	}

	return 0;
}
