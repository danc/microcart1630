#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>		/* File control definitions */
#include <errno.h>		/* Error number definitions */
#include <ctype.h>
#include <termios.h>	/* POSIX terminal control definitions */
#include <pthread.h>

#include "../microcart_cli.h"
#include "../uartutils.h"

FILE *testlog, *quadlog_test;
int sockfd = 0;
pthread_mutex_t socket_lock;
pthread_mutex_t uart_lock;

//#define  CMD_MAX_LENGTH  256

float getFloat(unsigned char* str, int pos) {
	union {
		float f;
		int i;
	} x;
	x.i = ((str[pos+3] << 24) | (str[pos+2] << 16) | (str[pos+1] << 8) | (str[pos]));
	return x.f;
}

int getInt(unsigned char* str, int pos) {
	int i = ((str[pos+3] << 24) | (str[pos+2] << 16) | (str[pos+1] << 8) | (str[pos]));
	return i;
}

// send a line as a command packet to the quad
void sendCommand(char *line, int pSize) {
 	// overwrite the newline at the end with the packet end byte
 	int lineLength = strlen(line);
 	int cmdLength = lineLength + 3; // start byte, 'C', end byte
 	static char cmdBuf[CMD_MAX_LENGTH+3];
 	
 	// set up packet 
 	cmdBuf[0] = PACKET_START_BYTE;
 	cmdBuf[1] = 'C'; // C for command, U for update
 	strcpy(&cmdBuf[2], line);
 	cmdBuf[cmdLength - 1] = PACKET_END_BYTE; 

	pthread_mutex_lock(&uart_lock);
	uartWrite(cmdBuf, cmdLength);
	pthread_mutex_unlock(&uart_lock);
}

// send the camera position information as-is to the quad
// It is formatted in microcart_cli
void sendCameraData(char *buffer, int pSize) {
	fprintf(testlog, "%d\n", pSize);
	pthread_mutex_lock(&uart_lock);
	uartWrite(buffer, pSize);
	pthread_mutex_unlock(&uart_lock);
}

int main(int argc, char *argvp[]) {
	//GITify this!!!!!!!
	//initialize components////////////////////////////////
	unsigned char buffer[255]; // goes in user input struct (see Joe and/or Amy about forwaring this through the control loop)
	int n, thread_quadlog_id = 0;
	pthread_t thread;
	
	testlog = fopen("logs/control_loop_test_log.txt", "w+");
	quadlog_test = fopen("logs/quadlog_test.txt", "w+");
	
	fprintf(testlog, "testing socket \n");
	fflush(testlog);

	if (pthread_mutex_init(&socket_lock, NULL) != 0) {
		fprintf(stderr, "Failed to initialize mutex.\n");
		return 1;
	}

	sockfd = init_socket_client(SOCKET_PORT_2);
	fprintf(testlog, "Connected to socket on port %d\n", SOCKET_PORT_2);
	fflush(testlog);
	
	
	// Initialize the UART connection
	int ok = uartInit("/dev/rfcomm0");
	if (!ok) {
		fprintf(testlog, "Error connecting... Aborting.\r\n");
		fflush(testlog);
		return 1; 
	}
	fprintf(testlog, "Connected to /dev/rfcomm0 successfully.\r\n\r\n");
	fflush(testlog);
	
	if (pthread_mutex_init(&uart_lock, NULL) != 0) {
		fprintf(stderr, "Failed to initialize mutex.\n");
		return 1;
	}
	
	while(1) {
		// Clear the buffer and read the message from the socket (from the server)
		bzero(buffer, 255);
	
		// get user input //////////////////////////////////////////////	
		pthread_mutex_lock(&socket_lock);
		n = read(sockfd, buffer, 255);
		pthread_mutex_unlock(&socket_lock);
		
		// If there was an error reading from the socket, throw an error
		if(n < 0) {
			error("ERROR reading from socket");
			//fprintf(testlog, "ERROR reading from socket\n");
			//fflush(testlog);
		}
		// end get user input //////////////////////////////////////////////	

		// send actuator commands ////////////////////////////////////
		if(buffer[1] == 'U') {
			sendCameraData(buffer, n);

			fprintf(testlog, "Sending VRPN Data %c, %d, %f, %f, %f, %f, %f, %f...%d\n", buffer[1], getInt(buffer, 2), getFloat(buffer, 6), getFloat(buffer, 10), getFloat(buffer, 14), getFloat(buffer, 18), getFloat(buffer, 22), getFloat(buffer, 26), sizeof(buffer));
			fflush(testlog);
		} else {
			// Send the command to the quad
			fprintf(testlog, "Sending Command: %s\n", buffer);
			fflush(testlog);
			sendCommand(buffer, n);
		}
		// end send actuator commands ////////////////////////////////////
	}
	
	fprintf(testlog, "%s\n", buffer);
	
	fclose(testlog);
	return 0;
}

