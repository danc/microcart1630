#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "CLI/vrpnhandler.h"

int value = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    value = returnValue();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    ui->label->setText("Hello");
}
