#ifndef VRPN_H
#define VRPN_H

#include <string>
#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"

void vrpn_init(std::string connectionName, void (*)(void*, const vrpn_TRACKERCB));

void vrpn_go();

#endif
