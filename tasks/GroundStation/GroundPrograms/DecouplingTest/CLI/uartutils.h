#ifndef _UARTUTILS_H
#define _UARTUTILS_H


// use ascii 2 and 3 for start/end of packet
#define  PACKET_START_BYTE  2
#define  PACKET_END_BYTE  3


int uartInit(char const*);

void uartWrite(unsigned char*, int);

int uartRead(unsigned char*, int);

#endif
