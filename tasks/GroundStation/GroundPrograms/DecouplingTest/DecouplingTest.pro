#-------------------------------------------------
#
# Project created by QtCreator 2015-11-18T19:54:11
#
#-------------------------------------------------

QT       += core gui

TARGET = DecouplingTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    CLI/vrpnutils.c

HEADERS  += mainwindow.h \
    CLI/vrpnhandler.h \
    CLI/vrpn.h \
    CLI/sync.h

FORMS    += mainwindow.ui

unix|win32: LIBS += -lquat -lvrpn -lpthread
