#include <stdio.h>
#include "main.h"
#include "TEST/test.h"
#include "VRPN/vrpnhandler.h"
#include <pthread.h>

int main(int argc, char* argv[]) {
/*
#ifdef __cplusplus
    main_cpp(argc, argv);
#endif
#ifndef __cplusplus
    int something = 0;
    something = printSomething();
    printf("This is something: %d\n", 35);
#endif
*/

    if(argc == 1) {
        pthread_t vrpnThread;
        pthread_create(&vrpnThread, NULL, vrpnmain, NULL);

        //vrpnmain(argc, argv);
        main_cpp(argc, argv);
    } else {
        int something = 0;
        something = printSomething();
        printf("This is something: %d\n", 35);

        //vrpnmain(argc, argv);
    }

	return 0;
}

int printSomething() {
    return 17;
}

