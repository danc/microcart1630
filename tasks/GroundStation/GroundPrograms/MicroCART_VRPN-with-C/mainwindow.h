#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "VRPN/vrpnhandler.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_startButton_clicked();
    void updateGUI();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
    int count;
    vrpnInfo_t test;
};

#endif // MAINWINDOW_H
