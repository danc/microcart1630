#include <stdio.h> /* Standard input/output definitions */
#include <stdlib.h>
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <errno.h> /* Error number definitions */
#include <ctype.h>
#include <fcntl.h> /* File control definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/stat.h>        /* For mode constants */
#include <pthread.h>
#include <quat.h>
#include "VRPN/vrpnhandler.h"
#include "VRPN/vrpn.h"

volatile vrpnInfo_t mostRecentUpdate;
volatile int gotFirstUpdate = 0;

// the rate at which packets are sent to the quad, in Hz
const int PACKET_RATE_PER_SEC = 50;
const int numArgs = 0;

// --------------------------------------------------------------------------
int vrpnmain(int argc, char* argv[]) {
    numArgs = argc;

    // initialize
	init();
	
	// create a thread to send cam updates via uart
	pthread_t sendCamUpdateThread;
	if (pthread_create(&sendCamUpdateThread, NULL, sendCamUpdateFunc, NULL)) {
		fprintf(stderr, "Error creating thread\n");
		return 1;
	}
	
	// go into vrpn loop, this uses the main thread completely 
	vrpn_go();
}
// --------------------------------------------------------------------------

void* sendCamUpdateFunc(void* param) {
	// wait for the first update to come in
	while (!gotFirstUpdate) {
		usleep(10000);
	}	
	
	// send off PACKET_RATE_PER_SEC packets every second
	while (1) {
		sendVrpnPacket(mostRecentUpdate);
		
		// sleep until the next packet
		usleep(1e6 / PACKET_RATE_PER_SEC);
	}
}

void init() {
	// initialize vrpn
	vrpn_init("192.168.0.120:3883", handle_pos);
}

// this function is called by VRPN whenever a camera update comes in
void VRPN_CALLBACK handle_pos(void* unused, const vrpn_TRACKERCB t) {
	// the tracker info comes in as a quaternion, but we want euler angles
    /*q_vec_type euler;
    q_type quat;
    quat[0] = t.quat[0];
    quat[1] = t.quat[1];
    quat[2] = t.quat[2];
    quat[3] = t.quat[3];

    q_to_euler(euler, quat);*/

    // euler is [yaw, pitch, roll]
    // t.pos is [x, y, z]

    /*
     * These are the x-y-z values reported from the camera system.
     * Due to its configuration, CURRENTLY positive X points east,
     * positive Y points south, and positive Z points into the ground.
     */
    float x = (float) t.pos[0];
    float y = (float) t.pos[1];
    float z = (float) t.pos[2];

    /*
     * Therefore, north is -y, east is x, and altitude is -z
     */
    mostRecentUpdate.north = -y;
	mostRecentUpdate.east  =  x;	
	mostRecentUpdate.alt   = -z;	
	
	/*
	 * Roll, pitch, and yaw come directly as the camera system says
	 */
    /*mostRecentUpdate.roll  = (float) euler[2];
	mostRecentUpdate.pitch = (float) euler[1];
    mostRecentUpdate.yaw   = (float) euler[0];*/
	
	// set the flag that says we can start sending packets
	gotFirstUpdate = 1;
}

void sendVrpnPacket(vrpnInfo_t info) {
    printf("Sending vrpn packet [n:%.2f e:%.2f a:%.2f r:%.2f p:%.2f y:%.2f]\n", info.north, info.east, info.alt, info.roll, info.pitch, info.yaw);
}

vrpnInfo_t getVRPNData() {
    return mostRecentUpdate;
}

