#ifndef _VRPNHANDLER_H
#define _VRPNHANDLER_H

#include <unistd.h>
#include <quat.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "vrpn.h"

//#include "vrpn_Connection.h"
//#include "vrpn_Tracker.h"

typedef struct {
	float north; //north
	float east; //east
	float alt; //altitude
	
	float roll;
	float pitch;
	float yaw;
} vrpnInfo_t;

void* vrpnmain(void* param);

void init();

void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);

void* sendCamUpdateFunc(void* param);

void sendVrpnPacket(vrpnInfo_t);

#ifdef __cplusplus
extern "C" {
    void getVRPNData(vrpnInfo_t* a);
}
#endif

#endif
