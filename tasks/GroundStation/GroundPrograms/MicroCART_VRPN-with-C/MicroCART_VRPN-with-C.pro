#-------------------------------------------------
#
# Project created by QtCreator 2015-12-05T15:41:02
#
#-------------------------------------------------

QT       += core gui

TARGET = MicroCART_VRPN-with-C
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    TEST/test.c \
    VRPN/vrpnutils.c \
    VRPN/vrpn.cpp

HEADERS  += mainwindow.h \
    TEST/test.h \
    main.h \
    VRPN/vrpnhandler.h \
    VRPN/vrpn.h

FORMS    += \
    mainwindow.ui


unix|win32: LIBS += -lvrpn
