#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "VRPN/vrpnhandler.h"
#include <sstream>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_startButton_clicked() {
    // Ideally, we would check if dataReceived is true here.
    // For the sake of seeing that the update functions run, it is not part of the program.

    ui->startButton->setEnabled(false);


    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateGUI()));
    timer->start(5);
}

void MainWindow::updateGUI() {
    QString displayValue;
    getVRPNData(&test);
    count++;

    //x, y, z Position
    displayValue = displayValue.setNum(test.north, 'g', 6);
    ui->textEdit_4->setText(displayValue);

    displayValue = displayValue.setNum(test.east, 'g', 6);
    ui->textEdit_5->setText(displayValue);

    displayValue = displayValue.setNum(test.alt, 'g', 6);
    ui->textEdit_6->setText(displayValue);

    displayValue = displayValue.setNum(count);
    ui->textEdit_7->setText(displayValue);
}
