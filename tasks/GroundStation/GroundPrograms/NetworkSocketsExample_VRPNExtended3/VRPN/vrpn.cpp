#include "vrpn.h"
#include "unistd.h"

vrpn_Connection *connection;
vrpn_Tracker_Remote *tracker;

void vrpn_init(std::string connectionName, void (*callback)(void*, const vrpn_TRACKERCB)) {
	connection = vrpn_get_connection_by_name(connectionName.c_str());
	tracker = new vrpn_Tracker_Remote("UAV", connection);

	tracker->register_change_handler(0, callback);
	usleep(2000);
}

void vrpn_go() {
	while(1){
		connection->mainloop();
		tracker->mainloop();
		usleep(20000);
	}
}

