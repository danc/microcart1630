#include <stdio.h> /* Standard input/output definitions */
#include <stdlib.h>
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <errno.h> /* Error number definitions */
#include <ctype.h>
#include <fcntl.h> /* File control definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
#include <pthread.h>
#include <signal.h>

#include "vrpnhandler.h"

FILE *vrpntestlog;
int newsockfd;
int socketfd;

volatile vrpnInfo_t mostRecentUpdate;
volatile int gotFirstUpdate = 0;

// The rate at which packets are sent to the quad, in Hz
const int PACKET_RATE_PER_SEC = 10;

void* sendCamUpdateFunc(void* param);

void sigINT_handler(int signum)
{
	fprintf(stderr, "\nExited VRPN handler.\n");
	close(newsockfd);
	exit(1);
}

int main(int argc, char *argv[]) {
	newsockfd = 0;
	socketfd = 0;

	signal(SIGINT, sigINT_handler);

	// Set up socket for sending VRPN data
	if((newsockfd = init_socket_server(SOCKET_PORT_1, socketfd)) <= 0)
		fprintf(stderr, "VrpnHandler: Failed to connect Control Loop on port %d\n", SOCKET_PORT_1);
	else
		fprintf(stderr, "VrpnHandler: Connected to Control Loop on port %d\n", SOCKET_PORT_1);

	// initialize vrpn
	vrpn_init("192.168.0.120:3883", handle_pos);
	
	// create a thread to send cam updates via uart
	pthread_t sendCamUpdateThread;
	if(pthread_create(&sendCamUpdateThread, NULL, sendCamUpdateFunc, NULL)) {
		fprintf(stderr, "Error creating thread (VRPN)\n");
		return 1;
	}
	
	// go into vrpn loop, this uses the main thread completely 
	vrpn_go();
	
	close(newsockfd);
	close(socketfd);
	
	return 0;
}

void* sendCamUpdateFunc(void* param) {
	// wait for the first update to come in
	while(!gotFirstUpdate) {
		usleep(10000);
	}
	
	// send off PACKET_RATE_PER_SEC packets every second
	while(1) {
		sendVrpnPacket(mostRecentUpdate);
		
		// sleep until the next packet
		usleep(1e6 / PACKET_RATE_PER_SEC);
	}

	return NULL;
}

// this function is called by VRPN whenever a camera update comes in
void VRPN_CALLBACK handle_pos(void* unused, const vrpn_TRACKERCB t) {
	// the tracker info comes in as a quaternion, but we want euler angles
	q_vec_type euler;
	q_to_euler(euler, t.quat);
	
	// euler is [yaw, pitch, roll]
	// t.pos is [x, y, z]

	/*
	* These are the x-y-z values reported from the camera system.
	* Due to its configuration, CURRENTLY positive X points east,
	* positive Y points south, and positive Z points into the ground.
	*/
	float x = (float) t.pos[0];
	float y = (float) t.pos[1];
	float z = (float) t.pos[2];

	/*
	* This is the ID of the VRPN Packet to be sent. This can be checked to
	* see whether duplicate packets are being received by the quadcopter.
	*/
	mostRecentUpdate.packetId++;

	/*
	* Therefore, north is -y, east is x, and altitude is -z
	*/
	mostRecentUpdate.north = -y;
	mostRecentUpdate.east  = x;
	mostRecentUpdate.alt   = -z;

	/*
	 * Roll, pitch, and yaw come directly as the camera system says
	 */
	mostRecentUpdate.roll  = (float) euler[2];
	mostRecentUpdate.pitch = (float) euler[1];
	mostRecentUpdate.yaw   = (float) euler[0];

	// set the flag that says we can start sending packets
	gotFirstUpdate = 1;
}

void sendVrpnPacket(vrpnInfo_t info) {
	int pSize = sizeof(info) + 8;
	int n;
	char packet[pSize];
	packet[0] = 0xBE; // BEGIN					//PACKET_START_BYTE;
	packet[1] = 0x04;	// UPDATE				//'U'; // U for vrpn camera update, C for command
	packet[2] = 0x00; // N/A
	packet[3] = (info.packetId & 0x000000ff); 			// MSG ID(1)
	packet[4] =	((info.packetId >> 8) & 0x000000ff); // MSG ID(2)
	packet[5] = (sizeof(info) & 0x000000ff); 			// DATALEN(1)
	packet[6] = ((sizeof(info) >> 8) & 0x00000ff); 	// DATALEN(2)
	memcpy(&packet[7], &info, sizeof(info));

	char checksum = 0;
	int i;
	for(i=0; i < pSize - 1; i++)
		checksum ^= packet[i];

	packet[pSize - 1] = checksum; //PACKET_END_BYTE;

//	fprintf(stderr, "pSize is %d\n", pSize);
	
	/*char *packet2 = (char *)malloc(sizeof(char) * 256);
	sprintf(
		packet2,
		"[id: %d n:%.2f e:%.2f a:%.2f r:%.2f p:%.2f y:%.2f]\n",
		info.packetId,
		info.north,
		info.east,
		info.alt,
		info.roll,
		info.pitch,
		info.yaw
	);
	fprintf(vrpntestlog, "%s", packet2);
	fflush(vrpntestlog);*/

	//fprintf(stderr, "vrpnhandler3\n");
	//fflush(stderr);

	// file descriptor is invalid or closed (not sure if this actually works)
	if(fcntl(newsockfd, F_GETFL) >= 0 && errno != EBADF) {
//		fprintf(stderr, "writing socket\n");
		n = write(newsockfd, packet, pSize);
		if(n < 0) {	
			error("vrpnhandler: ERROR writing to socket");
		}
	}
}

