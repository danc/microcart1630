/*

"microcart_cli", will
	1.) Connect to the Zybo board (quad)
	2.) Start the control loop program
	3.) Take user input and forward it to the control
	4.) Print responses from the quad to the screen
*/

#include "microcart_cli.h"
#include "./ControlLoop/type_def.h"
#include "./Communication/communication.h"
#include "./Communication/commands.h"

int socket_quadlog;
int socket_vrpnhandler;
int socket_control_loop1 = 0, socket_control_loop2 = 0, control_loop_cmds = 0, control_loop_vrpn = 0;

pthread_mutex_t socket_lock;

void *thread_receive_vrpn_data(void *pVrpn) {
	unsigned char buffer[255];
	vrpnInfo_t * vInfo = pVrpn;

	while(1) {
		// Clear the buffer and read the message from the socket (from the server)
		memset(buffer, 0, 255);

		// If there was an error reading from the socket, throw an error
		if(read(control_loop_vrpn, buffer, 255) <= 0) {
			fprintf(stderr, "CLI VRPN: ERROR reading from socket.\n");
			fflush(stderr);
			continue;
		}

		unsigned char * vrpn_data;
		metadata_t metadata; 

		parse_packet(buffer, &vrpn_data, &metadata);

		// store the vrpn data into the vrpn struct so the CLI/GUI can display it
		vInfo->packetId = getInt(vrpn_data, 0);
		vInfo->x_pos = getFloat(vrpn_data, 4);
		vInfo->y_pos = getFloat(vrpn_data, 8);
		vInfo->z_pos = getFloat(vrpn_data, 12);
		vInfo->roll = getFloat(vrpn_data, 16);
		vInfo->pitch = getFloat(vrpn_data, 20);
		vInfo->yaw = getFloat(vrpn_data, 24);

		//display the vrpn data	
		/*fprintf(stderr, "[id: %d n:%.2f e:%.2f a:%.2f r:%.2f p:%.2f y:%.2f]\n",
			vInfo->packetId,
			vInfo->x_pos,
			vInfo->y_pos,
			vInfo->z_pos,
			vInfo->roll,
			vInfo->pitch,
			vInfo->yaw
		);*/
	}

	pthread_exit(NULL);
}

int main(int argc, char *argv[]) {

	pid_t connect_zybo = 0, control_loop = 0;
	socket_quadlog = 0;
	socket_vrpnhandler = 0;
	vrpnInfo_t vInfo;

	// disconnect the zybo if its connected
	while(access("/dev/rfcomm0", F_OK) == 0)
		remove("/dev/rfcomm0");

	// Establish connection to quad
	connect_zybo = fork();
	if(connect_zybo == 0) {
			execl("./connect_zybo_bt.sh", "connect_zybo_bt.sh", NULL);
	}
	
	fprintf(stderr, "CLI: Connecting to zybo (please be patient)...\n");
	//wait for connect zybo to run (times out at 10 seconds)
	int failed_connection = 0;
	clock_t start = clock();
	while(access("/dev/rfcomm0", F_OK) != 0)
	{
		//restart connect zybo if it already failed
		int status;
		pid_t alive = waitpid(connect_zybo, &status, WNOHANG);
		if(alive != 0)
		{
			// Establish connection to quad
			connect_zybo = fork();
			if(connect_zybo == 0) {
					execl("./connect_zybo_bt.sh", "connect_zybo_bt.sh", NULL);
			}
		}

		if((((float)(clock() - start)) / CLOCKS_PER_SEC) >= 10.0)
		{
			failed_connection = 1;
			break;
		}
	}
	
	if(failed_connection)
	{
		fprintf(stderr, "CLI: Connection to quad failed.");
		kill(connect_zybo, SIGINT);
		exit(-1);
	}

	// Start control_loop process
	control_loop = fork();
	if(control_loop == 0) {
		if(argc >= 2)
		{
			execl("./ControlLoop/main", "main", argv[1], NULL);		
		}
		else
		{
			execl("./ControlLoop/main", "main", NULL);
		}	
	}

	// Set up socket for communication with control_loop for user commands
	if((control_loop_cmds = init_socket_server(SOCKET_PORT_2, socket_control_loop1)) <= 0)
		fprintf(stderr, "CLI: Failed to connect to control_loop on port %d\n", SOCKET_PORT_2);
	else		
		fprintf(stderr, "CLI: Connected to control_loop socket on port %d\n", SOCKET_PORT_2);

	// Set up socket for communication with control_loop for vrpn data receiving
	if((control_loop_vrpn = init_socket_server(SOCKET_PORT_4, socket_control_loop2)) <= 0)
		fprintf(stderr, "CLI: Failed to connect to control_loop on port %d\n", SOCKET_PORT_4);
	else		
		fprintf(stderr, "CLI: Connected to control_loop socket on port %d\n", SOCKET_PORT_4);

	pthread_t thread;
	// Retrieve VRPN data from the control loop
	fprintf(stderr, "CLI: Starting VRPN receiving thread...\n");
	pthread_create(&thread, NULL, thread_receive_vrpn_data, &vInfo);
	fprintf(stderr, "CLI: Thread VRPN receiving started.\n");

	
	// Wait to display prompt to user (for the processes to get started in the control loop)
	usleep(500000);

	char response[CMD_MAX_LENGTH];
	char userCommand[CMD_MAX_LENGTH] = {};
	int n;
	fprintf(stderr, "$microcart> ");

	while(fgets(userCommand, sizeof(userCommand), stdin) != NULL) {
		
		// if the user simply hit enter then let them try again
		if((userCommand[0] == '\n') || (userCommand[0] == '\r'))
		{
			fprintf(stderr, "$microcart> ");

			memset(userCommand, 0, CMD_MAX_LENGTH);

			continue;
		}
		
		//remove the enter character from usercommand
		
		//if((userCommand[strlen(userCommand) - 1] == '\n') || (userCommand[strlen(userCommand) - 1] == '\r'))
		//	userCommand[strlen(userCommand) - 1] = 0;

		unsigned char * packet;

		formatCommand(userCommand, &packet);
		fprintf(stderr, "CLI sees as: %f\n", getFloat(packet, 7));

		// Write the command to the control_loop socket
		n = write(control_loop_cmds, packet, ((packet[6] << 8) | packet[5]) + 8);
		if(n < 0) {
			fprintf(stderr, "CLI: ERROR writing to socket\n");
		}

		// Read the response from the control loop
		n = read(control_loop_cmds, response, sizeof(char) * CMD_MAX_LENGTH);

		if(n <= 0) {
			fprintf(stderr, "CLI: ERROR reading from socket %d\n", n);
		}
		else
		{
			//unsigned char * data;
		//	metadata_t metadata;
		//	parse_packet(response, &data, &metadata);
			//data[metadata.data_len - 1] = 0;

		//	fprintf(stderr, "CLI: Response from Control Loop: %s\n\n", data);
		}
		
		fprintf(stderr, "$microcart> ");

		memset(userCommand, 0, CMD_MAX_LENGTH);
	}
	fprintf(stderr, "Exiting the MicroCART Program. (2)\n");

	return 0;
}

