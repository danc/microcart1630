#include <stdio.h>		/* Standard input/output definitions */
#include <string.h>		/* String function definitions */
#include <unistd.h>		/* UNIX standard function definitions */
#include <fcntl.h>		/* File control definitions */
#include <errno.h>		/* Error number definitions */
#include <ctype.h>
#include <termios.h>	/* POSIX terminal control definitions */
#include <stdlib.h>
#include <signal.h>

#include "../microcart_cli.h"
#include "../uartutils.h"
#include "../ControlLoop/type_def.h"
#include "../Communication/communication.h"
#include "../Communication/commands.h"

int uart_fd;
FILE *quadlog_file;

void sigINT_handler(int signum)
{
	fprintf(stderr, "\nExited quadlog.\n");
	fclose(quadlog_file);
	raise(SIGINT);
	exit(1);
}

// receives a packet from the uart
//
// 	packet: an unallocated (char*) (pass into this function as &packet)
//			it gets filled with the message from the uart
//
int read_packet_uart(unsigned char ** packet)
{	
	//----------------------------------------------------------------------------------------------
	//     index||     0    |     1    |      2      |  3 & 4 |      5 & 6       |  7+  |   end    |
	//---------------------------------------------------------------------------------------------|
	// msg param|| beg char | msg type | msg subtype | msg id | data len (bytes) | data | checksum |
	//-------------------------------------------------------------------------------------------- |
	//     bytes||     1    |     1    |      1      |    2   |        2         | var  |    1     |
	//----------------------------------------------------------------------------------------------	

	unsigned char buf[2000] = {};

	// read the meta data
	uartRead(buf, 7);

	int j;
	for(j=0; j< 7; j++)
		fprintf(stderr, "0x%02x\t", buf[j]);

	fprintf(stderr, "\n");

	// first byte must be the begin char
	if(buf[0] != 0xBE)
		return -1;

	// data length bytes 5 and 6
	int datalen = (buf[6] << 8) | buf[5];

	fprintf(stderr, "datalen: %d\n", datalen);

	// read in the data and checksum
	uartRead(&(buf[7]), datalen+1);

	// fill packet with the message
	*packet = malloc(datalen + 8);
	int i;
	for(i=0; i < datalen + 8; i++)
		(*packet)[i] = buf[i];

	return datalen;
}

int main(int argc, char *argv[]) {
	int socketfd = 0;
	int socket_control_loop = 0;

	// open the log file
	if(argc >= 2)
	{
		char log_file[300] = {'l', 'o', 'g','s', '/'};
		strcat(log_file, argv[1]);
		quadlog_file = fopen(log_file, "w+");
	}
	else
	{
		quadlog_file = fopen("logs/quadlog.txt", "w+");
	}

	// Set up socket for sending quad responses to control loop
	socket_control_loop = init_socket_server(SOCKET_PORT_3, socketfd);
	fprintf(stderr, "----BEG quad log init-----\n");
	fprintf(stderr, "QuadLog: Connected to Control loop on port %d.\n", SOCKET_PORT_3);

	int ok = uartInit("/dev/rfcomm0");
	if(!ok) { 
		fprintf(stderr, "QuadLog: Error connecting... Aborting.\r\n"); 
		return 1; 
	}
	fprintf(stderr, "QuadLog: Begin log\r\n");
	fprintf(stderr, "----END quad log init-----\n");
	fflush(quadlog_file);

	signal(SIGINT, sigINT_handler);

	while(1) {
		unsigned char * packet = NULL;

		int packet_len = read_packet_uart(&packet);

		fprintf(stderr, "Quadlog: Received packet from quad.\n");

		if(packet_len == -1)
		{
			fprintf(stderr, "QuadLog: Error parsing packet\n");
			if(packet != NULL)	
				free(packet);
			continue;
		}	

		fprintf(stderr, "%s\n", &(packet[7]));	

		//send packet to the control loop
		if(write(socket_control_loop, packet, packet_len) <= 0)
			fprintf(stderr, "QuadLog: Error sending to socket\n"); 

		if(packet != NULL)	
			free(packet);
	}

	return 0;
}
