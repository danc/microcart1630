#ifndef _SOCKET_UTILS_H
#define _SOCKET_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#define SOCKET_PORT_1 50009
#define SOCKET_PORT_2 50010
#define SOCKET_PORT_3 50011
#define SOCKET_PORT_4 50015

int init_socket_server(int portNumber, int socket_fd);

int init_socket_client(int portNumber);

void error(const char *msg);

#endif

