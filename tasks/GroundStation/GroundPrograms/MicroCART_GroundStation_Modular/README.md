This is the general read me file for microcart. It is a text file that explains how to download and use the project, and should also talk about the directory structure and where file should be commited/located.

This file can be rendered to an html page for better human readability. For .md (markdown) syntax and how we can render this file to an html page (if we choose to), visit this website: 
https://help.github.com/articles/markdown-basics/
