#ifndef _COMMUNICATION_H
#define _COMMUNICATION_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include "commands.h"
#include "../ControlLoop/type_def.h"


int formatCommand(unsigned char *command, unsigned char **formattedCommand);
int formatPacket(metadata_t *metadata, void *data, unsigned char **formattedCommand);
int logData(unsigned char *log_msg, unsigned char *formattedCommand);
int processCommand(unsigned char *command, modular_structs_t *structs);
int parse_packet(unsigned char * packet, unsigned char ** data, metadata_t * meta_data);

#endif
