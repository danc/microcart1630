#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>		/* File control definitions */
#include <errno.h>		/* Error number definitions */
#include <ctype.h>
#include <termios.h>	/* POSIX terminal control definitions */
//#include <pthread.h>
#include <string.h>

#include "../microcart_cli.h"
#include "../uartutils.h"
 
#include "send_actuator_commands.h"
#include "type_def.h"

int send_actuator_commands(log_t* log_struct, actuator_command_t* actuator_command_struct)
{
	static int loop_count = 0;
	loop_count++;

	// only send vrpn data 10 times per sec
	// this function gets called every 5 msec
	if(loop_count >= 20)
	{
		pthread_mutex_lock(&actuator_command_struct->vrpn_packet_lock);
	//	uartWrite(actuator_command_struct->vrpn_packet, actuator_command_struct->vrpn_packet_size);
		pthread_mutex_unlock(&actuator_command_struct->vrpn_packet_lock);
		loop_count = 0;	
	}
    return 0;
}
 
