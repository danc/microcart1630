#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>		/* File control definitions */
#include <errno.h>		/* Error number definitions */
#include <ctype.h>
#include <termios.h>	/* POSIX terminal control definitions */
#include <pthread.h>
#include <string.h>

#include "initialize_components.h"
#include "type_def.h"
#include "../Communication/communication.h"
#include "../Communication/commands.h"
#include "../microcart_cli.h"
#include "../uartutils.h"

void *thread_forward_vrpn_data(void * param) {

	modular_structs_t * structs = (modular_structs_t *) param;

	while(1) {
		
		unsigned char buffer[256] = {};	
		int n;

		// its ok to read this fd (without mutex) because this thread is the only thread that does so
		n = read(structs->user_input_struct.socket_vrpnhandler, (char *) buffer, 255);


		// If there was an error reading from the socket, throw an error
		if(n <= 0) {
			fprintf(stderr, "ERROR reading from socket. n = %d\n", n);
			fflush(stderr);
			//break;
		}

		if(buffer[0] != 0xBE)
			fprintf(stderr, "Control Loop: The first meta data byte was not 0xBE\n");

		// this packet will only be an update packet with subtype 0x00 and data size 28 bytes
		int datalen = (buffer[6] << 8) | buffer[5];

		//fprintf(stderr, "%d\n", (int)((buffer[10] << 24) | (buffer[9] << 16) | (buffer[8] << 8) | (buffer[7])));

		// Write vrpn data to the raw sensor struct
		pthread_mutex_lock(&(structs->raw_sensor_struct.vrpn_update_lock));
		structs->raw_sensor_struct.packetId = getInt(buffer, 7);
		structs->raw_sensor_struct.x_position = getFloat(buffer, 11);
		structs->raw_sensor_struct.y_position = getFloat(buffer, 15);
		structs->raw_sensor_struct.z_position = getFloat(buffer, 19);
		structs->raw_sensor_struct.roll = getFloat(buffer, 23);
		structs->raw_sensor_struct.pitch = getFloat(buffer, 27);
		structs->raw_sensor_struct.yaw = getFloat(buffer, 31);
		pthread_mutex_unlock(&(structs->raw_sensor_struct.vrpn_update_lock));

		pthread_mutex_lock(&(structs->actuator_command_struct.vrpn_packet_lock));
		if(structs->actuator_command_struct.vrpn_packet)
			free(structs->actuator_command_struct.vrpn_packet);
		structs->actuator_command_struct.vrpn_packet = malloc(datalen + 8);
		memcpy(structs->actuator_command_struct.vrpn_packet, buffer, n);
		structs->actuator_command_struct.vrpn_packet_size = n;
		write(structs->user_input_struct.socket_cli_vrpn, structs->actuator_command_struct.vrpn_packet, datalen + 8);
		pthread_mutex_unlock(&(structs->actuator_command_struct.vrpn_packet_lock));



		char given_checksum = buffer[35];
		char calc_checksum = 0;
		int i;
		for(i=0; i < datalen + 7; i++)
			calc_checksum ^= buffer[i];
		
		if(given_checksum != calc_checksum)
			fprintf(stderr, "Checksums did not match for packetId %d\n", structs->raw_sensor_struct.packetId);
	}

	pthread_exit(NULL);
}

 
int initializeAllComponents(modular_structs_t * structs)
{
	pthread_t thread;

	// Initialize the controller
	control_algorithm_init();

	// Initialize the UART connection
	int ok = uartInit("/dev/rfcomm0");
	if (!ok) {
		fprintf(stderr, "Error connecting... Aborting.\r\n");
		fflush(stderr);
		return 1; 
	}
	fprintf(stderr, "Control loop: Connected to /dev/rfcomm0 successfully.\r\n");

	// Initialize the loop timer
	timer_init();

	//need structures to pass IDs

	// Start vrpnhandler process
	structs->user_input_struct.vrpnhandler = fork();
	if(structs->user_input_struct.vrpnhandler == 0) {
		execl("/local/ucart/Desktop/MicroCART_May1630/tasks/GroundStation/GroundPrograms/MicroCART_GroundStation_Modular/VRPN/vrpnhandler", "vrpnhandler", NULL);
	}

	// Start quadlog process
	structs->user_input_struct.quadlog = fork();
	if(structs->user_input_struct.quadlog == 0) {
		execl("/local/ucart/Desktop/MicroCART_May1630/tasks/GroundStation/GroundPrograms/MicroCART_GroundStation_Modular/Quadlog/quadlog", "quadlog", structs->log_struct.log_filename, NULL);
	}
	
	// wait for vrpn to start
	usleep(100000);


	// setup socket with CLI/GUI for reading user commands
	if((structs->user_input_struct.socket_cli_cmds = init_socket_client(SOCKET_PORT_2)) <= 0)
		fprintf(stderr, "Control Loop: Failed to connect to CLI on port %d\n", SOCKET_PORT_2);	
	else
		fprintf(stderr, "Control Loop: Connected to CLI on port %d\n", SOCKET_PORT_2);

	// set up non-blocking reads
	int flags = fcntl(structs->user_input_struct.socket_cli_cmds, F_GETFL, 0);
	fcntl(structs->user_input_struct.socket_cli_cmds, F_SETFL, flags | O_NONBLOCK);


	// setup socket with CLI/GUI for forwarding VRPN data to the CLI/GUI
	if((structs->user_input_struct.socket_cli_vrpn = init_socket_client(SOCKET_PORT_4)) <= 0)
		fprintf(stderr, "Control Loop: Failed to connect to CLI on port %d\n", SOCKET_PORT_4);	
	else
		fprintf(stderr, "Control Loop: Connected to CLI on port %d\n", SOCKET_PORT_4);


	//setup socket with quadlog
	structs->user_input_struct.socket_quadlog = init_socket_client(SOCKET_PORT_3);
	fprintf(stderr, "Connected to quadlog socket on port %d\n", SOCKET_PORT_3);

	// set up non-blocking reads
	flags = fcntl(structs->user_input_struct.socket_quadlog, F_GETFL, 0);
	fcntl(structs->user_input_struct.socket_quadlog, F_SETFL, flags | O_NONBLOCK);


	// setup socket with VRPN thread
	if((structs->user_input_struct.socket_vrpnhandler = init_socket_client(SOCKET_PORT_1)) <= 0)
		fprintf(stderr, "Control Loop: Failed to connect to vrpnhandler on port %d\n", SOCKET_PORT_1);
	else
		fprintf(stderr, "Control Loop: Connected to vrpnhandler on port %d\n", SOCKET_PORT_1);


	// Retrieve VRPN data until the process is killed
	fprintf(stderr, "Control Loop: Starting VRPN forwarding thread...\n");
	pthread_mutex_init(&structs->raw_sensor_struct.vrpn_update_lock, NULL);
	pthread_mutex_init(&structs->actuator_command_struct.vrpn_packet_lock, NULL);
	structs->user_input_struct.thread_vrpn_data = pthread_create(&thread, NULL, thread_forward_vrpn_data, structs);
	fprintf(stderr, "Control Loop: Thread VRPN forwarding started.\n");
	
	
	//////////////////////////////////////////////////////
	fprintf(stderr, "Control Loop: Initializing components complete\n");
	fflush(stderr);
	
	
    return 0;
}
 
