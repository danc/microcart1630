#ifndef INITALIZE_COMPONENTS_H_
#define INITALIZE_COMPONENTS_H_

#include "timer.h"
#include "control_algorithm.h"
#include "type_def.h"
/**
 * @brief 
 *      Runs loops to make sure the quad is responding and in the correct state before starting.
 *
 * @return
 *      error message
 *
 */
int protection_loops();

/**
 * @brief
 *      Initializes the sensors, communication, and anything else that needs
 * initialization.
 *
 * @return 
 *      error message
 *
 */
int initializeAllComponents(modular_structs_t * structs);

#endif /* INITALIZE_COMPONENTS_H_ */
