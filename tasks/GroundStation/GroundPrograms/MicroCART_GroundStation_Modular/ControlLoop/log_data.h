#ifndef LOG_DATA_H_
#define LOG_DATA_H_

#include "type_def.h"

/**
 * @brief 
 *      Logs the data obtained throughout the controller loop.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @return 
 *      error message
 *
 */
 int log_data(log_t* log_struct);

#endif /* LOG_DATA_H_ */
