#ifndef SENSOR_PROCESSING_H_
#define SENSOR_PROCESSING_H_

#include "log_data.h"
#include "sensor.h"
#include "type_def.h"
/**
 * @brief 
 *      Processes the data from the sensors.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param raw_sensor_struct
 *      structure of the raw data from the sensors
 *
 * @param sensor_struct
 *      structure of the processed data from the sensors
 *
 * @return 
 *      error message
 *
 */
int sensor_processing(log_t* log_struct, raw_sensor_t* raw_sensor_struct, sensor_t* sensor_struct);

#endif /* SENSOR_PROCESSING_H_ */
