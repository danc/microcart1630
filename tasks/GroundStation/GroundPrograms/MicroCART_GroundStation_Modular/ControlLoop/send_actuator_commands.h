#ifndef SEND_ACTUATOR_COMMANDS_H_
#define SEND_ACTUATOR_COMMANDS_H_

#include "log_data.h"
#include "actuator_command_processing.h"
#include "type_def.h"
/**
 * @brief 
 *      Sends commands to the actuators.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param actuator_command_struct
 *      structure of the commmands to go to the actuators
 *
 * @return 
 *      error message
 *
 */
int send_actuator_commands(log_t* log_struct, actuator_command_t* actuator_command_struct);

#endif /* SEND_ACTUATOR_COMMANDS_H_ */
