#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>		/* File control definitions */
#include <errno.h>		/* Error number definitions */
#include <ctype.h>
#include <termios.h>	/* POSIX terminal control definitions */
#include <string.h>

#include "../microcart_cli.h"
#include "../uartutils.h"

#include "user_input.h"
#include "type_def.h"

#include "../Communication/communication.h"
#include "../Communication/commands.h"

int get_user_input(log_t* log_struct,  user_input_t* user_input_struct)
{

	// receive command from CLI/GUI (non-blocking)
	if((user_input_struct->cli_n = read(user_input_struct->socket_cli_cmds, user_input_struct->cli_cmd_buffer, 255)) > 0)
	{
		fprintf(stderr, "Control Loop sees as: %f\n", getFloat(user_input_struct->cli_cmd_buffer, 7));

		write(user_input_struct->socket_cli_cmds, user_input_struct->cli_cmd_buffer, user_input_struct->cli_n);
		
		uartWrite(user_input_struct->cli_cmd_buffer, user_input_struct->cli_n);
	}

	// receive quad response from quadlog (non-blocking)
	if((user_input_struct->quadlog_n = read(user_input_struct->socket_quadlog, user_input_struct->quadlog_buffer, 255)) > 0)
	{	
		fprintf(stderr, "Received from quad: %s", user_input_struct->quadlog_buffer);

		if(user_input_struct->cli_data)
			free(user_input_struct->cli_data);

		parse_packet(user_input_struct->quadlog_buffer, &user_input_struct->cli_data, &user_input_struct->cli_metadata);		
		//write(user_input_struct->cli_sock, user_input_struct->quadlog_buffer, strlen(user_input_struct->cmd_buffer));
	}
	
	return 0;
}
 
