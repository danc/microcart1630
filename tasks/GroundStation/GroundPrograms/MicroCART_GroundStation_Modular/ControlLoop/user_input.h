#ifndef USER_INPUT_H_
#define USER_INPUT_H_

#include "log_data.h"
#include "type_def.h"

/**
 * @brief 
 *      Recieves user input to the system.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param user_input_struct
 *      structure of the data inputted by the user
 *
 * @return 
 *      error message
 *
 */
int get_user_input(log_t* log_struct,  user_input_t* user_input_struct);


#endif /* USER_INPUT_H_ */
