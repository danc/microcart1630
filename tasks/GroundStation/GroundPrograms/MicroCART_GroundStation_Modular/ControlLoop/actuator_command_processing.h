#ifndef ACTUATOR_COMMAND_PROCESSING_H_
#define ACTUATOR_COMMAND_PROCESSING_H_

#include "log_data.h"
#include "control_algorithm.h"
#include "type_def.h"


/**
 * @brief 
 *      Processes the commands to the actuators.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param raw_actuator_struct
 *      structure of the commmands outputted to go to the actuators
 *
 * @param actuator_command_struct
 *      structure of the commmands to go to the actuators
 *
 * @return 
 *      error message
 *
 */
int actuator_command_processing(log_t* log_struct, raw_actuator_t* raw_actuator_struct, actuator_command_t* actuator_command_struct);

#endif /* ACTUATOR_COMMAND_PROCESSING_H_ */
