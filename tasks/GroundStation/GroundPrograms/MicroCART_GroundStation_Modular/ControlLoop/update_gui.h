#ifndef UPDATE_GUI_H_
#define UPDATE_GUI_H_

#include "log_data.h"
#include "type_def.h"
/**
 * @brief 
 *      Updates the user interface.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @return 
 *      error message
 *
 */
int update_GUI(log_t* log_struct);

#endif /* UPDATE_GUI_H_ */
