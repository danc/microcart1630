#ifndef _PARSE_PACKET_H
#define _PARSE_PACKET_H

#include <stdio.h>
#include <stdlib.h>
#include "typedef.h"

int parse_packet(unsigned char * packet, unsigned char ** data, metadata_t * meta_data);
float getFloat(unsigned char* str, int pos);
int getInt(unsigned char* str, int pos);

#endif
