/*
 * main.c
 *
 *  Created on: Nov 11, 2015
 *      Author: Amy Seibert
 */
#include <stdio.h>

#include "timer.h"
#include "log_data.h"
#include "initialize_components.h"
#include "user_input.h"
#include "sensor.h"
#include "sensor_processing.h"
#include "control_algorithm.h"
#include "actuator_command_processing.h"
#include "send_actuator_commands.h"
#include "update_gui.h"
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>

//new typedef.h
#include "type_def.h"

void sigINT_handler_CL(int signum)
{
	fprintf(stderr, "\nExited Control Loop.\n");
	exit(1);
}

int main(int argc, char * argv[])
{
	// Structures to be used throughout
	modular_structs_t structs = {};


		if(argc >= 2)
		{
			structs.log_struct.log_filename = argv[1];
		}
	

	// Initialize all required components:
	// sockets, uart, control algorithm, structs
	int init_error = initializeAllComponents(&structs);
	if (init_error == -1) {
		fprintf(stderr, "ERROR (main): Problem initializing...Goodbye\r\n");
		fflush(stderr);
		return init_error;
	}

	signal(SIGINT, sigINT_handler_CL);

	fprintf(stderr, "Control loop intialized.\n");

	// Main control loop
	while(1)
	{
		// Processing of loop timer at the beginning of the control loop
		timer_start_loop();

		// Get the user input and put it into user_input_struct
		get_user_input(&(structs.log_struct), &(structs.user_input_struct));

		// Get data from the sensors and put it into raw_sensor_struct
		get_sensors(&(structs.log_struct), &(structs.user_input_struct), &(structs.raw_sensor_struct));

		// Process the sensor data and put it into sensor_struct
		sensor_processing(&(structs.log_struct), &(structs.raw_sensor_struct), &(structs.sensor_struct));

		// Run the control algorithm
		control_algorithm(&(structs.log_struct), &(structs.sensor_struct), &(structs.setpoint_struct), &(structs.parameter_struct), 
			&(structs.user_defined_struct), &(structs.raw_actuator_struct));

		// Process the commands going to the actuators
		actuator_command_processing(&(structs.log_struct), &(structs.raw_actuator_struct), &(structs.actuator_command_struct));

		// send the actuator commands
		send_actuator_commands(&(structs.log_struct), &(structs.actuator_command_struct));

		// update the GUI
		update_GUI(&(structs.log_struct));

		// Log the data collected in this loop
		log_data(&(structs.log_struct));

		// Processing of loop timer at the end of the control loop
		timer_end_loop();
		
	}
	return 0;
}

