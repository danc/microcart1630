#ifndef CONTROL_ALGORITHM_H_
#define CONTROL_ALGORITHM_H_

#include "log_data.h"
#include "sensor_processing.h"
#include "type_def.h"
/**
 * @brief 
 *      Initializes everything used in the control algorithm.
 *
 * @return
 *      error message
 *
 */
int control_algorithm_init();

/**
 * @brief
 *      Runs the control algorithm on the data and outputs a command for actuators.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param sensor_struct
 *      structure of the processed data from the sensors
 *
 * @param setpoint_struct
 *      structure of the setpoints used in the controller
 *
 * @param parameter_struct
 *      structure of the parameters used in the controller
 *
 * @param user_defined_struct
 *      structure of the user defined variables
 *
 * @param raw_actuator_struct
 *      structure of the commmands outputted to go to the actuators
 *
 * @return 
 *      error message
 *
 */
int control_algorithm(log_t* log_struct, 
                      sensor_t* sensor_struct, 
                      setpoint_t* setpoint_struct, 
                      parameter_t* parameter_struct, 
                      user_defined_t* user_defined_struct, 
                      raw_actuator_t* raw_actuator_struct);

#endif /* CONTROL_ALGORITHM_H_ */
