/*
 * timer.c
 *
 *  Created on: Feb 24, 2016
 *      Author: Amy Seibert
 */

#include "timer.h"
#include <time.h>

clock_t start, end;
float desired_secs = 0.005;

int timer_init()
{
	return 0;
}

int timer_start_loop()
{
	start = clock();
	return 0;
}

int timer_end_loop()
{
	end = clock();

	float current_loop_time = ((float)(end - start)) / CLOCKS_PER_SEC;

	while(current_loop_time < desired_secs)
	{
		end = clock();
		current_loop_time = ((float)(end - start)) / CLOCKS_PER_SEC;
	}

	return 0;
}
