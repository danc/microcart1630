#ifndef _MICROCART_CLI_H
#define _MICROCART_CLI_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <fcntl.h>

#include "socket_utils.h"

#define CMD_MAX_LENGTH 1024
#define PACKET_START_BYTE  2
#define PACKET_END_BYTE  3

int init_socket_vrpn();

int init_socket_control_loop(int *sockfd, int *newsockfd);

void close_sockets();

#endif

