/*
The purpose of this program is to simplify the process for connecting to the MicroCART quadcopter via Bluetooth.
This program will ultimately be the middle-man between the VRPN data and the program which communicates with the quadcopter.
There will be communication such that:
	a.) The VRPN Handler program will act as a server of VRPN data via a network socket.
		 This program will act as a client and receive the VRPN data.

	b.) This program will act as a server of VRPN Data and User Input to the Ground Station oftware
		 The ground station program will act as a client and receive the information sent from this program.

Coincidentally, this program, "microcart_cli", will
	1.) Start the bash script to connect the Bluetooth to the quadcopter. (connect_zybo_bt.sh)
	2.) Start the VRPN Handler program once the script has successfully connected.
	3.) Start the Ground Station program, which communicates directly with 
	4.) (Optional) Start the control loop program.

*/

#include "microcart_cli.h"

int socket_vrpnhandler, returnToMenu = 0;
int thread_quit = 0;
pid_t vrpnhandler;
pid_t control_loop;
int socket_control_loop = 0, newsocket_control_loop = 0;

pthread_mutex_t socket_lock;

/*void signal_callback_handler(int signal2) {
	signal(SIGINT, signal_callback_handler);

	printf("MicroCART: Returning to menu.\n");
	
	returnToMenu = 1;
}*/

/*void exit_handler(int signal) {
//void signal_callback_handler2(int signal) {
	printf("MicroCART: Quitting safely. Signal: %d\n", signal);
	
	if(kill(vrpnhandler, SIGINT) == 0) {
		printf("MicroCART CLI: Successfully killed vrpnhandler.\n");
	}
	
	//usleep(5000000);
	
	//pthread_exit(NULL);
	//close_sockets();
	
	if(socket_vrpnhandler != 0) {
		close(socket_vrpnhandler);
		socket_vrpnhandler = 0;
	}

	thread_quit = 1;

	if(kill(control_loop, SIGQUIT) == 0) {
		printf("MicroCART CLI: Successfully killed control_loop.\n");
	}

	if(socket_control_loop != 0) {
		close(socket_control_loop);
		socket_control_loop = 0;
	}
	if(newsocket_control_loop != 0) {
		close(newsocket_control_loop);
		newsocket_control_loop = 0;
	}

	exit(signal);
}*/

void *thread_forward_vrpn_data(void *thread_id) {
	char buffer[256];
	int n;
	
	pthread_mutex_lock(&socket_lock);
	//fprintf(stderr, "ffff\n");
	write(newsocket_control_loop, "what's up?", sizeof(char) * 9);
	pthread_mutex_unlock(&socket_lock);
	
	while(!thread_quit) {
		// Clear the buffer and read the message from the socket (from the server)
		bzero(buffer, 256);
		n = read(socket_vrpnhandler, buffer, 255);
		
		// If there was an error reading from the socket, throw an error
		if(n < 0) {
			fprintf(stderr, "ERROR reading from socket.\n");
			break;
			//error("ERROR reading from socket");
		}
		if(strcmp(buffer, "") == 0) {
			fprintf(stderr, "buffer is empty.\n");
			break;
		}
		
		// Print the VRPN data to the screen
		//fprintf(stderr, "%s", buffer);
		
		// Write to the socket so that control_loop can retrieve the buffer.
		
		pthread_mutex_lock(&socket_lock);
		n = write(newsocket_control_loop, buffer, sizeof(char) * 47);
		pthread_mutex_unlock(&socket_lock);
		if(n < 0) {
			fprintf(stderr, "ERROR writing to socket 2");
		}
	}

	pthread_exit(NULL);
}

int main(int argc, char *argvp[]) {
	int i = 0, thread_vrpn_data = 0;
	char a = 0;
	int socket_vrpnhandler_control_loop = 0;
	char buffer[256];
	
	pthread_t thread;
	
	socket_vrpnhandler = 0;
	vrpnhandler = 0;
	control_loop = 0;
	if (pthread_mutex_init(&socket_lock, NULL) != 0) {
		fprintf(stderr, "Failed to initialize mutex.\n");
		return 1;
	}
	
	// connect)zybo_bt.sh Script
	//ZB_MAC=00:06:66:64:61:D6
	//Attempt to release if exists
	//rfcomm release $ZB_MAC &>/dev/null
	//Attempt connection - this maps Bluetooth UART to /dev/rfcomm0
	//rfcomm connect 0 $ZB_MAC
	
	//pid_t connect_zybo_bt;
	//connect_zybo_bt = fork();
	//if(connect_zybo_bt == 0) {
	//	if(system("rfcomm release 00:06:66:64:61:D6 &>/dev/null") == -1) {
	//		fprintf(stderr, "Error releasing the Zybo board\n");
	//	}
	//	if(system("rfcomm connect 0 00:06:66:64:61:D6") == -1) {
	//		fprintf(stderr, "Error connecting to the Zybo board\n");
	//	}
		//while(1);
	//}
	//fprintf(stderr, "Connected to the Zybo board via Bluetooth!\n");
	//usleep(500000);
	// Set up signals for exiting the program and handling sockets, ports, threads
	//signal(SIGINT, signal_callback_handler);
	//signal(SIGQUIT, exit_handler);


	// Start vrpnhandler process
	vrpnhandler = fork();
	if(vrpnhandler == 0) {
		execv("../VRPN/vrpnhandler", NULL);
	}
	
	// Set up socket for communication with vrpnhandler
	socket_vrpnhandler = init_socket_client(SOCKET_PORT_1);
	fprintf(stderr, "Connected to socket on port %d\n", SOCKET_PORT_1);

	// Start control_loop process
	control_loop = fork();
	if(control_loop == 0) {
		execv("../main", NULL);
	}
		
	// Set up socket for communication with control_loop
	newsocket_control_loop = init_socket_server(SOCKET_PORT_2, socket_control_loop);
	fcntl(newsocket_control_loop, F_SETFL, O_NONBLOCK);
	fprintf(stderr, "Connected to socket on port %d\n", SOCKET_PORT_2);

	// Retrieve VRPN data until the process is killed
	fprintf(stderr, "Starting thread...\n");
	thread_vrpn_data = pthread_create(&thread, NULL, thread_forward_vrpn_data, NULL);
	fprintf(stderr, "Thread started.\n");
	
	usleep(1000000);

	char userCommand[CMD_MAX_LENGTH];
	int n;
	userCommand[0] = 0;
	fprintf(stderr, "$microcart> ");
	//fflush(stderr);
	while(fgets(userCommand, sizeof(userCommand), stdin) != NULL) {
		userCommand[strlen(userCommand) - 1] = 0;
		fprintf(stderr, "This was the command: %s\n", userCommand);
		fflush(stderr);
		
		//fprintf(stderr, "This was the command: %s\n", userCommand);
		
		//if(strncmp(userCommand, "quit", 4) == 0) {
		//	break;
		//}
		
		pthread_mutex_lock(&socket_lock);
		n = write(newsocket_control_loop, userCommand, sizeof(char) * strlen(userCommand));
		if(n < 0) {
			fprintf(stderr, "ERROR writing to socket\n");
		}
		//read(newsocket_control_loop, userCommand, sizeof(char) * strlen(userCommand));
		n = read(newsocket_control_loop, buffer, 256);
		//if(n < 0) {
		//	fprintf(stderr, "ERROR reading from socket\n");
		//}
		pthread_mutex_unlock(&socket_lock);
		
				// If there was an error reading from the socket, throw an error
		//if(n < 0) {
			//error("ERROR reading from socket");
			//fprintf(stderr, "ERROR reading from socket\n");
			//fflush(stderr);
		//}
		fprintf(stderr, "%s\n", buffer);
		
		//userCommand[0] = 0;		
		//while(returnToMenu == 0) {
		//	printf("Testing this...");
		//	usleep(1000000);
		//}
		//printf("Returning to menu. 2\n");
		//returnToMenu = 0;
		
		
		printf("$microcart> ");
		//fflush(stderr);
	}
	printf("Exiting the MicroCART Program. (2)\n");
	
	/*while(1) {
		printf("Boo yah!\n");
		usleep(100000);
	}*/

	
	// Kill the thread
	//pthread_exit(NULL);
	// Close the socket if not already closed
	//close_sockets();
	//exit_handler(0);
	return 0;
}

void close_sockets() {
	if(socket_vrpnhandler != 0) {
		close(socket_vrpnhandler);
		socket_vrpnhandler = 0;
	}
	if(socket_control_loop != 0) {
		close(socket_control_loop);
		socket_control_loop = 0;
	}
	if(newsocket_control_loop != 0) {
		close(newsocket_control_loop);
		newsocket_control_loop = 0;
	}
}

