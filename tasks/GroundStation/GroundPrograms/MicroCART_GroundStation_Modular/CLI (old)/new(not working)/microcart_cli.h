#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define SOCKET_PORT 50003
#define SOCKET_PORT_2 50004

int init_socket_vrpn();
int init_socket_control_loop(int *sockfd, int *newsockfd);

void error(const char *msg) {
	perror(msg);
	exit(1);
}

