#include "socket_utils.h"

/* 
 * This function will initialize a socket used mainly for writing to the socket.
 * A separate socket will be set up in init_socket_client to read from the socket.
 *
 * RETURNS: The new socket file descriptor
 */
int init_socket_server(int portNumber, int socket_fd) {
	socklen_t clilen;
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	int n, yes = 1, newsocket_fd;


	// Create the socket
	socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_fd < 0) {
		error("ERROR opening socket");
	}

	// Initialize the server information
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNumber);


	// Allow program to reuse TCP port if in use
	if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		error("getsockopt");
	}


	// Bind to the socket
	if(bind(socket_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		fprintf(stderr, "On port: %d,\n", portNumber);
		error("ERROR on binding");
	}


	// Start listening for connections on the socket
	// Backlog queue (number of connections waiting) is limited to 5
	listen(socket_fd, 5);


	// Blocks the process until a client connects
	clilen = sizeof(cli_addr);
	newsocket_fd = accept(socket_fd, (struct sockaddr *) &cli_addr, &clilen);
	if(newsocket_fd < 0) {
		error("ERROR on accept");
	}
	
	return newsocket_fd;
}

/*
 * This function will initialize a socket used mainly for reading from a socket.
 * A separate socket will be set up in init_socket_server to write to the socket.
 *
 * RETURNS: The socket file descriptor
 */
int init_socket_client(int portNumber) {
	int n;
	int socket_fd;
	struct sockaddr_in serv_addr;
	struct hostent *server;


	// Create socket
	socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	
	// If socket not created, throw error
	if(socket_fd < 0) {
		error("ERROR opening socket");
	}


	// Get the server name from the command arguments
	server = gethostbyname("127.0.0.1");
	// If the server name is not valid, throw error.
	if (server == NULL) {
		error("ERROR, no such host");
	}


	// Set memory to zero and initialize the server information
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(portNumber);


	while(connect(socket_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		//fprintf(stderr, "On port: %d,\n", portNumber);
		//error("ERROR connecting");
	}
	
	return socket_fd;
}

void error(const char *msg) {
	perror(msg);
	//fprintf(stderr, "%s\n", msg);
}

