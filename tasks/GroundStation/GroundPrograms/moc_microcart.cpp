/****************************************************************************
** Meta object code from reading C++ file 'microcart.h'
**
** Created: Wed Dec 2 18:17:39 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "GUI/MicroCART/microcart.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'microcart.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MicroCART[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x08,
      36,   10,   10,   10, 0x08,
      45,   10,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MicroCART[] = {
    "MicroCART\0\0on_startButton_clicked()\0"
    "update()\0updateGUI()\0"
};

void MicroCART::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MicroCART *_t = static_cast<MicroCART *>(_o);
        switch (_id) {
        case 0: _t->on_startButton_clicked(); break;
        case 1: _t->update(); break;
        case 2: _t->updateGUI(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData MicroCART::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MicroCART::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MicroCART,
      qt_meta_data_MicroCART, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MicroCART::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MicroCART::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MicroCART::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MicroCART))
        return static_cast<void*>(const_cast< MicroCART*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MicroCART::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
