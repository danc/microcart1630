#-------------------------------------------------
#
# Project created by QtCreator 2015-12-02T18:16:02
#
#-------------------------------------------------

QT       += core gui


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TestWithCCode
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    test.c

HEADERS  += mainwindow.h \
    test.h \
    main.h

FORMS    += mainwindow.ui
