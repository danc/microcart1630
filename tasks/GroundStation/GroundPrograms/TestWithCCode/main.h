#ifndef MAIN_H
#define MAIN_H

#ifdef __cplusplus
extern "C" {
    int main_cpp(int argc, char* argv[]);
}
#endif

#endif // MAIN_H
