#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "test.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    int something = 0;

    something = printSomething();

    if(something == 17) {
        ui->lineEdit->setText("17");
    } else {
        ui->lineEdit->setText("Booo");
    }

}

MainWindow::~MainWindow() {
    delete ui;
}


