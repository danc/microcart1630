#include <stdio.h>
#include "test.h"

int main(int argc, char* argv[]) {
#ifdef __cplusplus
    main_cpp(argc, argv);
#endif
#ifndef __cplusplus
    int something = 0;
    something = printSomething();
    printf("This is something: %d\n", 35);
#endif

	return 0;
}

int printSomething() {
    return 17;
}

