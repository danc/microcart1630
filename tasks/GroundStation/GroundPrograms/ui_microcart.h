/********************************************************************************
** Form generated from reading UI file 'microcart.ui'
**
** Created: Wed Dec 2 18:17:37 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MICROCART_H
#define UI_MICROCART_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MicroCART
{
public:
    QAction *actionVRPN_Camera_Data;
    QWidget *centralWidget;
    QPushButton *startButton;
    QTextEdit *textEdit;
    QTextEdit *textEdit_2;
    QTextEdit *textEdit_3;
    QTextEdit *textEdit_4;
    QTextEdit *textEdit_5;
    QTextEdit *textEdit_6;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QTextEdit *textEdit_7;
    QTextEdit *textEdit_8;
    QLabel *label_7;
    QLabel *label_8;
    QMenuBar *menuBar;
    QMenu *menuMicroCART;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MicroCART)
    {
        if (MicroCART->objectName().isEmpty())
            MicroCART->setObjectName(QString::fromUtf8("MicroCART"));
        MicroCART->resize(401, 299);
        actionVRPN_Camera_Data = new QAction(MicroCART);
        actionVRPN_Camera_Data->setObjectName(QString::fromUtf8("actionVRPN_Camera_Data"));
        centralWidget = new QWidget(MicroCART);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        startButton = new QPushButton(centralWidget);
        startButton->setObjectName(QString::fromUtf8("startButton"));
        startButton->setGeometry(QRect(150, 200, 101, 27));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(30, 30, 81, 31));
        textEdit_2 = new QTextEdit(centralWidget);
        textEdit_2->setObjectName(QString::fromUtf8("textEdit_2"));
        textEdit_2->setGeometry(QRect(160, 30, 81, 31));
        textEdit_3 = new QTextEdit(centralWidget);
        textEdit_3->setObjectName(QString::fromUtf8("textEdit_3"));
        textEdit_3->setGeometry(QRect(290, 30, 81, 31));
        textEdit_4 = new QTextEdit(centralWidget);
        textEdit_4->setObjectName(QString::fromUtf8("textEdit_4"));
        textEdit_4->setGeometry(QRect(30, 100, 101, 31));
        textEdit_5 = new QTextEdit(centralWidget);
        textEdit_5->setObjectName(QString::fromUtf8("textEdit_5"));
        textEdit_5->setGeometry(QRect(150, 100, 101, 31));
        textEdit_6 = new QTextEdit(centralWidget);
        textEdit_6->setObjectName(QString::fromUtf8("textEdit_6"));
        textEdit_6->setGeometry(QRect(270, 100, 101, 31));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(160, 10, 81, 17));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(30, 10, 91, 17));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(30, 140, 141, 17));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(290, 10, 81, 20));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(30, 80, 81, 17));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(230, 140, 141, 17));
        textEdit_7 = new QTextEdit(centralWidget);
        textEdit_7->setObjectName(QString::fromUtf8("textEdit_7"));
        textEdit_7->setGeometry(QRect(30, 160, 131, 31));
        textEdit_8 = new QTextEdit(centralWidget);
        textEdit_8->setObjectName(QString::fromUtf8("textEdit_8"));
        textEdit_8->setGeometry(QRect(230, 160, 141, 31));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(150, 80, 81, 17));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(270, 80, 81, 17));
        MicroCART->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MicroCART);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 401, 25));
        menuMicroCART = new QMenu(menuBar);
        menuMicroCART->setObjectName(QString::fromUtf8("menuMicroCART"));
        MicroCART->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MicroCART);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MicroCART->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MicroCART);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MicroCART->setStatusBar(statusBar);

        menuBar->addAction(menuMicroCART->menuAction());
        menuMicroCART->addAction(actionVRPN_Camera_Data);

        retranslateUi(MicroCART);

        QMetaObject::connectSlotsByName(MicroCART);
    } // setupUi

    void retranslateUi(QMainWindow *MicroCART)
    {
        MicroCART->setWindowTitle(QApplication::translate("MicroCART", "MicroCART", 0, QApplication::UnicodeUTF8));
        actionVRPN_Camera_Data->setText(QApplication::translate("MicroCART", "VRPN Camera Data", 0, QApplication::UnicodeUTF8));
        startButton->setText(QApplication::translate("MicroCART", "Start", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MicroCART", "update()", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MicroCART", "dataReceived", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MicroCART", "Connection Status", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MicroCART", "updateGUI()", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MicroCART", "x Position", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MicroCART", "VRPN Time", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MicroCART", "y Position", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("MicroCART", "z Position", 0, QApplication::UnicodeUTF8));
        menuMicroCART->setTitle(QApplication::translate("MicroCART", "MicroCART", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MicroCART: public Ui_MicroCART {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MICROCART_H
