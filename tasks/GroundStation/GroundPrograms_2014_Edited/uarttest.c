#include <stdio.h>		/* Standard input/output definitions */
#include <string.h>		/* String function definitions */
#include <unistd.h>		/* UNIX standard function definitions */
#include <fcntl.h>		/* File control definitions */
#include <errno.h>		/* Error number definitions */
#include <termios.h>	/* POSIX terminal control definitions */

// Use ascii 2 and 3 for start/end of packet
#define  PACKET_START_BYTE 2
#define  PACKET_END_BYTE 3

int uart_fd;

void setup() {
uart_fd = open("/dev/rfcomm0", O_RDWR | O_NDELAY);
	if (uart_fd  == -1) {
		// Could not open the port.
		perror("open_port: Unable to open /dev/ttyS0 - ");
	} else {
		fcntl(uart_fd , F_SETFL, 0);
	}

	struct termios options; // UART port options data structure

	// Get the current options for the port...
	tcgetattr(uart_fd, &options);

	// Set the baud rates to 9600...
	cfsetispeed(&options, B38400);
	cfsetospeed(&options, B38400);

	// Enable the receiver and set local mode...
	options.c_cflag |= (CLOCAL | CREAD);

	// Set charater size
	options.c_cflag &= ~CSIZE; // Mask the character size bits
	options.c_cflag |= CS8;    // Select 8 data bits

	// Set no parity 8N1
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;

	// Disable Hardware flow control
	options.c_cflag &= ~CRTSCTS;

	// Use raw input
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

	// Disable SW flow control
	options.c_iflag &= ~(IXON | IXOFF | IXANY);

	// Use raw output
	options.c_oflag &= ~OPOST;

	// Set new options for the port...
	tcsetattr(uart_fd , TCSANOW, &options);
}

int main(int argc, char *argv[]) {
	setup();
	char data[100];
	data[0] = PACKET_START_BYTE;
	data[1] = 'C';
	data[2] = 'h';
	data[3] = 'e';
	data[4] = 'l';
	data[5] = 'l';
	data[6] = 'o';
	data[7] = PACKET_END_BYTE;
	int length = 8;
	write(uart_fd, data, length);
	printf("wrote\r\n");

	char resp[100];	
	while(1) {
		read(uart_fd, resp, 1);
		printf("GOT: %c %d\r\n", resp[0]);
	}
	
	return 0;
}
