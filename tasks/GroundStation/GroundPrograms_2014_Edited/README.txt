Microcart Senior Design at Iowa State
Ground Station Programs


See operations manual: GroundPrograms/MicroCART-ground-station-operations.docx


##
## TO PAIR WITH THE PMOD BLUETOOTH MODULE ON THE QUAD (ITS DEVICE ID IS ZYBO_BT):
##   1. open bluetooth preferences
##   2. click set up new device
##   3. select zybo_bt (00:06:66:64:61:D6)
##   4. choose custom PIN and type in 1234 (do NOT select 1234 from the
##         list, you need to choose custom PIN and type in 1234)
##   5. the pmod bluetooth module is now paired, but still needs to be connected using
##         the ./connect_zybo_bluetooth.sh script
##


