#include <stdio.h>		/* Standard input/output definitions */
#include <string.h>		/* String function definitions */
#include <unistd.h>		/* UNIX standard function definitions */
#include <fcntl.h>		/* File control definitions */
#include <errno.h>		/* Error number definitions */
#include <ctype.h>
#include <termios.h>	/* POSIX terminal control definitions */
#include "uartutils.h"

int uart_fd;

int main(int argc, char *argv[]) {
	setvbuf(stdout, NULL, _IONBF, 0);

	fprintf(stderr, "Starting...\r\n");
	int ok = uartInit("/dev/rfcomm0");
	if(!ok) { 
		fprintf(stderr, "Error connecting.. Aborting.\r\n"); 
		return 1; 
	}
	fprintf(stderr, "Connected OK\r\n");
	fprintf(stderr, "Begin log\r\n\r\n");

	char c;
	while(1) {
		// read char
		uartRead(&c, 1);
				
		if (isprint(c) || isspace(c)) {
			printf("%c", c);
		}
	}

	return 0;
}
