#include "uartutils.h"
#include <stdio.h> /* Standard input/output definitions */
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h> /* File control definitions */
#include <errno.h> /* Error number definitions */
#include <ctype.h>
#include <termios.h> /* POSIX terminal control definitions */

int uart_fd;

int uartInit(char const* device) {
	uart_fd = open(device, O_RDWR | O_NDELAY);
	if (uart_fd  == -1)
	{
		//Could not open the port.
		perror("open_port: Unable to open device.");
		return 0;
	}
	else
	{
	  fcntl(uart_fd , F_SETFL, 0);
	}


	struct termios options; // UART port options data structure

	// Get the current options for the port...
	tcgetattr(uart_fd, &options);

	// Set the baud rates to 9600...
	cfsetispeed(&options, B38400);
	cfsetospeed(&options, B38400);

	// Enable the receiver and set local mode...
	options.c_cflag |= (CLOCAL | CREAD);

	// Set charater size
	options.c_cflag &= ~CSIZE; // Mask the character size bits
	options.c_cflag |= CS8;    // Select 8 data bits

	// Set no parity 8N1
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;

	// Disable Hardware flow control
	options.c_cflag &= ~CRTSCTS;

	// Use raw input
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

	// Disable SW flow control
	options.c_iflag &= ~(IXON | IXOFF | IXANY);

	// Use raw output
	options.c_oflag &= ~OPOST;

	// Set new options for the port...
	tcsetattr(uart_fd , TCSANOW, &options);

	return 1;
}
  


int uartWrite(unsigned char* buf, int len) {
	int done = 0;
	while(done < len) {
		done += write(uart_fd, &buf[done], len - done);
	}
}

int uartRead(unsigned char* buf, int len) {
	return read(uart_fd, buf, len);
} 
  
