#!/bin/bash
#########
# Shell script to connect to zybo_bt (the PMOD bluetooth module). It should already be paired
#########
# This script just establishes the connection; it doesn't open a terminal or anything. 
# To do this, go to another terminal window and run 
# $> sudo screen /dev/rfcomm0 921600
# Bluetooth speed is configurable, max is 921600. Was 115200
# Bluetooth modle is RN-42
# Datasheets can be found at 
# https://www.sparkfun.com/datasheets/Wireless/Bluetooth/rn-42-ds.pdf
# and
# ww1.microchip.com/downloads/en/DeviceDoc/bluetooth_cr_UG-v1.0r.pdf
#########

##
## TO PAIR WITH ZYBO_BT:
##	1. Open Bluetooth preferences
##	2. Click set up new device
##	3. Select zybo_bt (00:06:66:64:61:D6)
##	4. Choose custom PIN and type in 1234 (do NOT select 1234 from the
##		list, you need to choose custom PIN and type in 1234)
##

# Must be root
if [[ $EUID -ne 0 ]]; then
	echo "You must be a root user" 2>&1
	exit 1
fi


# MAC Address of zybo_bt (can find with `hcitool scan`)
ZB_MAC=00:06:66:64:61:D6

# Attempt to release if exists
rfcomm release $ZB_MAC &>/dev/null

# Attempt connection - this maps Bluetooth UART to /dev/rfcomm0
rfcomm connect 0 $ZB_MAC
