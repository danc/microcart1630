-- before we added IMU offsets------------------------------------------------------------------
-- this test was done with the quad being complete motionless and flat on the carpet -----------
-- assuming that this should be 0 degrees, we should consider adding a small offset ------------

*** averages from the data below:
***	accel_x: -0.0226	accel_y: -0.0087	accel_z: -1.087	gyro_x: 0.0068	gyro_y: -0.0051	gyro_z: 0.0046***

accel_x: -0.023	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.024	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.008	accel_z: -1.089	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.010	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.021	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.024	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.008	accel_z: -1.089	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.085	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.006	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.008	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.024	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.024	accel_y: -0.010	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.024	accel_y: -0.009	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.024	accel_y: -0.010	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.008	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.006	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.008	accel_z: -1.089	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.024	accel_y: -0.008	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.009	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.024	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.006	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.006	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.006	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.024	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.007	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.089	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.006	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.010	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.007	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.007	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.010	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.086	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.024	accel_y: -0.010	accel_z: -1.086	gyro_x: 0.006	gyro_y: -0.006	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.006	gyro_z: 0.005

accel_x: -0.024	accel_y: -0.010	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.021	accel_y: -0.008	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.004	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.010	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.008	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.089	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.008	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.086	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.024	accel_y: -0.009	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.008	accel_z: -1.088	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.021	accel_y: -0.008	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.006	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.010	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.010	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.008	accel_z: -1.089	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.008	accel_z: -1.089	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.006	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.024	accel_y: -0.010	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.006	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.010	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.086	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.008	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.010	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.008	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.089	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.007	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.006	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.021	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.006	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.010	accel_z: -1.089	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.010	accel_z: -1.088	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.008	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.008	accel_z: -1.086	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.022	accel_y: -0.009	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.007	accel_z: -1.088	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.010	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.007	accel_z: -1.088	gyro_x: 0.006	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.008	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

accel_x: -0.022	accel_y: -0.009	accel_z: -1.089	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.005

accel_x: -0.023	accel_y: -0.009	accel_z: -1.087	gyro_x: 0.007	gyro_y: -0.005	gyro_z: 0.004

