#include <stdio.h>		/* Standard input/output definitions */
#include <string.h>		/* String function definitions */
#include <stdlib.h>
#include <unistd.h>		/* UNIX standard function definitions */
#include <fcntl.h>		/* File control definitions */
#include <errno.h>		/* Error number definitions */
#include <termios.h>	/* POSIX terminal control definitions */
#include <sys/stat.h>	/* For mode constants */
#include <semaphore.h>
#include <signal.h>
#include "uartutils.h"
#include "sync.h"

/*
 * On the whole semaphore business:
 *		It's suspected there may be issues calling the write() system call
 *    from multiple threads to write to the same uart file descriptor for 
 *    the bluetooth. In the interest of correct concurrency and avoiding 
 *    race conditions, the capability is provided to use a semaphor as a mutex
 *    to keep only one thread calling write() at a time. However, testing has 
 *    shown this may not be necessary so it is disabled at this time. If you
 *    wish to enable it, change the UART_USE_SEM value to 1 in "sync.h".
 */
#if UART_USE_SEM
sem_t *sem;
#endif

#define  CMD_MAX_LENGTH  1024 

// send a line as a command packet to the quad
void sendCommand(char* line) {
 	// overwrite the newline at the end with the packet end byte
 	int lineLength = strlen(line);
 	int cmdLength = lineLength + 3; // start byte, 'C', end byte
 	char cmdBuf[CMD_MAX_LENGTH+3] = {};
 	
 	// set up packet 
 	cmdBuf[0] = PACKET_START_BYTE;
 	cmdBuf[1] = 'C'; // C for command, U for update
 	strcpy(&cmdBuf[2], line);
 	cmdBuf[cmdLength - 1] = PACKET_END_BYTE; 


#if UART_USE_SEM
	sem_wait(sem);
#endif
	/* Begin Critical section */
	{
		uartWrite(cmdBuf, cmdLength);
	}
	/* End Critical section */
#if UART_USE_SEM
	sem_post(sem);
#endif
}

int main(int argc, char *argv[]) {
#if UART_USE_SEM
	/* try to open a preexisting semaphore. */
	sem = sem_open(SEM_NAME, O_CREAT, 0644, 1); 
#endif 

	// Initialize stuff
	printf("Starting...\r\n");
	int ok = uartInit("/dev/rfcomm0");
	if (!ok) {
		printf("Error connecting... Aborting.\r\n");
		return 1; 
	}
	printf("Connected to /dev/rfcomm0 successfully.\r\n\r\n");
   
	char line[CMD_MAX_LENGTH] = {};
	printf("$> ");
	// Get line from stdin
 	while (fgets(line, CMD_MAX_LENGTH, stdin) != NULL) {
		// send command
		sendCommand(line);
		printf("$> ");
	}
	
#if UART_USE_SEM
	sem_close(sem);
#endif

	return 0;
}
