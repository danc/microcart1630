#ifndef _COMMANDS_H
#define _COMMANDS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "type_def.h"

// ----------------------
// Helper stuff

#define MAX_TYPE 6
#define MAX_SUBTYPE 100

enum Message{
	BEGIN_CHAR = 0xBE,
	END_CHAR   = 0xED
};

// This should also have double to avoid confusion with float values.
enum DataType
{
	floatType,
	intType,
	stringType
};

// MESSAGE SUBTYPES
struct MessageSubtype{
	char ID;
	char cmdText[100];
	char cmdDataType;
	int (*functionPtr)(unsigned char *command, int dataLen, modular_structs_t *structs);
};

// MESSAGE TYPES
struct MessageType{
	char ID;
	struct MessageSubtype subtypes[MAX_SUBTYPE];
};

int debug(unsigned char *c, int dataLen, modular_structs_t *structs);
int update(unsigned char *c, int dataLen, modular_structs_t *structs);
int logdata(unsigned char *c, int dataLen, modular_structs_t *structs);
int response(unsigned char *packet, int dataLen, modular_structs_t *structs);
int yawset(unsigned char *c, int dataLen, modular_structs_t *structs);
int yawp(unsigned char *c, int dataLen, modular_structs_t *structs);
int yawd(unsigned char *c, int dataLen, modular_structs_t *structs);
int rollset(unsigned char *c, int dataLen, modular_structs_t *structs);
int rollp(unsigned char *c, int dataLen, modular_structs_t *structs);
int rolld(unsigned char *c, int dataLen, modular_structs_t *structs);
int pitchset(unsigned char *c, int dataLen, modular_structs_t *structs);
int pitchp(unsigned char *c, int dataLen, modular_structs_t *structs);
int pitchd(unsigned char *c, int dataLen, modular_structs_t *structs);
int throttleset(unsigned char *c, int dataLen, modular_structs_t *structs);
int throttlep(unsigned char *c, int dataLen, modular_structs_t *structs);
int throttlei(unsigned char *c, int dataLen, modular_structs_t *structs);
int throttled(unsigned char *c, int dataLen, modular_structs_t *structs);
int accelreq(unsigned char *c, int dataLen, modular_structs_t *structs);
int gyroresp(unsigned char *c, int dataLen, modular_structs_t *structs);
int pitchangleresp(unsigned char *c, int dataLen, modular_structs_t *structs);
int rollangleresp(unsigned char *c, int dataLen, modular_structs_t *structs);
int gyroreq(unsigned char *c, int dataLen, modular_structs_t *structs);
int pitchanglereq(unsigned char *c, int dataLen, modular_structs_t *structs);
int rollanglereq(unsigned char *c, int dataLen, modular_structs_t *structs);
int accelresp(unsigned char *c, int dataLen, modular_structs_t *structs);

// TODO add in string to be read from the command line when sending a subtype of message
extern struct MessageType MessageTypes[MAX_TYPE];

#endif
