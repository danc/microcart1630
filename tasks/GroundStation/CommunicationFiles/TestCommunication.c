#include <stdio.h>
#include "communication.h"
#include "commands.h"
#include "type_def.h"

void printHelpMenu() {
	int type, subtype;
	
	printf("Available Commands:\n");
	
	for(type = 0; type < MAX_TYPE; type++) {
		for(subtype = 0; subtype < MAX_SUBTYPE; subtype++) {
			if(strcmp(MessageTypes[type].subtypes[subtype].cmdText, "")) {
				printf("\t%s\n", MessageTypes[type].subtypes[subtype].cmdText);
			}
		}
	}
}

int main(int argc, char *argv[]) {
	unsigned char line[100];
	unsigned char *formattedCommand;
	int error;
	
	modular_structs_t modular_structs = {};
	
	printf("$> ");
	// Get line from stdin
	while(fgets(line, sizeof(line), stdin) != NULL) {
		if(strcmp(line, "help\n") == 0) {
			// Print the help menu
			printHelpMenu();
		} else if(strcmp(line, "quit\n") == 0) {
			// Exit the program
			break;
		} else {
			// Send command
			error = formatCommand(line, &formattedCommand);
			if(error) {
				printf("\nInvalid command\n\n");
			} else {
				error = processCommand(formattedCommand, &modular_structs);
				
				if(error) {
					printf("\nUnable to process packet\n\n");// %s\n\n", formattedCommand);
				} else {
					printf("Successfully processed packet!\n");
				}
			}
		}

		printf("$> ");
	}
	
	// ------------------------------------------------
	// Mock VRPN Format
	char update[28];
	int packetId = 13;
	float y_pos = 0.87;
	float x_pos = 1.9;
	float alt_pos = 0.23;
	float roll = 0.74;
	float pitch = 1.3;
	float yaw = 7.2;
	
	memcpy(&(update[0]), &packetId, sizeof(int));
	memcpy(&update[4], &y_pos, sizeof(float));
	memcpy(&update[8], &x_pos, sizeof(float));
	memcpy(&update[12], &alt_pos, sizeof(float));
	memcpy(&update[16], &roll, sizeof(float));
	memcpy(&update[20], &pitch, sizeof(float));
	memcpy(&update[24], &yaw, sizeof(float));
	
	metadata_t metadata = 
	{
		BEGIN_CHAR,
		MessageTypes[4].ID,
		MessageTypes[4].subtypes[0].ID,
		packetId,
		sizeof(update)
	};
	
	error = formatPacket(&metadata, &update, &formattedCommand);
	
	if(error) {
		printf("\nInvalid command\n\n");
	}else{
		//printf("Successfully formatted command! %s\n", formattedCommand);
	}

	error = processCommand(formattedCommand, &modular_structs);
	
	if(error) {
		printf("\nUnable to process packet\n\n");// %s\n\n", formattedCommand);
	}else{
		printf("Successfully processed packet!\n");
	}
	
	return 0;
}
