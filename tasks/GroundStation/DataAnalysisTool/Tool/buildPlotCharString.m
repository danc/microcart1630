function plotCharStr = buildPlotCharString(plotParams)
%buildPlotCharString builds a character string for plotting using the
%plotParams input structure
%   plotParams : structure that contains the plotting style parameters.
%       fields of plotParams:
%           plotParams.plot
%           plotParams.style
%           plotParams.color
%           plotParams.marker
%           plotParams.backgnd
%           

% keeps track of the index in the string
i = 1;

% color
if(~isempty(plotParams.color))
    plotCharStr(i) = plotParams.color;
    i = i + 1;
end

% marker
if(~isempty(plotParams.marker))
    plotCharStr(i) = plotParams.marker;
    i = i + 1;
end

% style
if(~isempty(plotParams.style))
    plotCharStr(i) = plotParams.style;
    i = i + 1;
end

% sanity check for the string
if(~isPlotCharString(plotCharStr))
    error('Wrong plot style formatting options, please use allowed values only');
end

end

