#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <time.h>
#include <string.h>

#include "ticToc.h"

#define MAX_NUM_DATA 100

//#define TESTING_CPTR_TO_QUAD
//#define TESTING_QUAD_TO_CPTR
//#define TESTING_BOTH_DIRECTIONS
#define TESTING_ROUND_TRIP

void error(char *msg)
{
    perror(msg);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n, i, iterations, past_iterations;

    struct sockaddr_in serv_addr;
    struct hostent *server;
    
    clock_t begin, end;
    double deltT();

    char buffer[2] = "i";
    char recv_buffer[11];
    char start_buffer[2] = "s";
    char ready_buffer[8] = "ready\n\r";
    
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       return 0;
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
    {
        error("ERROR opening socket");
        return 0;
    }
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        return 0;
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
    {
        error("ERROR connecting");
        return 0;
    }

    // Throughput testing
    sleep(3);
    n = write(sockfd,ready_buffer,strlen(ready_buffer));
    printf("Sent:%s\n", ready_buffer);
    if (n < 0) 
    {
        error("ERROR writing to socket");
        return 0;
    }

    
#ifdef TESTING_CPTR_TO_QUAD
    printf("Testing computer to quad\n\n");
    printf("BYTES, SECONDS\n");
    n = write(sockfd,ready_buffer,strlen(ready_buffer));
    if (n < 0) 
    {
        error("ERROR writing to socket");
        return 0;
    }
    
    for(iterations = 1; iterations < MAX_NUM_DATA; iterations += past_iterations)
    {
        past_iterations = iterations - past_iterations;
        n = write(sockfd,start_buffer,strlen(start_buffer));
        if (n < 0) 
        {
            error("ERROR writing to socket");
            return 0;
        }
        tic();
        for(i = 0; i < iterations; i++)
        {
            n = write(sockfd,buffer,strlen(buffer));
            if (n < 0) 
            {
                error("ERROR writing to socket");
                return 0;
            }
            
        }
        toc();
        
        
        printf("%d, %lf\n", i, deltT());
    }
#endif

#ifdef TESTING_QUAD_TO_CPTR
    printf("Testing quad to computer\n\n");
    printf("BYTES, SECONDS\n");
    iterations = 1;
    while(1)
    {
        n = read(sockfd,buffer,255);
        if (n < 0) 
        {
            error("ERROR reading from socket");
            return 0;
        }
        
        for(i = 0; i < n; i++)
        {
            switch (buffer[i])
            {
                case 's':
                    // Time processing for the past iteration
                    if(!first)
                    {
                        deltT() = ((double)(end - begin)) / CLOCKS_PER_SEC;
                        printf("%d, %.3lf\n", iterations, deltT());
                    }
                    first = 0;
                    
                    if (num_received != iterations)
                    {
                        printf("Incorrect number %d received on %d iterations.\n", num_received, iterations);
                    }
                    iterations += past_iterations;
                    if(iterations > MAX_NUM_DATA)
                    {
                        done = 1;
                    }
                    past_iterations = iterations - past_iterations;
                    num_received = 0;
                    //printf("Iterations: %d\n", iterations);
                    //fflush(stdout);
                    
                    // Record the start time to receive the data
                    tic();
                    break;
                case 'q':
                    num_received ++;
                    if (num_received > iterations)
                    {
                        printf("Incorrect number %d received on %d iterations.\n", num_received, iterations);
                    }
                    else if (num_received == iterations)
                    {
                        toc();
                    }
                    break;
                case '\n':
                case '\r':
                    break;
                default:
                    printf("Invalid character received: %x\n", buffer[i]);
                    break;
            }
            
            if (done)
                break;
        }
        if (done)
            break;
    }
#endif

#ifdef TESTING_BOTH_DIRECTIONS
    printf("Testing both directions\n\n");
    printf("Please enter the message: ");
    bzero(buffer,256);
    fgets(buffer,255,stdin);
    n = write(sockfd,buffer,strlen(buffer));
    if (n < 0) 
    {
        error("ERROR writing to socket");
        return 0;
    }
    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0) 
    {
        error("ERROR reading from socket");
        return 0;
    }
    printf("%s\n",buffer);
#endif

#ifdef TESTING_ROUND_TRIP
    printf("Testing round trip\n\n");
    tic();
    //for(i = 0; i < MAX_NUM_DATA; i++)
    //{
        n = write(sockfd,buffer,strlen(buffer));
        if (n < 0) 
        {
            error("ERROR writing to socket");
            return 0;
        }
        //printf("wrote\n");
        do
        {
            n = read(sockfd,recv_buffer,1);
        }while(n <= 0 || recv_buffer[0] != 'i');
        //printf("read %d\n", i);
    //}
    toc();
    
    double total_time = deltT();
    printf("Round trip time: %lf : %lf per round trip\n", total_time, (total_time - 640)/1000000);///MAX_NUM_DATA);
    
    return 0;
#endif
}