This directory holds the code to communicate with the WiFi test in module_access_point_2. 

The makefile can be used to compile the throughput_test with the ticToc code. 

TicToc code was obtained from RADA and converted to C from C++. 

Only the round trip throughput test was fully implemented in throughput_test.c.

The test can be run in the following way:

./throughput 192.168.4.1 1336

192.168.4.1 - WiFi module's IP address
1336 - port the WiFi module is communicating on
