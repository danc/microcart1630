/*

 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *



 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
*/

#include <stdio.h>
#include <stdlib.h>
//#include "platform.h"
#include "xil_printf.h"
#include "xuartps.h"
#include "sleep.h"
#include "WIFI.h"

int main()
{
    XUartPs uart0;
    // int *sw_ptr = (int *)XPAR_SWS_8BITS_BASEADDR;
	xil_printf("WiFi Initialize starting.\n");

    // Initalize the WiFi
    if(wifi_init(&uart0) < 0)
    {
    	xil_printf("WiFi Initialize failed.\n");
    //	return -1;
    }

    // Receive data until an s is sent to send a message to connection
    //char recv_buffer[500];

    //while(recv_buffer[0] != 's')
    //{
    	//wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 2);
    	// Get ready to send data
        char * str = "hello";
    	wifi_send_str("AT+CIPSEND=0,6");
    	wifi_recieve_msg(">", 1);
    	wifi_send_str(str);
    	int error = wifi_recieve_msg("hello", 5);
    	if(error == 0) xil_printf("\r\n\r\nReceived test!\r\n\r\n");
    //}
//
   /* // Send beginning message
    char * str = "hello";
	wifi_send_str("AT+CIPSEND=0,7");
	wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);
	sleep(1);
	wifi_send_str(str);

    while(1)
	{
		wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 2);
	}

    // Don't send a message until switch 0 is on
    /*while((*sw_ptr & 0x1) == 0)
    {

    	wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 2);

    }

	// Get ready to send data
    char * str = "hello";
	wifi_send_str("AT+CIPSEND=0,7");
	wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);
	sleep(1);
	wifi_send_str(str);

	// use sw7 to end the program
	while((*sw_ptr & 0x80) == 0)
	{
		wifi_send_str("AT+CIPSEND=0,4");
		wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);
		sleep(1);
		wifi_send_str(image_ptr);

		wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 2);

	}*/

    xil_printf("\n\rexiting program\n\r");
    return 0;
}
