/*
 * WIFI.h
 *
 *  Created on: Dec 5, 2015
 *      Author: seiberta
 */

#ifndef WIFI_H_
#define WIFI_H_

#include <stdio.h>
#include "xil_printf.h"
#include "xuartps.h"
#include "sleep.h"
#include <stdlib.h>

int wifi_init(XUartPs * returned_instance);
int setup_commands();
int wifi_recieve_msg(char* msg, int length);
int msg_found(char* msg, int length, char recv_buffer[]);
void uart0_config(XUartPs_Config *config, XUartPs *InstancePtr);
int wifi_recv(u32 base_address, char *buffer, u16 buffer_size);
int wifi_send(u32 base_address, char *buffer, u16 buffer_size);
int wifi_send_str(char *buffer);



#endif /* WIFI_H_ */
