/*

 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *



 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
*/


#include"WIFI.h"

int wifi_init(XUartPs *returned_instance)
{
	sleep(5);
	xil_printf("STARTING WIFI INIT\n\r");
    XUartPs_Config * config = XUartPs_LookupConfig(XPAR_PS7_UART_0_DEVICE_ID);

    if(config == NULL)
    {
    	xil_printf("failed config.\n");
    	return -1;
    }
    if(XUartPs_CfgInitialize(returned_instance, config, config->BaseAddress) != XST_SUCCESS)
    {
    	xil_printf("failed init.\n");
    	return -1;
    }

	uart0_config(config, returned_instance);

    XUartPs_EnableUart(returned_instance);
    
	int error = 0;
    error = setup_commands();

    xil_printf("FINISHED WIFI INIT\n\r\r");
    return error;
}

int setup_commands()
{

	char recv_buffer[500];
	int error = 0;


    xil_printf("Starting setup commands\n\r\r");

	// Confirm it is responding
	/*error += wifi_recieve_msg("\r\nOK\r\n", 6);
	wifi_send_str("AT");
	error += wifi_recieve_msg("\r\nOK\r\n", 6);
    xil_printf("Error = %d\n\r\r", error);

	// Reset the WiF module
	wifi_send_str("AT+RST");
	error += wifi_recieve_msg("\r\nOK\r\n", 6);
    xil_printf("Error = %d\n\r\r", error);

	// Get the firmware version
	wifi_send_str("AT+GMR");
	error += wifi_recieve_msg("\r\nOK\r\n", 6);
    xil_printf("Error = %d\n\r\r", error);

	// Set as access point
	wifi_send_str("AT+CWMODE=3");
	error += wifi_recieve_msg("\r\nOK\r\n", 6);
    xil_printf("Error = %d\n\r\r", error);

	// Clear current WiFi network
	wifi_send_str("AT+CWQAP");
	//error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500); // Expected to return ERROR or OK
	error += wifi_recieve_msg("\r\nOK\r\n", 6);
    xil_printf("Error = %d\n\r\r", error);
	//sleep(1);

	// List WiFi networks
	//wifi_send_str("AT+CWLAP");
	//error += wifi_recieve_msg("\r\nOK\r\n", 6);
	error += wifi_recieve_msg("WIFI CONNECTED\r\n", 16);
    xil_printf("Error = %d\n\r\r", error);

	// Join microcart wifi
	//wifi_send_str("AT+CWJAP=\"microcart\",\"microcart\"");
	//error += wifi_recieve_msg("WIFI CONNECTED\r\n", 16);
	//error += wifi_recieve_msg("WIFI GOT IP\r\n", 13);
	//error += wifi_recieve_msg("OK\r\n", 4);
    //xil_printf("Error = %d\n\r\r", error);
*/
	// List the IP
	wifi_send_str("AT+CIFSR");
	error += wifi_recieve_msg("\r\nOK\r\n", 6);
 /*   xil_printf("Error = %d\n\r\r", error);

	// Enable multiple connections
	wifi_send_str("AT+CIPMUX=1");
	error += wifi_recieve_msg("\r\nOK\r\n", 6);
    xil_printf("Error = %d\n\r\r", error);

	// Channel connection in UDP
	wifi_send_str("AT+CIPSTART=2,\"UDP\",\"192.168.123.1\",5001");
	error += wifi_recieve_msg("\r\nOK\r\n", 6);
    xil_printf("Error = %d\n\r\r", error);*/

    return error;
}

int wifi_recieve_msg(char* msg, int length)
{
    int found_msg = 0, max_num_failed = 30, num = 0, error = 0, msg_index = 0, i = 0, found = 0;
	char recv_buffer[length + 1];


    xil_printf("%s n\r\r", msg);
    
    // Looks for the msg
    while(!found_msg && num < max_num_failed)
    {
        
        error = wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, length);

        // Only increment num if it was unable to recieve the data
        if(error)
        {
            num++;
        }
        
        // Look if the msg was recieved
		// Process each character
        else
        {
			for(i = 0; i < length; i++)
			{
				if(recv_buffer[i] != 0)
				{
					if(!found && recv_buffer[i] == msg[msg_index])
					{
						msg_index++;
						found = 1;
						if(msg_index == length)
						{
							found_msg = 1;
							break;
						}
					}
					if(found)
					{
						if(recv_buffer[i] == msg[msg_index])
						{
							msg_index++;
						}
						else
						{
							found = 0;
						}

						if(msg_index == length)
						{
							found_msg = 1;
							break;
						}
					}
				}
				else
				{
					num++;
				}
			}
        }

    }
    
    // Return error if it timed out
    if( num == max_num_failed)
    {
        return -1;
    }
    
    // Only gets here if a valid message was found
    return 0;
}

int msg_found(char* msg, int length, char recv_buffer[])
{
    int recv_index = 0, start_index = -1, i = 0;
    
    // Loop through recv_buffer looking for the first character of msg
    for(recv_index = 0; recv_index < length; recv_index++)
    {
        // Found the first character of msg
        if(msg[0] == recv_buffer[recv_index])
        {
            // Check if the rest of the messge is in recv_buffer
            for(i = 1; i < length && recv_index + i < length; i++)
            {
                if(msg[i] != recv_buffer[recv_index + i])
                {
                    break;
                }
            }
            
            // Check if the loop above completed and if it did return the start index
            if(i == length || recv_index + i == length)
            {
                start_index = recv_index;
                break;
            }
        }
    }
    
    return start_index;
}

void uart0_config(XUartPs_Config *config, XUartPs *InstancePtr)
{
    u32 * uart_control_reg = (u32*) XPAR_PS7_UART_0_BASEADDR;
    *uart_control_reg |= 3; // resets the transmission and receive
}

int wifi_send_str(char *buffer)
{
	// Send the message
	int error = wifi_send(XPAR_PS7_UART_0_BASEADDR, buffer, strlen(buffer));
	// Add /r/n at end
	char ending[2] = {0x0d, 0x0a};
	error += wifi_send(XPAR_PS7_UART_0_BASEADDR, ending, 2);
	return error;
}

int wifi_recv(u32 base_address, char *buffer, u16 buffer_size)
{
	if (buffer == NULL || buffer_size < 2)
	{
		return -1;
	}

    u32 i = 0, num_failed = 0, max_num_failed = 500000;
    while(!XUartPs_IsReceiveData(base_address) && num_failed < 10 * max_num_failed)
    {
    	usleep(1);
		num_failed++;
    }
	if(num_failed >= 10 * max_num_failed)
	{
		buffer[0] = 0;
		return -1;
	}
	for (i = 0; i < buffer_size-1; i++)
	{
		num_failed = 0;
		while(!XUartPs_IsReceiveData(base_address) && num_failed < max_num_failed)
		{
			num_failed++;
			usleep(1);
		}
		if(num_failed >= max_num_failed)
			break;
		buffer[i] = XUartPs_RecvByte(base_address);
		xil_printf("%c", buffer[i]);
	}
    buffer[i] = 0;
    return 0;
}

int wifi_send(u32 base_address, char *buffer, u16 buffer_size)
{
	if (buffer == NULL || buffer_size < 1)
	{
		return -1;
	}

    u16 i;
    for (i=0; i < buffer_size; i++)
    {
    	while (XUartPs_IsTransmitFull(base_address));
    	XUartPs_SendByte(base_address, buffer[i]);
    }
    return 0;
}

