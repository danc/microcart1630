/*

 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *



 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
*/


#include"WIFI.h"

int wifi_init(XUartPs *returned_instance)
{
	xil_printf("STARTING WIFI INIT\n\r");
    XUartPs_Config * config = XUartPs_LookupConfig(XPAR_PS7_UART_0_DEVICE_ID);

    if(config == NULL)
    {
    	xil_printf("failed config.\n");
    	return -1;
    }
    if(XUartPs_CfgInitialize(returned_instance, config, config->BaseAddress) != XST_SUCCESS)
    {
    	xil_printf("failed init.\n");
    	return -1;
    }

	uart0_config(config, returned_instance);

    XUartPs_EnableUart(returned_instance);

	char recv_buffer[500];
	int error = 0;

	// Confirm it is responding
	wifi_send_str("AT");
	error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);

	// Reset the WiFi module
	wifi_send_str("AT+RST");
	error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);

	// Get the firmware version
	wifi_send_str("AT+GMR");
	error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);

	// Set as access point
	wifi_send_str("AT+CWMODE=3");
	error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);

	// Reset the WiF module
	wifi_send_str("AT+RST");
	error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);

	// Set as access point
	wifi_send_str("AT+CWMODE=2");
	error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);

	// List IP addresses
	wifi_send_str("AT+CWLIF");
	error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);

	// Set it as a multiple channel connection
	wifi_send_str("AT+CIPMUX=1");
	error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);

	// List the IP
	wifi_send_str("AT+CIFSR");
	error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);

	// Get module to listen
	wifi_send_str("AT+CIPSERVER=1,1336");
	error += wifi_recv(XPAR_PS7_UART_0_BASEADDR, recv_buffer, 500);

    xil_printf("FINISHED WIFI INIT\n\r\r");
    return error;
}

void uart0_config(XUartPs_Config *config, XUartPs *InstancePtr)
{
    u32 * uart_control_reg = (u32*) XPAR_PS7_UART_0_BASEADDR;
    *uart_control_reg |= 3; // resets the transmission and receive
}

int wifi_send_str(char *buffer)
{
	// Send the message
	int error = wifi_send(XPAR_PS7_UART_0_BASEADDR, buffer, strlen(buffer));
	// Add /r/n at end
	char ending[2] = {0x0d, 0x0a};
	error += wifi_send(XPAR_PS7_UART_0_BASEADDR, ending, 2);
	return error;
}

int wifi_recv(u32 base_address, char *buffer, u16 buffer_size)
{
	if (buffer == NULL || buffer_size < 2)
	{
		return -1;
	}

    u32 i = 0, num_failed = 0, max_num_failed = 500000;
    while(!XUartPs_IsReceiveData(base_address) && num_failed < 10 * max_num_failed)
    {
    	usleep(1);
		num_failed++;
    }
	if(num_failed >= 10 * max_num_failed)
	{
		buffer[0] = 0;
		return -1;
	}
	for (i = 0; i < buffer_size-1; i++)
	{
		num_failed = 0;
		while(!XUartPs_IsReceiveData(base_address) && num_failed < max_num_failed)
		{
			num_failed++;
			usleep(1);
		}
		if(num_failed >= max_num_failed)
			break;
		buffer[i] = XUartPs_RecvByte(base_address);
		xil_printf("%c", buffer[i]);
	}
    buffer[i] = 0;
    return 0;
}

int wifi_send(u32 base_address, char *buffer, u16 buffer_size)
{
	if (buffer == NULL || buffer_size < 1)
	{
		return -1;
	}

    u16 i;
    for (i=0; i < buffer_size; i++)
    {
    	while (XUartPs_IsTransmitFull(base_address));
    	XUartPs_SendByte(base_address, buffer[i]);
    }
    return 0;
}

