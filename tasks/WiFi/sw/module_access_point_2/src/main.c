/*

 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *



 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
*/

#include <stdio.h>
#include <stdlib.h>
//#include "platform.h"
#include "xil_printf.h"
#include "xuartps.h"
#include "sleep.h"
#include "WIFI.h"

//#define TESTING_CPTR_TO_QUAD
//#define TESTING_QUAD_TO_CPTR
//#define TESTING_BOTH_DIRECTIONS
#define TESTING_ROUND_TRIP

#define MAX_NUM_DATA 100

int main()
{
    XUartPs uart0;

	xil_printf("WiFi Initialize starting.\n\r");

    // Initalize the WiFi
    if(wifi_init(&uart0) < 0)
    {
    	xil_printf("WiFi Initialize failed.\n\r");
    	return -1;
    }

    // Throughput testing
    // Only the round trip test is actually fully implemented
    int iterations, past_iterations = 0, i, data_length = 0;
    char recv_buffer[500] = {0};

	xil_printf("Starting testing...\n\r");

	// Wait for the Ground Station socket to connect to the WiFi module
	do
	{
	    data_length = wifi_recieve_msg("0,CONNECT");

	}while(data_length < 0);

	// This is a message sent from the ground station once the socket is ready
	do
	{
	    data_length = wifi_recieve_msg("ready\n\r");

	}while(data_length < 0);
	data_length = 0;
	xil_printf("Socket connected\n\r");

#ifdef TESTING_CPTR_TO_QUAD

    for(iterations = 1; iterations < MAX_NUM_DATA; iterations += past_iterations)
    {
        past_iterations = iterations - past_iterations;
        wifi_recieve_msg("s");

        for(i = 0; i < iterations; i+= data_length)
        {
        	wifi_recieve_msg("i");
        	data_length = strlen(recv_buffer);
        }
    }

    xil_printf("CPTR_TO_QUAD: Received correct amount of data.\n\r");

#endif

#ifdef TESTING_QUAD_TO_CPTR

    // Works, but slow? the actual transfer time does not compute as slow.

    xil_printf("Testing Quad to Computer\n\r");

    for(iterations = 1; iterations < MAX_NUM_DATA; iterations += past_iterations)
    {
        past_iterations = iterations - past_iterations;
        data_length = 1;
        while(data_length != 0)
        {
        	data_length = wifi_send_msg("s");
        }

        for(i = 0; i < iterations; i+=data_length)
        {
        	data_length = wifi_send_msg("q");
        	if(data_length == 0)
        		data_length = 1;
        	else
        		data_length = 0;
        }
        xil_printf("Sent %d characters\n\r", iterations);
    }

    xil_printf("QUAD_TO_CPTR: Sent correct amount of data.\n\r");

#endif

#ifdef TESTING_BOTH_DIRECTIONS
    xil_printf("Testing both directions\n\r");

    for(iterations = 1; iterations < MAX_NUM_DATA; iterations += past_iterations)
    {
        past_iterations = iterations - past_iterations;
    	wifi_send_msg("s");

        for(i = 0; i < iterations; i++)
        {
        	wifi_recieve_msg("i");
        	wifi_send_msg("q");
        }
    }

    xil_printf("BOTH_DIRECTIONS: Received and sent correct amount of data.\n\r");

#endif

#ifdef TESTING_ROUND_TRIP
    xil_printf("Testing Round Trip\n\r");

    // Receive an 'i' from the ground station
	do
	{
		data_length = wifi_recieve_msg("i");

	}while(data_length < 0);

	for(i = 0; i < 1000000; i++)
	{
		usleep(1);
	}

	// Send an 'i' back to the ground station
	wifi_send_msg("i");

    xil_printf("ROUND_TRIP: Received correct amount of data.\n\r");

#endif

    xil_printf("\n\rexiting program\n\r");
    return 0;
}
