/*
 * main.c
 *
 *  Created on: Nov 11, 2015
 *      Author: Amy Seibert
 */


#include "uart.h"


int main() {
	// Initialize UART0 (Bluetooth)
	uart0_init(XPAR_PS7_UART_0_DEVICE_ID, 921600);
	uart0_clearFIFOs();

	// Check sending a byte is working
	uart0_sendByte(0xAB);
	if(uart0_recvByte() != 0xAB)
	{
		xil_printf("problem\n\r");
	}
	else
	{
		xil_printf("worked\n\r");
	}

	return 0;
}
