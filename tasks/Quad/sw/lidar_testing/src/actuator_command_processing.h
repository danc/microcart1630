/*
 * actuator_command_processing.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef ACTUATOR_COMMAND_PROCESSING_H_
#define ACTUATOR_COMMAND_PROCESSING_H_
 
#include <stdio.h>

#include "log_data.h"
#include "control_algorithm.h"

/**
 * @brief
 *      Processes the commands to the actuators.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param raw_actuator_struct
 *      structure of the commmands outputted to go to the actuators
 *
 * @param actuator_command_struct
 *      structure of the commmands to go to the actuators
 *
 * @return 
 *      error message
 *
 */
int actuator_command_processing(log_t* log_struct, user_input_t * user_input_struct, raw_actuator_t* raw_actuator_struct, actuator_command_t* actuator_command_struct);

void old_Aero_to_PWMS(int* PWMs, int* aero);
void Aero_to_PWMS(int* PWMs, int* aero);

#endif /* ACTUATOR_COMMAND_PROCESSING_H_ */
