/*
 * control_algorithm.c
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

// This implemented modular quadrotor software implements a PID control algorithm
 
#include "control_algorithm.h"
#include "communication.h"

#define ROLL_PITCH_MAX_ANGLE 0.3490 // 20 degrees

 int control_algorithm_init(parameter_t * parameter_struct)
 {
	// HUMAN Piloted (RC) PID DEFINITIONS //////
	// RC PIDs for roll (2 loops: angle --> angular velocity)
	parameter_struct->roll_angle_pid.dt = 0.005; parameter_struct->roll_ang_vel_pid.dt = 0.005; // 5 ms calculation period

	// RC PIDs for pitch (2 loops: angle --> angular velocity)
	parameter_struct->pitch_angle_pid.dt = 0.005; parameter_struct->pitch_ang_vel_pid.dt = 0.005; // 5 ms calculation period

	// initialize Yaw PID_t and PID constants
	// RC PID for yaw (1 loop angular velocity)
	parameter_struct->yaw_ang_vel_pid.dt = 0.005; // 5 ms calculation period

	// AUTOMATIC Pilot (Position) PID DEFINITIONS //////
	// Local X PID using a translation from X camera system data to quad local X position (3 loops: local y position --> angle --> angular velocity)
	parameter_struct->local_x_pid.dt = 0.100;

	// Local Y PID using a translation from Y camera system data to quad local Y position(3 loops: local x position --> angle --> angular velocity)
	parameter_struct->local_y_pid.dt = 0.100;

	// CAM PIDs for yaw (2 loops angle --> angular velocity)
	parameter_struct->yaw_angle_pid.dt = 0.100;

	// CAM PID for altitude (1 loop altitude)
	parameter_struct->alt_pid.dt = 0.100;

	// PID coeffiecients (Position)
	setPIDCoeff(&(parameter_struct->local_y_pid), YPOS_KP, YPOS_KI, YPOS_KD);
	setPIDCoeff(&(parameter_struct->local_x_pid), XPOS_KP, XPOS_KI, XPOS_KD);
	setPIDCoeff(&(parameter_struct->alt_pid), ALT_ZPOS_KP, ALT_ZPOS_KI, ALT_ZPOS_KD);

	// PID coefficients (Angle)
	setPIDCoeff(&(parameter_struct->pitch_angle_pid), PITCH_ANGLE_KP, PITCH_ANGLE_KI, PITCH_ANGLE_KD);
	setPIDCoeff(&(parameter_struct->roll_angle_pid), ROLL_ANGLE_KP, ROLL_ANGLE_KI, ROLL_ANGLE_KD);
	setPIDCoeff(&(parameter_struct->yaw_angle_pid), YAW_ANGLE_KP, YAW_ANGLE_KI, YAW_ANGLE_KD);

	// PID coefficients (Angular Velocity)
	setPIDCoeff(&(parameter_struct->pitch_ang_vel_pid), PITCH_ANGULAR_VELOCITY_KP, PITCH_ANGULAR_VELOCITY_KI, PITCH_ANGULAR_VELOCITY_KD);
	setPIDCoeff(&(parameter_struct->roll_ang_vel_pid), ROLL_ANGULAR_VELOCITY_KP, ROLL_ANGULAR_VELOCITY_KI, ROLL_ANGULAR_VELOCITY_KD);
	setPIDCoeff(&(parameter_struct->yaw_ang_vel_pid), YAW_ANGULAR_VELOCITY_KP, YAW_ANGULAR_VELOCITY_KI, YAW_ANGULAR_VELOCITY_KD);

	return 0;
 }
 
 int control_algorithm(log_t* log_struct, user_input_t * user_input_struct, sensor_t* sensor_struct, setpoint_t* setpoint_struct, parameter_t* parameter_struct, user_defined_t* user_defined_struct, raw_actuator_t* raw_actuator_struct, modular_structs_t* structs)
 {
	// use the 'flap' switch as the flight mode selector
	int cur_fm_switch = read_flap(user_input_struct->rc_commands[FLAP]);
	static int last_fm_switch = MANUAL_FLIGHT_MODE;

	// reset flight_mode to MANUAL right away if the flap switch is in manual position
	// to engage AUTO mode the code waits for a new packet after the flap is switched to auto
	// before actually engaging AUTO mode
	if(cur_fm_switch == MANUAL_FLIGHT_MODE)
		user_defined_struct->flight_mode = MANUAL_FLIGHT_MODE;

//	static float roll_trim = 0.0;
//	static float pitch_trim = 0.0;

	// flap switch was just toggled to auto flight mode
	if((last_fm_switch != cur_fm_switch) && (cur_fm_switch == AUTO_FLIGHT_MODE))
	{
		user_defined_struct->engaging_auto = 1;

		// Read in trimmed values because it should read trim values right when the pilot flips the flight mode switch
//		pitch_trim = user_input_struct->pitch_angle_manual_setpoint; //rc_commands[PITCH] - PITCH_CENTER;
//		roll_trim = user_input_struct->roll_angle_manual_setpoint; //rc_commands[ROLL] - ROLL_CENTER;
		//sensor_struct->trimmedRCValues.yaw = yaw_manual_setpoint; //rc_commands[YAW] - YAW_CENTER;

		sensor_struct->trims.roll = raw_actuator_struct->controller_corrected_motor_commands[ROLL];
		sensor_struct->trims.pitch = raw_actuator_struct->controller_corrected_motor_commands[PITCH];
		sensor_struct->trims.yaw = raw_actuator_struct->controller_corrected_motor_commands[YAW];
		sensor_struct->trims.throttle = user_input_struct->rc_commands[THROTTLE];

		log_struct->trims.roll = sensor_struct->trims.roll;
		log_struct->trims.pitch = sensor_struct->trims.pitch;
		log_struct->trims.yaw = sensor_struct->trims.yaw;
		log_struct->trims.throttle = sensor_struct->trims.throttle;
	}

	// process a command if there is one
//	if(user_input_struct->hasPacket == 'C')
//	{
//		processCommand(user_input_struct->sb, setpoint_struct, parameter_struct);//structs);
//	}

//	if(user_input_struct->hasPacket == 'U' && user_defined_struct->engaging_auto == 1)
//			user_defined_struct->engaging_auto = 2;

	// If the quad has received a packet and it's not an update packet
	if(user_input_struct->hasPacket != -1 && user_input_struct->hasPacket != 0x04)
	{
		printf("Received packet from Uart!\n");
		processCommand((unsigned char *)user_input_struct->sb->buf, structs);//structs);
	}


	// if the flap switch was toggled to AUTO_FLIGHT_MODE and we've received a new packet
	// then record the current position as the desired position
	// also reset the previous error and accumulated error from the position PIDs
	if((cur_fm_switch == AUTO_FLIGHT_MODE) && (user_defined_struct->engaging_auto == 2))
	{
		// zero out the accumulated error so the I terms don't cause wild things to happen
		parameter_struct->alt_pid.acc_error = 0.0;
		parameter_struct->local_x_pid.acc_error = 0.0;
		parameter_struct->local_y_pid.acc_error = 0.0;

		// make previous error equal to the current so the D term doesn't spike
		parameter_struct->alt_pid.prev_error = 0.0;
		parameter_struct->local_x_pid.prev_error = 0.0;
		parameter_struct->local_y_pid.prev_error = 0.0;

		setpoint_struct->desiredQuadPosition.alt_pos = sensor_struct->currentQuadPosition.alt_pos;
		setpoint_struct->desiredQuadPosition.x_pos = sensor_struct->currentQuadPosition.x_pos;
		setpoint_struct->desiredQuadPosition.y_pos = sensor_struct->currentQuadPosition.y_pos;
		setpoint_struct->desiredQuadPosition.yaw = 0.0;//currentQuadPosition.yaw;

		// reset the flag that engages auto mode
		user_defined_struct->engaging_auto = 0;

		// finally engage the AUTO_FLIGHT_MODE
		// this ensures that we've gotten a new update packet right after the switch was set to auto mode
		user_defined_struct->flight_mode = AUTO_FLIGHT_MODE;
	}

	//PIDS///////////////////////////////////////////////////////////////////////

	/* 					Position loop
	 * Reads current position, and outputs
	 * a pitch or roll for the angle loop PIDs
	 */

//		static int counter_between_packets = 0;

	if(user_input_struct->hasPacket == 0x04)
	{
		parameter_struct->local_y_pid.current_point = sensor_struct->currentQuadPosition.y_pos;
		parameter_struct->local_y_pid.setpoint = setpoint_struct->desiredQuadPosition.y_pos;

		parameter_struct->local_x_pid.current_point = sensor_struct->currentQuadPosition.x_pos;
		parameter_struct->local_x_pid.setpoint = setpoint_struct->desiredQuadPosition.x_pos;

		parameter_struct->alt_pid.current_point = sensor_struct->currentQuadPosition.alt_pos;
		parameter_struct->alt_pid.setpoint = setpoint_struct->desiredQuadPosition.alt_pos;

		//logging and PID computation
		log_struct->local_y_PID_values = pid_computation(&(parameter_struct->local_y_pid));
		log_struct->local_x_PID_values = pid_computation(&(parameter_struct->local_x_pid));
		log_struct->altitude_PID_values = pid_computation(&(parameter_struct->alt_pid));

		// yaw angular position PID calculation
		parameter_struct->yaw_angle_pid.current_point = sensor_struct->currentQuadPosition.yaw;// in radians
		parameter_struct->yaw_angle_pid.setpoint = setpoint_struct->desiredQuadPosition.yaw; // constant setpoint

		//logging and PID computation
		log_struct->angle_yaw_PID_values = pid_computation(&(parameter_struct->yaw_angle_pid));

		// for testing data update rate
//			char debug_msg[100] = {};
//			sprintf(debug_msg, "time between packets(msec): %d\n",  5 * counter_between_packets);
//			uart0_sendStr(debug_msg);
//			sprintf(debug_msg, "y: %.2f\tx: %.2f\n",
//					sensor_struct->currentQuadPosition.y_pos, sensor_struct->currentQuadPosition.x_pos);
//			uart0_sendStr(debug_msg);
//			msleep(100);

//			counter_between_packets = 0;

	}
//	else
//	{
//		counter_between_packets++;
//	}


	/* 					Angle loop
	 * Calculates current orientation, and outputs
	 * a pitch, roll, or yaw velocity for the angular velocity loop PIDs
	 */

	//angle boundaries
	if(parameter_struct->local_x_pid.pid_correction > ROLL_PITCH_MAX_ANGLE)
	{
		parameter_struct->local_x_pid.pid_correction = ROLL_PITCH_MAX_ANGLE;
	}
	if(parameter_struct->local_x_pid.pid_correction < -ROLL_PITCH_MAX_ANGLE)
	{
		parameter_struct->local_x_pid.pid_correction = -ROLL_PITCH_MAX_ANGLE;
	}
	if(parameter_struct->local_y_pid.pid_correction > ROLL_PITCH_MAX_ANGLE)
	{
		parameter_struct->local_y_pid.pid_correction = ROLL_PITCH_MAX_ANGLE;
	}
	if(parameter_struct->local_y_pid.pid_correction < -ROLL_PITCH_MAX_ANGLE)
	{
		parameter_struct->local_y_pid.pid_correction = -ROLL_PITCH_MAX_ANGLE;
	}

	parameter_struct->pitch_angle_pid.current_point = sensor_struct->pitch_angle_filtered;
	parameter_struct->pitch_angle_pid.setpoint =
			(user_defined_struct->flight_mode == AUTO_FLIGHT_MODE)?
			(parameter_struct->local_x_pid.pid_correction) : user_input_struct->pitch_angle_manual_setpoint;

	parameter_struct->roll_angle_pid.current_point = sensor_struct->roll_angle_filtered;
	parameter_struct->roll_angle_pid.setpoint =
			(user_defined_struct->flight_mode == AUTO_FLIGHT_MODE)?
			(parameter_struct->local_y_pid.pid_correction) : user_input_struct->roll_angle_manual_setpoint;


	//logging and PID computation
	log_struct->angle_pitch_PID_values = pid_computation(&(parameter_struct->pitch_angle_pid));
	log_struct->angle_roll_PID_values = pid_computation(&(parameter_struct->roll_angle_pid));


	/* 				Angular Velocity Loop
	 * Takes the desired angular velocity from the angle loop,
	 * and calculates a PID correction with the current angular velocity
	 */

	// theta_dot is the angular velocity about the y-axis
	// it is calculated from using the gimbal equations
	parameter_struct->pitch_ang_vel_pid.current_point = sensor_struct->theta_dot;
	parameter_struct->pitch_ang_vel_pid.setpoint = parameter_struct->pitch_angle_pid.pid_correction;

	// phi_dot is the angular velocity about the x-axis
	// it is calculated from using the gimbal equations
	parameter_struct->roll_ang_vel_pid.current_point = sensor_struct->phi_dot;
	parameter_struct->roll_ang_vel_pid.setpoint = parameter_struct->roll_angle_pid.pid_correction;

	// Yaw angular velocity PID
	// psi_dot is the angular velocity about the z-axis
	// it is calculated from using the gimbal equations
	parameter_struct->yaw_ang_vel_pid.current_point = sensor_struct->psi_dot;
	parameter_struct->yaw_ang_vel_pid.setpoint = (user_defined_struct->flight_mode == AUTO_FLIGHT_MODE)?
			parameter_struct->yaw_angle_pid.pid_correction : user_input_struct->yaw_manual_setpoint; // no trim added because the controller already works well

	//logging and PID computation
	log_struct->ang_vel_pitch_PID_values = pid_computation(&(parameter_struct->pitch_ang_vel_pid));
	log_struct->ang_vel_roll_PID_values = pid_computation(&(parameter_struct->roll_ang_vel_pid));
	log_struct->ang_vel_yaw_PID_values = pid_computation(&(parameter_struct->yaw_ang_vel_pid));

	//END PIDs///////////////////////////////////////////////////////////////////////


	 // here for now so in case any flight command is not PID controlled, it will default to rc_command value:
	memcpy(raw_actuator_struct->controller_corrected_motor_commands, user_input_struct->rc_commands, sizeof(int) * 6);

	// don't use the PID corrections if the throttle is less than about 10% of its range
	if((user_input_struct->rc_commands[THROTTLE] >
	118000) || (user_defined_struct->flight_mode == AUTO_FLIGHT_MODE))
	{

		if(user_defined_struct->flight_mode == AUTO_FLIGHT_MODE)
		{
			//THROTTLE
			raw_actuator_struct->controller_corrected_motor_commands[THROTTLE] =
				((int)(parameter_struct->alt_pid.pid_correction)) + sensor_struct->trims.throttle;

			//ROLL
			raw_actuator_struct->controller_corrected_motor_commands[ROLL] =
					parameter_struct->roll_ang_vel_pid.pid_correction + sensor_struct->trims.roll;

			//PITCH
			raw_actuator_struct->controller_corrected_motor_commands[PITCH] =
					parameter_struct->pitch_ang_vel_pid.pid_correction + sensor_struct->trims.pitch;

			//YAW
			raw_actuator_struct->controller_corrected_motor_commands[YAW] =
					parameter_struct->yaw_ang_vel_pid.pid_correction;// + sensor_struct->trims.yaw;
		}
		else{
			//ROLL
			raw_actuator_struct->controller_corrected_motor_commands[ROLL] =
					parameter_struct->roll_ang_vel_pid.pid_correction;

			//PITCH
			raw_actuator_struct->controller_corrected_motor_commands[PITCH] =
					parameter_struct->pitch_ang_vel_pid.pid_correction;

			//YAW
			raw_actuator_struct->controller_corrected_motor_commands[YAW] =
					parameter_struct->yaw_ang_vel_pid.pid_correction;
		}

		//BOUNDS CHECKING
		if(raw_actuator_struct->controller_corrected_motor_commands[ROLL] > 20000)
			raw_actuator_struct->controller_corrected_motor_commands[ROLL] = 20000;

		if(raw_actuator_struct->controller_corrected_motor_commands[ROLL] < -20000)
			raw_actuator_struct->controller_corrected_motor_commands[ROLL] = -20000;

		if(raw_actuator_struct->controller_corrected_motor_commands[PITCH] > 20000)
			raw_actuator_struct->controller_corrected_motor_commands[PITCH] = 20000;

		if(raw_actuator_struct->controller_corrected_motor_commands[PITCH] < -20000)
			raw_actuator_struct->controller_corrected_motor_commands[PITCH] = -20000;

		if(raw_actuator_struct->controller_corrected_motor_commands[YAW] > 20000)
			raw_actuator_struct->controller_corrected_motor_commands[YAW] = 20000;

		if(raw_actuator_struct->controller_corrected_motor_commands[YAW] < -20000)
			raw_actuator_struct->controller_corrected_motor_commands[YAW] = -20000;

	}
	else
	{
		raw_actuator_struct->controller_corrected_motor_commands[ROLL] = 0;
		raw_actuator_struct->controller_corrected_motor_commands[PITCH] = 0;
		raw_actuator_struct->controller_corrected_motor_commands[YAW] = 0;
	}

	//logging
	// here we are not actually duplicating the logging from the PID computation
	// the PID computation logs PID_values struct where this logs the PID struct
	// they contain different sets of data
	log_struct->local_y_PID = parameter_struct->local_y_pid;
	log_struct->local_x_PID = parameter_struct->local_x_pid;
	log_struct->altitude_PID = parameter_struct->alt_pid;

	log_struct->angle_roll_PID = parameter_struct->roll_angle_pid;
	log_struct->angle_pitch_PID = parameter_struct->pitch_angle_pid;
	log_struct->angle_yaw_PID = parameter_struct->yaw_angle_pid;

	log_struct->ang_vel_roll_PID = parameter_struct->roll_ang_vel_pid;
	log_struct->ang_vel_pitch_PID = parameter_struct->pitch_ang_vel_pid;
	log_struct->ang_vel_yaw_PID = parameter_struct->yaw_ang_vel_pid;

	last_fm_switch = cur_fm_switch;

	if(user_input_struct->hasPacket != -1)
	{
		user_input_struct->sb->clear(user_input_struct->sb);
	}

    return 0;
 }
 
 void setPIDCoeff(PID_t* p, float pValue, float iValue, float dValue) {

 	p->Kp = pValue;
 	p->Ki = iValue;
 	p->Kd = dValue;

 }

