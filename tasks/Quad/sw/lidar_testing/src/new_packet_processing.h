/*
 * process_packet.h
 *
 *  Created on: Mar 2, 2016
 *      Author: ucart
 */

#ifndef PROCESS_PACKET_H_
#define PROCESS_PACKET_H_

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "type_def.h"

tokenList_t tokenize(char* cmd);
int processUpdate(char* update, quadPosition_t* currentQuadPosition);
//int processCommand(stringBuilder_t * sb, modular_structs_t* structs);
int doProcessing(char* cmd, tokenList_t * tokens, setpoint_t * setpoint_struct, parameter_t * parameter_struct);
float getFloat(char* str, int pos);
int getInt(char* str, int pos);

#endif /* PROCESS_PACKET_H_ */
