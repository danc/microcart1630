/*
 * uart.h
 *
 *  Created on: Nov 10, 2014
 *      Author: ucart
 */

#ifndef UART_H_
#define UART_H_

#include "xparameters.h"
#include "xuartps.h"
#include "stringBuilder.h"

#define PACKET_START_CHAR 2
#define PACKET_END_CHAR 3
#define UPDATE_SIZE 28

extern XUartPs* _Uart0PS;
extern XUartPs* _Uart1PS;


/************************************************/
/************** Main UART Interface *************/
XUartPs* uart_init(XUartPs* uartps_ptr, u16 deviceID, int baudRate);
void uart_clearFIFOs(XUartPs* uartps_ptr);
void uart_sendByte(XUartPs* uartps_ptr, char data);
void uart_sendStr(XUartPs* uartps_ptr, char* str);
void uart_sendBytes(XUartPs* uartps_ptr, char* data, int numBytes);
int uart_isSending(XUartPs* uartps_ptr);
int uart_hasData(XUartPs* uartps_ptr);

//char uart_recvByte(); // block until char received

//int uart_recvBytes(char* buffer, int numBytes, int timeoutMicros); // block until all received

//void uart_recvCallback(void (*func)(char data));

void uart_recvBytes(XUartPs* uartps_ptr, char* buffer, int numBytes);

char uart_recvByte(XUartPs* uartps_ptr);
/************************************************/
/************************************************/



/************************************************/
/********** UART 0 convenience methods **********/
XUartPs* uart0_init(u16 deviceID, int baudRate);
void uart0_clearFIFOs();
void uart0_sendByte(char data);
void uart0_sendStr(char* data);
void uart0_sendBytes(char* data, int numBytes);
int uart0_isSending();
int uart0_hasData();
void uart0_recvBytes(char* buffer, int numBytes);
char uart0_recvByte();
/************************************************/
/************************************************/



/************************************************/
/********** UART 1 convenience methods **********/
XUartPs* uart1_init(u16 deviceID, int baudRate);
void uart1_clearFIFOs();
void uart1_sendByte(char data);
void uart1_sendStr(char* data);
void uart1_sendBytes(char* data, int numBytes);
int uart1_isSending();
int uart1_hasData();
void uart1_recvBytes(char* buffer, int numBytes);
char uart1_recvByte();
/************************************************/
/************************************************/

int tryReceivePacket(stringBuilder_t* sb, int echo);

#endif /* UART_H_ */
