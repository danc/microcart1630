/*
 * stringBuilder.c
 *
 *  Created on: Sep 24, 2014
 *      Author: ucart
 */

#include <stdlib.h>
#include <string.h>
#include "stringBuilder.h"

int stringBuilder_maybeExpand(stringBuilder_t* sb) {
	if (!sb || !sb->buf) {
		return STRINGBUILDER_ILLEGALARGUMENT;
	}

	// See if we need to expand
	if (sb->length + 1 >= sb->capacity) {
		if (sb->capacity >= sb->maxCapacity) {
			// Would exceed maxCapacity, can't do anything
			return STRINGBUILDER_AT_MAX_CAPACITY;
		}

		// Compute new size (try doubling)
		int newCapacity = sb->capacity * 2;
		if (newCapacity <= 2) {
			newCapacity = 2;
		}
		if (newCapacity >= sb->maxCapacity) {
			newCapacity = sb->maxCapacity;
		}

		// Get a pointer to the old buf mem
		char* oldBuf = sb->buf;
		sb->buf = malloc(newCapacity);
		if (!sb->buf) {
			// Agh, no mem for buf. Restore the old one and return error
			sb->buf = oldBuf;
			return STRINGBUILDER_NO_MEM_FOR_EXPANSION;
		} else {
			// Got mem for new buf, copy from old buf
			strncpy(sb->buf, oldBuf, sb->length + 1);
			sb->capacity = newCapacity;
			free(oldBuf);
		}
	}

	return STRINGBUILDER_SUCCESS;
}

stringBuilder_t* stringBuilder_createWithMaxCapacity(int initialCapacity,
		int maxCapacity) {
	// Invalid arguments
	if (initialCapacity > maxCapacity || initialCapacity < 1
			|| maxCapacity < 1) {
		return NULL ;
	}

	stringBuilder_t* sb = (stringBuilder_t*) malloc(sizeof(stringBuilder_t));
	if (!sb) {
		// No mem for buffer
		return NULL ;
	}

	// Try to allocate mem for buf
	sb->buf = malloc(initialCapacity);
	if (!sb->buf) {
		// No mem for buf
		free(sb);
		return NULL ;
	}

	// Set up function pointers
	sb->addStr = stringBuilder_addStr;
	sb->addStrAt = stringBuilder_addStrAt;
	sb->addChar = stringBuilder_addChar;
	sb->addCharAt = stringBuilder_addCharAt;
	sb->removeCharAt = stringBuilder_removeCharAt;
	sb->clear = stringBuilder_clear;


	// Good to go
	sb->length = 0;
	sb->capacity = initialCapacity;
	sb->maxCapacity = maxCapacity;
	sb->buf[0] = '\0';
	return sb;
}

stringBuilder_t* stringBuilder_createWithInitialCapacity(int initialCapacity) {
	return stringBuilder_createWithMaxCapacity(initialCapacity,
			STRINGBUILDER_DEFAULT_MAX_CAPACITY);
}

stringBuilder_t* stringBuilder_create() {
	return stringBuilder_createWithMaxCapacity(
			STRINGBUILDER_DEFAULT_INITIAL_CAPACITY,
			STRINGBUILDER_DEFAULT_MAX_CAPACITY);
}

void stringBuilder_free(stringBuilder_t* sb) {
	if (sb && sb->buf) {
		free(sb->buf);
	}
	if (sb) {
		free(sb);
	}
}

int stringBuilder_addStr(stringBuilder_t* sb, char* str) {
	if (!sb || !sb->buf) {
		return STRINGBUILDER_ILLEGALARGUMENT;
	}

	while (*str) {
		int status = sb->addChar(sb, *str);
		if (status != STRINGBUILDER_SUCCESS) {
			return status;
		}
		str++;
	}

	return STRINGBUILDER_SUCCESS;
}

int stringBuilder_addStrAt(stringBuilder_t* sb, char* str, int index) {
	if (!sb || !sb->buf) {
		return STRINGBUILDER_ILLEGALARGUMENT;
	}

	while (*str) {
		int status = sb->addCharAt(sb, *str, index);
		if (status != STRINGBUILDER_SUCCESS) {
			return status;
		}

		str++;
		index++;
	}

	return STRINGBUILDER_SUCCESS;
}

/** Add a character to end of the StringBuilder */
int stringBuilder_addChar(stringBuilder_t* sb, char c) {
	if (!sb || !sb->buf) {
		return STRINGBUILDER_ILLEGALARGUMENT;
	}

	return stringBuilder_addCharAt(sb, c, sb->length);
}

/** Add a character to the StringBuilder at the specified index, if possible */
int stringBuilder_addCharAt(stringBuilder_t* sb, char c, int index) {
	if (!sb || !sb->buf || index < 0 || index > sb->length) {
		return STRINGBUILDER_ILLEGALARGUMENT;
	}

	// Try expanding if necessary
	int status = stringBuilder_maybeExpand(sb);
	if (status != STRINGBUILDER_SUCCESS) {
		return status;
	}

	// Move everything right of index over by 1
	int i;
	for (i = sb->length; i >= index; i--) {
		sb->buf[i + 1] = sb->buf[i];
	}

	// Insert the character and add the null terminator
	sb->buf[index] = c;
	sb->length += 1;

	return STRINGBUILDER_SUCCESS;
}

/** Remove a character from the StringBuilder at the specified index */
int stringBuilder_removeCharAt(stringBuilder_t* sb, int index) {
	if (!sb || !sb->buf || index < 0 || index >= sb->length) {
		return STRINGBUILDER_ILLEGALARGUMENT;
	}

	// Move everything right of index over left
	int i;
	for (i = index; i < sb->length; i++) {
		sb->buf[i] = sb->buf[i + 1];
	}
	sb->length -= 1;

	return STRINGBUILDER_SUCCESS;
}

void stringBuilder_clear(stringBuilder_t* sb) {
	sb->length = 0;
	sb->buf[0] = '\0';
}

