/*
 * sensor_processing.c
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */
 
#include "sensor_processing.h"
#include "timer.h"
#include <math.h>

int sensor_processing(log_t* log_struct, user_input_t *user_input_struct, raw_sensor_t* raw_sensor_struct, sensor_t* sensor_struct)
{
	// copy currentQuadPosition and trimmedRCValues from raw_sensor_struct to sensor_struct
	deep_copy_Qpos(&(sensor_struct->currentQuadPosition), &(raw_sensor_struct->currentQuadPosition));

	// Psuedo-Nonlinear Extension for determining local x/y position based on yaw angle
	// provided by Matt Rich
	//
	// local x/y/z is the moving frame of reference on the quad that we are transforming so we can assume yaw angle is 0 (well enough)
	// 		for the autonomous position controllers
	//
	// camera given x/y/z is the inertia frame of reference (the global coordinates)
	//
	// |local x|	|cos(yaw angle)  -sin(yaw angle)  0| |camera given x|
	// |local y|  = |sin(yaw angle)   cos(yaw angle)  0| |camera given y|
	// |local z|	|       0               0         1| |camera given z|

	sensor_struct->currentQuadPosition.x_pos =
			sensor_struct->currentQuadPosition.x_pos * cos(sensor_struct->currentQuadPosition.yaw) +
			sensor_struct->currentQuadPosition.y_pos * -sin(sensor_struct->currentQuadPosition.yaw);

	sensor_struct->currentQuadPosition.y_pos =
			sensor_struct->currentQuadPosition.x_pos * sin(sensor_struct->currentQuadPosition.yaw) +
			sensor_struct->currentQuadPosition.y_pos * cos(sensor_struct->currentQuadPosition.yaw);


	// Calculate Euler angles and velocities using Gimbal Equations below
	/////////////////////////////////////////////////////////////////////////
	// | Phi_d   |   |  1  sin(Phi)tan(theta)    cos(Phi)tan(theta) |  | p |
	// | theta_d | = |  0  cos(Phi)              -sin(Phi)		    |  | q |
	// | Psi_d   |   |  0  sin(Phi)sec(theta)    cos(Phi)sec(theat) |  | r |
	//
	// Phi_dot = p + q sin(Phi) tan(theta) + r cos(Phi) tan(theta)
	// theta_dot = q cos(Phi) - r sin(Phi)
	// Psi_dot = q sin(Phi) sec(theta) + r cos(Phi) sec(theta)
	///////////////////////////////////////////////////////////////////////////

	// javey:
	//
	// The gimbal equations are defined in the book "Flight Simulation" by Rolfe and Staples.
	// Find on page 46, equation 3.6

	// these are calculated to be used in the gimbal equations below
	// the variable roll(pitch)_angle_filtered is phi(theta)
	double sin_phi = sin(sensor_struct->roll_angle_filtered);
	double cos_phi = cos(sensor_struct->roll_angle_filtered);
	double tan_theta = tan(sensor_struct->pitch_angle_filtered);
	double sec_theta = 1/cos(sensor_struct->pitch_angle_filtered);

//	Gryo "p" is the angular velocity rotation about the x-axis (defined as var gyro_xVel_p in gam struct)
//	Gyro "q" is the angular velocity rotation about the y-axis (defined as var gyro_xVel_q in gam struct)
//	Gyro "r" is the angular velocity rotation about the z-axis (defined as var gyro_xVel_r in gam struct)

	// phi is the conventional symbol used for roll angle, so phi_dot is the roll velocity
	sensor_struct->phi_dot = raw_sensor_struct->gam.gyro_xVel_p + (raw_sensor_struct->gam.gyro_yVel_q*sin_phi*tan_theta)
			+ (raw_sensor_struct->gam.gyro_zVel_r*cos_phi*tan_theta);

	// theta is the conventional symbol used for pitch angle, so theta_dot is the pitch velocity
	sensor_struct->theta_dot = (raw_sensor_struct->gam.gyro_yVel_q*cos_phi)
			- (raw_sensor_struct->gam.gyro_zVel_r*sin_phi);

	// psi is the conventional symbol used for yaw angle, so psi_dot is the yaw velocity
	sensor_struct->psi_dot = (raw_sensor_struct->gam.gyro_yVel_q*sin_phi*sec_theta)
			+ (raw_sensor_struct->gam.gyro_zVel_r*cos_phi*sec_theta);

	// Complementary Filter Calculations
	sensor_struct->pitch_angle_filtered = 0.98 * (sensor_struct->pitch_angle_filtered + sensor_struct->theta_dot * LOOP_TIME)
			+ 0.02 * raw_sensor_struct->gam.accel_pitch;

	sensor_struct->roll_angle_filtered = 0.98 * (sensor_struct->roll_angle_filtered + sensor_struct->phi_dot* LOOP_TIME)
			+ 0.02 * raw_sensor_struct->gam.accel_roll;
			
	sensor_struct->lidar_altitude = (float)(raw_sensor_struct->ldr_z*0.3);

//	static int loop_counter = 0;
//	loop_counter++;
//
//	if(loop_counter == 50)
//	{
//		char dMsg[100] = {};
//		sprintf(dMsg, "Loop time: %.4f\n", LOOP_TIME);
//		uart0_sendStr(dMsg);
//		loop_counter = 0;
//	}

	//logging
	log_struct->currentQuadPosition = sensor_struct->currentQuadPosition;
	log_struct->roll_angle_filtered = sensor_struct->roll_angle_filtered;
	log_struct->pitch_angle_filtered = sensor_struct->pitch_angle_filtered;
	log_struct->phi_dot = sensor_struct->phi_dot;
	log_struct->theta_dot = sensor_struct->theta_dot;
	log_struct->psi_dot = sensor_struct->psi_dot;
	log_struct->lidar_altitude = sensor_struct->lidar_altitude;
	return 0;
}

void set_pitch_angle_filtered(sensor_t * sensor_struct, float accel_roll)
{
	sensor_struct->pitch_angle_filtered = accel_roll;
}
void set_roll_angle_filtered(sensor_t * sensor_struct, float accel_pitch)
{
	sensor_struct->roll_angle_filtered = accel_pitch;
}

void deep_copy_Qpos(quadPosition_t * dest, quadPosition_t * src)
{
	dest->packetId = src->packetId;
	dest->y_pos = src->y_pos;
	dest->x_pos = src->x_pos;
	dest->alt_pos = src->alt_pos;
	dest->roll = src->roll;
	dest->pitch = src->pitch;
	dest->yaw = src->yaw;

}
