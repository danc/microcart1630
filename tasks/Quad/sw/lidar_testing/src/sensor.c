/*
 * sensor.c
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */
 
#include "sensor.h"

int sensor_init(raw_sensor_t * raw_sensor_struct, sensor_t * sensor_struct)
{
	if((startMPU9150() == -1)||(startLidar() == -1))
		return -1;

	// read sensor board and fill in gryo/accelerometer/magnetometer struct
	get_gam_reading(&(raw_sensor_struct->gam));

	// Sets the first iteration to be at the accelerometer value since gyro initializes to {0,0,0} regardless of orientation
	sensor_struct->pitch_angle_filtered = raw_sensor_struct->gam.accel_roll;
	sensor_struct->roll_angle_filtered = raw_sensor_struct->gam.accel_pitch;

    return 0;
}

int get_sensors(log_t* log_struct, user_input_t* user_input_struct, raw_sensor_t* raw_sensor_struct)
{
	// if there was a new update packet this loop then do the required processing
	if (user_input_struct->hasPacket == 0x04) {
		processUpdate(user_input_struct->sb->buf, &(raw_sensor_struct->currentQuadPosition));
//		char debug_msg[1000] = {};
//		sprintf(debug_msg, "x: %.5f\ty: %.5f\n",  raw_sensor_struct->currentQuadPosition.x_pos, raw_sensor_struct->currentQuadPosition.y_pos);
//		uart0_sendStr(debug_msg);
	}

	// the the sensor board and fill in the readings into the GAM struct
	get_gam_reading(&(raw_sensor_struct->gam));
	raw_sensor_struct->ldr_z=ReadLidarHeight();
	

	log_struct->gam = raw_sensor_struct->gam;

    return 0;
}
 
