/*
 * main.c
 *
 *  Created on: Nov 11, 2015
 *      Author: ucart
 */
#include <stdio.h>
#include "timer.h"
#include "log_data.h"
#include "initialize_components.h"
#include "user_input.h"
#include "sensor.h"
#include "sensor_processing.h"
#include "control_algorithm.h"
#include "actuator_command_processing.h"
#include "send_actuator_commands.h"
#include "update_gui.h"

int main()
{
	// Structures to be used throughout
	modular_structs_t structs = { };


	// Initialize all required components and structs:
	// Uart, PWM receiver/generator, I2C, Sensor Board
	// Xilinx Platform, Loop Timer, Control Algorithm
	int init_error = initializeAllComponents(&(structs.user_input_struct), &(structs.log_struct),
			&(structs.raw_sensor_struct), &(structs.sensor_struct), &(structs.setpoint_struct), &(structs.parameter_struct),
			&(structs.user_defined_struct), &(structs.raw_actuator_struct), &(structs.actuator_command_struct));

	if (init_error != 0) {
		printf("ERROR (main): Problem initializing...Goodbye\r\n");
		return -1;
	}

	// Loops to make sure the quad is responding correctly before starting the control loop
	protection_loops();

	// Main control loop
	do
	{
		// Processing of loop timer at the beginning of the control loop
		timer_start_loop();

		// Get the user input and put it into user_input_struct
		get_user_input(&(structs.log_struct), &(structs.user_input_struct));

		// Get data from the sensors and put it into raw_sensor_struct
		get_sensors(&(structs.log_struct), &(structs.user_input_struct), &(structs.raw_sensor_struct));

		// Process the sensor data and put it into sensor_struct
		sensor_processing(&(structs.log_struct), &(structs.user_input_struct), &(structs.raw_sensor_struct), &(structs.sensor_struct));

		// Run the control algorithm
		control_algorithm(&(structs.log_struct), &(structs.user_input_struct), &(structs.sensor_struct), &(structs.setpoint_struct),
				&(structs.parameter_struct), &(structs.user_defined_struct), &(structs.raw_actuator_struct), &structs);

		// Process the commands going to the actuators
		actuator_command_processing(&(structs.log_struct), &(structs.user_input_struct), &(structs.raw_actuator_struct), &(structs.actuator_command_struct));

		// send the actuator commands
		send_actuator_commands(&(structs.log_struct), &(structs.actuator_command_struct));

		// update the GUI
		update_GUI(&(structs.log_struct));

		// Processing of loop timer at the end of the control loop
		timer_end_loop(&(structs.log_struct));

		// Log the data collected in this loop
		log_data(&(structs.log_struct));

		if(structs.user_defined_struct.flight_mode == AUTO_FLIGHT_MODE)
		{
			static int loop_counter = 0;
			loop_counter++;

			// toggle the MIO7 on and off to show that the quad is in AUTO_FLIGHT_MODE
			if(loop_counter == 10)
			{
				MIO7_led_off();
			}
			else if(loop_counter >= 20)
			{

				MIO7_led_on();
				loop_counter = 0;
			}

		}
		if(structs.user_defined_struct.flight_mode == MANUAL_FLIGHT_MODE)
			MIO7_led_on();

	} while(!kill_condition(&(structs.user_input_struct)));


	stringBuilder_free((structs.user_input_struct).sb);

	pwm_kill();

	MIO7_led_off();

	printLogging();

	flash_MIO_7_led(10, 100);

	return 0;
}


