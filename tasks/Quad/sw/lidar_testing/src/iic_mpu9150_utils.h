/* iic_mpu9150_utils.h
 *
 * A header file for the prototyping constants used for
 * the I2C Controller 0 (I2C0) on the Zybo Board
 *
 * This file is intended SOLELY for the Sparkfun MPU9150
 * and the Diligent ZyBo Board
 *
 * Author: ucart
 *
 */


#ifndef IIC_MPU9150_UTILS_H
#define IIC_MPU9150_UTILS_H


#include "xbasic_types.h"
#include "xiicps.h"
#include "type_def.h"

// System configuration registers
// (Please see Appendix B: System Level Control Registers in the Zybo TRM)
#define IIC_SYSTEM_CONTROLLER_RESET_REG_ADDR 	(0xF8000224)
#define IO_CLK_CONTROL_REG_ADDR 				(0xF800012C)

// IIC0 Registers
#define IIC0_CONTROL_REG_ADDR 		(0xE0004000)
#define IIC0_STATUS_REG_ADDR 		(0xE0004004)
#define IIC0_SLAVE_ADDR_REG 		(0xE0004008)
#define IIC0_DATA_REG_ADDR 			(0xE000400C)
#define IIC0_INTR_STATUS_REG_ADDR 	(0xE0004010)
#define IIC0_TRANFER_SIZE_REG_ADDR	(0xE0004014)
#define IIC0_INTR_EN			    (0xE0004024)
#define IIC0_TIMEOUT_REG_ADDR 		(0xE000401C)

// MPU9150 Sensor Defines (Address is defined on the Sparkfun MPU9150 Datasheet)
#define MPU9150_DEVICE_ADDR 		0b01101000
#define MPU9150_COMPASS_ADDR 		0x0C

#define LIDAR_WRITE_ADDR	0xC4
#define LIDAR_READ_ADDR		0xC5


#define ACCEL_GYRO_READ_SIZE 		14		//Bytes
#define ACCEL_GYRO_BASE_ADDR		0x3B	//Starting register address

#define MAG_READ_SIZE 				6
#define MAG_BASE_ADDR 				0x03

#define RAD_TO_DEG 57.29578
#define DEG_TO_RAD 0.0174533

// Array indicies when reading from ACCEL_GYRO_BASE_ADDR
#define ACC_X_H 0
#define ACC_X_L 1
#define ACC_Y_H 2
#define ACC_Y_L 3
#define ACC_Z_H 4
#define ACC_Z_L 5

#define GYR_X_H 8
#define GYR_X_L 9
#define GYR_Y_H 10
#define GYR_Y_L 11
#define GYR_Z_H 12
#define GYR_Z_L 13

#define MAG_X_L 0
#define MAG_X_H 1
#define MAG_Y_L 2
#define MAG_Y_H 3
#define MAG_Z_L 4
#define MAG_Z_H 5

//Interrupt Status Register Masks
#define ARB_LOST       (0x200)
#define RX_UNF          (0x80)
#define TX_OVF          (0x40)
#define RX_OVF          (0x20)
#define SLV_RDY         (0x10)
#define TIME_OUT        (0x08)
#define NACK      		(0x04)
#define MORE_DAT 	    (0x02)
#define TRANS_COMPLETE 	(0x01)

#define WRITE_INTR_MASK (ARB_LOST | TIME_OUT | RX_OVF | TX_OVF | NACK)
#define READ_INTR_MASK (ARB_LOST | TIME_OUT | RX_OVF | RX_UNF | NACK)


// Gyro is configured for +/-2000dps
// Sensitivity gain is based off MPU9150 datasheet (pg. 11)
#define GYRO_SENS 16.4

#define GYRO_X_BIAS	0.005f
#define GYRO_Y_BIAS	-0.014f
#define GYRO_Z_BIAS	0.045f

#define ACCEL_X_BIAS	0.023f
#define ACCEL_Y_BIAS	0.009f
#define ACCEL_Z_BIAS	0.087f

// Initialize hardware; Call this FIRST before calling any other functions
int initI2C0();

void iic0Write(u8 register_addr, u8 data, u16 device_addr);
void iic0Read(u8* recv_buffer, u8 register_addr, int size, u16 device_addr);


// Wake up the MPU for data collection
// Configure Gyro/Accel/Mag
int startMPU9150();
int startLidar();

// Put MPU back to sleep
void stopMPU9150();
void stopLidar();

void CalcMagSensitivity();
void ReadMag(gam_t* gam);
void ReadGyroAccel(gam_t* gam);
int ReadLidarHeight();

int get_gam_reading(gam_t* gam);


/////////////
// Deprecated functions below
/////////////


// Initialize hardware; Call this FIRST before calling any other functions
void init_iic0();

// Wake up the MPU for data collection
void start_mpu9150();

// Put MPU back to sleep
void stop_mpu9150();


// Write a byte of data at the given register address on the MPU
void iic0_write(Xuint16 reg_addr, Xuint8 data);

// Read a single byte at a given register address on the MPU
Xuint8 iic0_read(Xuint16 reg_addr);

// Read multiple bytes consecutively at a starting register address
// places the resulting bytes in rv
int iic0_read_bytes(Xuint8* rv, Xuint16 reg_addr, int bytes);

// Helper function to initialize I2C0 controller on the Zybo board
// Called by init_iic0
void iic0_hw_init();


// Clears the interrupt status register
// Called by configuration functions
void iic0_clear_intr_status();



// Configure I2C0 controller on Zybo to receive data
void iic0_config_ctrl_to_receive();

// Configure I2C0 controller to transmit data
void iic0_config_ctrl_to_transmit();


void wake_mag();

#endif /*IIC_MPU9150_UTILS_H*/
