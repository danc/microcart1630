/*#include "packet_processing.h"

static int msgNum = 0;

tokenList_t tokenize(char* cmd) {
	int maxTokens = 16;
	tokenList_t ret;
	ret.numTokens = 0;
	ret.tokens = malloc(sizeof(char*) * maxTokens);
	ret.tokens[0] = NULL;

	int i = 0;
	char* token = strtok(cmd, " ");
	while (token != NULL && i < maxTokens - 1) {
		ret.tokens[i++] = token;
		ret.tokens[i] = NULL;
		ret.numTokens++;
		token = strtok(NULL, " ");
	}

	return ret;
}

int checkFloat(char *floatString, float *value) {
	char *tmp;
	*value = strtod(floatString, &tmp);
	if(!(isspace(*tmp) || *tmp == 0)) {
		fprintf(stderr, "%s is not a valid floating-point number\n", floatString);
		return 0;
	}
	return 1;
}

int checkInt(char *intString, int *value) {
	char *tmp;
    long temp_long;
    temp_long = strtol(intString, &tmp, 0); // base 10 number inputted
	if(temp_long < INT_MIN || temp_long > INT_MAX ||  !(isspace(*tmp) || *tmp == 0)) {
		fprintf(stderr, "%s is not a valid integer number\n", intString);
		return 0;
	}
    printf("temp: %ld\n\n", temp_long);
    *value = (int) temp_long;
	return 1;
}

//--------------------------------
// Ground Station
//--------------------------------

// Formatting commands from ground station CLI

int formatCommand(unsigned char *command, unsigned char *formattedCommand) {

    // Erase the existing command
    formattedCommand[0] = 0;

	command[strlen(command) - 1] = 0;

	tokenList_t tokens = tokenize(command);
	float floatValue = 0.0;
	int intValue = 0;
    int dataLength = 0;
    char data_checksum = 0;

	int i, type, subtype, valid;

	// ----------------------------------------------
	if(tokens.numTokens > 0) {
        for(type = 0; type < MAX_TYPE; type++)
        {
            for(subtype = 0; subtype < MAX_SUBTYPE; subtype++)
            {
                if(strcmp(tokens.tokens[0], MessageTypes[type].subtypes[subtype].cmdText) == 0)
                {
                    printf("Sending\n\ttype: %d, \n\tsubtype: %d\n\tcommand: %s\n", type, subtype, MessageTypes[type].subtypes[subtype].cmdText);
                    // Make sure there is a second token
                    if(tokens.numTokens <= 1)
                    {
                        return -1;
                    }

                    // Make sure the second token is the right type
                    switch (MessageTypes[type].subtypes[subtype].cmdDataType)
                    {
                        // Validate the float input
                        case floatType:
                            valid = checkFloat(tokens.tokens[1], &floatValue);
                            if(!valid) {
                                return -1;
                            }
                            break;
                        // Validate the integer input
                        case intType:
                            valid = checkInt(tokens.tokens[1], &intValue);
                            if(!valid) {
                                return -1;
                            }
                            break;
                        // Validate the string input (doesn't need to happen)
                        case stringType:
                            break;
                        default:
                            return -1;
                    }

                    //printf("%f\n", value);

                    //int formattedCommandLength = 8;

    //----------------------------------------------------------------------------------------------
	//     index||     0    |     1    |      2      |  3 & 4 |      5 & 6       |  7+  |   end    |
	//---------------------------------------------------------------------------------------------|
	// msg param|| beg char | msg type | msg subtype | msg id | data len (bytes) | data | checksum |
	//-------------------------------------------------------------------------------------------- |
	//     bytes||     1    |     1    |      1      |    2   |        2         | var  |    1     |
	//----------------------------------------------------------------------------------------------

                    // Begin Char: 0xBE
                    formattedCommand[0] = BEGIN_CHAR;

                    // Msg type:
                    formattedCommand[1] = MessageTypes[type].ID;

                    // Msg subtype
                    formattedCommand[2] = MessageTypes[type].subtypes[subtype].ID;

                    //Msg id (msgNum is 2 bytes)
                    memcpy(&formattedCommand[3], &msgNum, sizeof(msgNum));
                    msgNum++;

                    // Data length and data - bytes 5&6 for len, 7+ for data
                    switch (MessageTypes[type].subtypes[subtype].cmdDataType)
                    {
                        // float input
                        case floatType:
                            dataLength = sizeof(floatValue);
                            memcpy(&formattedCommand[5], &dataLength, sizeof(dataLength));
                            memcpy(&formattedCommand[7], &floatValue, sizeof(floatValue));
							printf("blah %f\n", floatValue);
							printf("blah %f\n", formattedCommand[7]);
                            break;
                        // integer input
                        case intType:
                            dataLength = sizeof(intValue);
                            memcpy(&formattedCommand[5], &dataLength, sizeof(dataLength));
                            memcpy(&formattedCommand[7], &intValue, sizeof(intValue));
                            break;
                        // string input
                        case stringType:
                            dataLength = sizeof(tokens.tokens[1]);
                            memcpy(&formattedCommand[5], &dataLength, sizeof(dataLength));
                            memcpy(&formattedCommand[7], &tokens.tokens[1], sizeof(tokens.tokens[1]));
                            break;
                        default:
                            return -1;
                    }

                    // Checksum

                    // receive data and calculate checksum
                    for(i = 0; i < 7 + dataLength; i++)
                    {
                        data_checksum ^= formattedCommand[i];
                    }

                    formattedCommand[7+dataLength] = data_checksum;

                    return 0;
                }
            }
        }
    }

    // Only gets here if the command does not exist
	return -1;
}

// Format the log data from log_message
int logData(unsigned char *log_msg, unsigned char *formattedCommand)
{
    //formattedCommand = malloc(sizeof(char) * (strlen(log_msg) + 8));
    if (formattedCommand == NULL)
    {
        return -1;
    }

    //----------------------------------------------------------------------------------------------
	//     index||     0    |     1    |      2      |  3 & 4 |      5 & 6       |  7+  |   end    |
	//---------------------------------------------------------------------------------------------|
	// msg param|| beg char | msg type | msg subtype | msg id | data len (bytes) | data | checksum |
	//-------------------------------------------------------------------------------------------- |
	//     bytes||     1    |     1    |      1      |    2   |        2         | var  |    1     |
	//----------------------------------------------------------------------------------------------

    // Begin Char: 0xBE
    formattedCommand[0] = BEGIN_CHAR;

    // Msg type:
    formattedCommand[1] = MessageTypes[5].ID;

    // Msg subtype
    formattedCommand[2] = MessageTypes[5].subtypes[0].ID;

    //Msg id (msgNum is 2 bytes)
    memcpy(&formattedCommand[3], &msgNum, sizeof(msgNum));
    msgNum++;

    // Data length and data - bytes 5&6 for len, 7+ for data
    int dataLength = strlen(log_msg) + 1;
    memcpy(&formattedCommand[5], &dataLength, sizeof(dataLength));
    memcpy(&formattedCommand[7], log_msg, strlen(log_msg) + 1);

    // Checksum

    // receive data and calculate checksum
    int i;
    char data_checksum = 0;
    for(i = 0; i < 7 + dataLength; i++)
    {
        data_checksum ^= formattedCommand[i];
    }

    formattedCommand[7 + dataLength] = data_checksum;

    return 8 + dataLength;
}*/
