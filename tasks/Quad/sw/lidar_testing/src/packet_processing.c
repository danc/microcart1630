/*
 * process_packet.c
 *
 *  Created on: Mar 2, 2016
 *      Author: ucart
 */
#include "packet_processing.h"
#include "uart.h"
#include "type_def.h"
#include "sleep.h"
#include "util.h"

#define DEBUG 0

tokenList_t tokenize(char* cmd) {
	int maxTokens = 16;
	tokenList_t ret;
	ret.numTokens = 0;
	ret.tokens = malloc(sizeof(char *)* 20 * maxTokens);

	int i = 0;
	ret.tokens[0] = NULL;
	char* token = strtok(cmd, " ");
	while (token != NULL && i < maxTokens - 1) {
		ret.tokens[i] = malloc(strlen(token) + 10);
		strcpy(ret.tokens[i], token);
		ret.tokens[++i] = NULL;
		ret.numTokens++;
		token = strtok(NULL, " ");
	}

	return ret;
}

int processUpdate(char* update, quadPosition_t* currentQuadPosition) {
	//static char buf[16384];
	//sprintf(buf, "update..(%d)..[%s]\r\n", strlen(update), update);
	//uart0_sendStr(buf);


	// Packet must come as [NEARPY], 4 bytes each
	int packetId = getInt(update, 0);
	float y_pos = getFloat(update, 4);
	float x_pos = getFloat(update, 8);
	float alt_pos = getFloat(update, 12);
	float roll = getFloat(update, 16);
	float pitch = getFloat(update, 20);
	float yaw = getFloat(update, 24);

	currentQuadPosition->packetId = packetId;
	currentQuadPosition->y_pos = y_pos;
	currentQuadPosition->x_pos = x_pos;
	currentQuadPosition->alt_pos = alt_pos;
	currentQuadPosition->roll = roll;
	currentQuadPosition->pitch = pitch;
	currentQuadPosition->yaw = yaw;

	return 0;
}

/*int processCommand(stringBuilder_t * sb, setpoint_t * setpoint_struct, parameter_t * parameter_struct) {
#if DEBUG
	{
//		char buf[512] = { };
//		snprintf(buf, 512, "\nPROCESSING COMMAND \"%s\" (%d)\n", cmd, strlen(cmd));
//		uart0_sendStr(buf);
	}
#endif

	char cmdOrig[1024] = {}; //= malloc(sb->length + 1);
	strcpy(cmdOrig, sb->buf);
	int ret = 0;
	tokenList_t tokens = tokenize(cmdOrig);

#if DEBUG
	char dMsg[1024] = {};
	int i;
	for(i = 0; i < tokens.numTokens; i++)
		strcat(dMsg, tokens.tokens[i]);
	uart0_sendStr(dMsg);
#endif

	if (sb->length > 0) {
		ret = doProcessing(sb->buf, &tokens, setpoint_struct, parameter_struct);
	}

	return ret;
}*/

int doProcessing(char* cmd, tokenList_t *tokens, setpoint_t * setpoint_struct, parameter_t * parameter_struct) {
#ifdef DEBUG
	char buf2[512];
	snprintf(buf2, 512, "Number of tokens: %d\r\n", tokens->numTokens);
	uart0_sendStr(buf2);
	usleep(1000);
	int i;
	for (i=0; i < tokens->numTokens; i++) {
		snprintf(buf2, sizeof(buf2), "--> %s\r\n", tokens->tokens[i]);
		uart0_sendStr(buf2);
		usleep(1000);
	}
#endif

	static int n=0;

	char buf[1024] = {};
	if (tokens->numTokens > 0) {
		if (strcmp(tokens->tokens[0], "hello") == 0) {
			sprintf(buf, "Hello there!\n");
			uart0_sendStr(buf);
		}
		else if (strcmp(tokens->tokens[0], "n") == 0) {
			int inc = tokens->tokens[1][0] - '0';
			n += inc;
		} else if (strcmp(tokens->tokens[0], "g") == 0) {
			sprintf(buf, "Num: %d\r\n", n);
			uart0_sendStr(buf);
		} else if (strcmp(tokens->tokens[0], "P") == 0){
			snprintf(buf, sizeof(buf), "You did the P command with %d args\r\n", tokens->numTokens - 1);
			//float val = atoff(tokens.tokens[1]);
			//pVal += val;
			uart0_sendStr(buf);
		} /*else if (strcmp(tokens->tokens[0], "setyaw") == 0) {
			if (tokens->numTokens == 2) {
				float desiredYaw;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredYaw);
				if (numSet != 1) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setyaw - must be numeric\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				setpoint_struct->desiredQuadPosition.yaw = desiredYaw;
				snprintf(buf, sizeof(buf), "Successfully set desired yaw to %.2f\r\n", desiredYaw);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setyaw requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setyawp") == 0) {
			if (tokens.numTokens == 2) {
				float desiredYawP;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredYawP);
				if (numSet != 1 || desiredYawP < -100 || desiredYawP > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setyawp - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->yaw_angle_pid.Kp = desiredYawP;
				snprintf(buf, sizeof(buf), "Successfully set yaw P coefficient to %.2f\r\n", desiredYawP);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setyawp requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setyawd") == 0) {
			if (tokens.numTokens == 2) {
				float desiredYawD;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredYawD);
				if (numSet != 1 || desiredYawD < -100 || desiredYawD > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setyawd - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->yaw_angle_pid.Kd = desiredYawD;
				snprintf(buf, sizeof(buf), "Successfully set yaw D coefficient to %.2f\r\n", desiredYawD);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setyawd requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		} else if (strcmp(tokens.tokens[0], "setx") == 0) {
			if (tokens.numTokens == 2) {
				float desiredX;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredX);
				if (numSet != 1) { // || desiredX < -M_PI || desiredX > M_PI) { // TODO find limits for x position
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setx - must be numeric and in the range \r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				setpoint_struct->desiredQuadPosition.x_pos = desiredX;
				snprintf(buf, sizeof(buf), "Successfully set desired x to %.2f\r\n", desiredX);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setx requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setxp") == 0) {
			if (tokens.numTokens == 2) {
				float desiredXP;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredXP);
				if (numSet != 1 || desiredXP < -100 || desiredXP > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setxp - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->local_x_pid.Kp = desiredXP;
				snprintf(buf, sizeof(buf), "Successfully set X P coefficient to %.2f\r\n", desiredXP);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setxp requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setxi") == 0) {
			if (tokens.numTokens == 2) {
				float desiredXI;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredXI);
				if (numSet != 1 || desiredXI < -100 || desiredXI > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setxi - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->local_x_pid.Ki = desiredXI;
				snprintf(buf, sizeof(buf), "Successfully set X I coefficient to %.2f\r\n", desiredXI);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setxi requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setxd") == 0) {
			if (tokens.numTokens == 2) {
				float desiredXD;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredXD);
				if (numSet != 1 || desiredXD < -100 || desiredXD > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setxd - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->local_x_pid.Kd = desiredXD;
				snprintf(buf, sizeof(buf), "Successfully set X D coefficient to %.2f\r\n", desiredXD);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setxd requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		} else if (strcmp(tokens.tokens[0], "sety") == 0) {
			if (tokens.numTokens == 2) {
				float desiredY;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredY);
				if (numSet != 1) { // || desiredY < -M_PI || desiredY > M_PI) { // TODO find limits for y position
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to sety - must be numeric and in the range \r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				setpoint_struct->desiredQuadPosition.y_pos = desiredY;
				snprintf(buf, sizeof(buf), "Successfully set desired y to %.2f\r\n", desiredY);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - sety requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setyp") == 0) {
			if (tokens.numTokens == 2) {
				float desiredYP;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredYP);
				if (numSet != 1 || desiredYP < -100 || desiredYP > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setyp - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->local_y_pid.Kp = desiredYP;
				snprintf(buf, sizeof(buf), "Successfully set Y P coefficient to %.2f\r\n", desiredYP);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setyp requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setyi") == 0) {
			if (tokens.numTokens == 2) {
				float desiredYI;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredYI);
				if (numSet != 1 || desiredYI < -100 || desiredYI > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setyi - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->local_y_pid.Ki = desiredYI;
				snprintf(buf, sizeof(buf), "Successfully set Y I coefficient to %.2f\r\n", desiredYI);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setyi requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setyd") == 0) {
			if (tokens.numTokens == 2) {
				float desiredYD;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredYD);
				if (numSet != 1 || desiredYD < -100 || desiredYD > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setyd - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->local_y_pid.Kd = desiredYD;
				snprintf(buf, sizeof(buf), "Successfully set Y D coefficient to %.2f\r\n", desiredYD);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setyd requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		} else if (strcmp(tokens.tokens[0], "setz") == 0) {
			if (tokens.numTokens == 2) {
				float desiredZ;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredZ);
				if (numSet != 1) { // || desiredZ < -M_PI || desiredZ > M_PI) { // TODO find limits for z position
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setz - must be numeric and in the range \r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				setpoint_struct->desiredQuadPosition.alt_pos = desiredZ;
				snprintf(buf, sizeof(buf), "Successfully set desired z to %.2f\r\n", desiredZ);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setz requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setzp") == 0) {
			if (tokens.numTokens == 2) {
				float desiredZP;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredZP);
				if (numSet != 1 || desiredZP < -100 || desiredZP > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setzp - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->alt_pid.Kp = desiredZP;
				snprintf(buf, sizeof(buf), "Successfully set Z P coefficient to %.2f\r\n", desiredZP);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setzp requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setzi") == 0) {
			if (tokens.numTokens == 2) {
				float desiredZI;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredZI);
				if (numSet != 1 || desiredZI < -100 || desiredZI > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setzi - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->alt_pid.Ki = desiredZI;
				snprintf(buf, sizeof(buf), "Successfully set Z I coefficient to %.2f\r\n", desiredZI);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setzi requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setzd") == 0) {
			if (tokens.numTokens == 2) {
				float desiredZD;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredZD);
				if (numSet != 1 || desiredZD < -100 || desiredZD > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setzd - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				parameter_struct->alt_pid.Kd = desiredZD;
				snprintf(buf, sizeof(buf), "Successfully set Z D coefficient to %.2f\r\n", desiredZD);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setyd requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		} else if (strcmp(tokens.tokens[0], "dumprand") == 0) {
			// dump a bunch of bytes of random data
			// format: $> dumprand <numBytes>
			if (tokens.numTokens == 2) {
				int numBytes;
				int numSet = sscanf(tokens.tokens[1], "%d", &numBytes);
				if (numSet != 1 || numBytes < 0 || numBytes > 1000000) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to dumprand - must be integer and in the range [0, 1000000]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				int i;
				srand(42);
				// split it into 10KB blocks
				char randBuf[10240];			snprintf(buf, sizeof(buf), "Unrecognized Command!");
			uart0_sendStr(buf);
			usleep(1000);
				int counter = 0;
				for (i=0; i < numBytes; i++) {
					// get random letter
					randBuf[counter++] = (rand() % 26) + 'A';

					// send it if the block gets full
					if (counter == sizeof(randBuf)) {
						counter = 0;
						uart0_sendBytes(randBuf, sizeof(randBuf));
					}
				}
				if (counter != 0) {
					uart0_sendBytes(randBuf, counter);
				}
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - dumprand requires exactly one integer argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}*/
		else {
			snprintf(buf, sizeof(buf), "Unrecognized command: \"%s\" [length %d]\r\n", cmd, strlen(cmd));
			snprintf(buf, sizeof(buf), "Unrecognized Command!");
			uart0_sendStr(buf);
			usleep(1000);
			return 1;
		}
	}

	return 0;
}

float getFloat(char* str, int pos) {
	union {
		float f;
		int i;
	} x;
	x.i = ((str[pos+3] << 24) | (str[pos+2] << 16) | (str[pos+1] << 8) | (str[pos]));
	return x.f;
}

int getInt(char* str, int pos) {
	int i = ((str[pos+3] << 24) | (str[pos+2] << 16) | (str[pos+1] << 8) | (str[pos]));
	return i;
}
