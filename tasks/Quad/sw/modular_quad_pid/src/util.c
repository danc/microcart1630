/*
 * util.c
 *
 *  Created on: Oct 11, 2014
 *      Author: ucart
 */

#include "util.h"

extern int motor0_bias, motor1_bias, motor2_bias, motor3_bias;
//Global variable representing the current pulseW
int pulseW = pulse_throttle_low;

/**
 * Initializes the PWM output components.
 * Default pulse length  = 1 ms
 * Default period length = 2.33 ms
 */
void pwm_init() {
	printf("Period initialization starting\r\n");

	int* pwm_0 = (int*) PWM_0_ADDR + PWM_PERIOD;
	int* pwm_1 = (int*) PWM_1_ADDR + PWM_PERIOD;
	int* pwm_2 = (int*) PWM_2_ADDR + PWM_PERIOD;
	int* pwm_3 = (int*) PWM_3_ADDR + PWM_PERIOD;

	// Initializes all the PWM address to have the correct period width at 450 hz
	*pwm_0 = period_width;
	*pwm_1 = period_width;
	*pwm_2 = period_width;
	*pwm_3 = period_width;
	printf("Period initialization successful %d\n", period_width);
	// Initializes the PWM pulse lengths to be 1 ms
	*(int*) (PWM_0_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_1_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_2_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_3_ADDR + PWM_PULSE) = pulse_throttle_low;
	printf("Pulse initialization successful %d\n", pulse_throttle_low);

#ifdef X_CONFIG
	printf("In x config mode\n");
#else
	printf("In + config mode\n");
#endif
	usleep(1000000);
}

/**
 * Writes all PWM components to be the same given pulsewidth
 */
void pwm_write_all(int pulseWidth) {
	// Check lower and upper bounds
	if (pulseWidth > pulse_throttle_high)
		pulseWidth = pulse_throttle_high;
	if (pulseWidth < pulse_throttle_low)
		pulseWidth = pulse_throttle_low;
	// Set all the pulse widths
	*(int*) (PWM_0_ADDR + PWM_PULSE) = pulseWidth;
	*(int*) (PWM_1_ADDR + PWM_PULSE) = pulseWidth;
	*(int*) (PWM_2_ADDR + PWM_PULSE) = pulseWidth;
	*(int*) (PWM_3_ADDR + PWM_PULSE) = pulseWidth;
}
/**
 * Write a given pulseWidth to a channel
 */
void pwm_write_channel(int pulseWidth, int channel){
	// Check lower and upper bounds
		if (pulseWidth > pulse_throttle_high)
			pulseWidth = pulse_throttle_high;
		if (pulseWidth < pulse_throttle_low)
			pulseWidth = pulse_throttle_low;

		switch(channel){
		case 0:
			*(int*) (PWM_0_ADDR + PWM_PULSE) = pulseWidth;
			break;
		case 1:
			*(int*) (PWM_1_ADDR + PWM_PULSE) = pulseWidth;
			break;
		case 2:
			*(int*) (PWM_2_ADDR + PWM_PULSE) = pulseWidth;
			break;
		case 3:
			*(int*) (PWM_3_ADDR + PWM_PULSE) = pulseWidth;
			break;
		default:
			break;
		}
}
/**
 * Reads the registers from the PWM_Recorders, and returns the pulse width
 * of the last PWM signal to come in
 */
int read_rec(int channel) {
	switch (channel) {
	case 0:
		return *((int*) PWM_REC_0_ADDR);
	case 1:
		return *((int*) PWM_REC_1_ADDR);
	case 2:
		return *((int*) PWM_REC_2_ADDR);
	case 3:
		return *((int*) PWM_REC_3_ADDR);
	case 4:
		return *((int*) PWM_REC_4_ADDR);
	case 5:
		return *((int*) PWM_REC_5_ADDR);
	default:
		return 0;
	}
}
/**
 * Reads all 4 receiver channels at once
 */
void read_rec_all(int* mixer){
	int i;
	for(i = 0; i < 6; i++){
		mixer[i] = read_rec(i);
	}
}

int hexStrToInt(char *buf, int startIdx, int endIdx) {
	int result = 0;
	int i;
	int power = 0;
	for (i=endIdx; i >= startIdx; i--) {
		int value = buf[i];
		if ('0' <= value && value <= '9') {
			value -= '0';
		} else if ('a' <= value && value <= 'f') {
			value -= 'a';
			value += 10;
		} else if ('A' <= value && value <= 'F') {
			value -= 'A';
			value += 10;
		}

		result += (2 << (4 * power)) * value;
		power++;
	}

	return result;
}

void read_bluetooth_all(int* mixer) {
	char buffer[32];

	int done = 0;
	int gotS = 0;
	char c  = 0;
	while (!done) {
		int counter = 0;
		if (!gotS) {
			c = uart0_recvByte();
		}
		if (c == 'S') {

			while (1) {
				char cc = uart0_recvByte();
				if (cc == 'S') {
									counter = 0;
									gotS = 1;
									break;
								}
				printf("C=%c,\r\n",cc);
				buffer[counter++] = cc;


				if (counter == 12) {
					buffer[12] = 0;
					done = 1;
					gotS = 0;
				}
			}
			//uart0_recvBytes(buffer, 12);
			//buffer[12] = 0;


		}
	}

//	// data := "XX XX XX XX XX"
//	uart0_recvBytes(buffer, 12);
//	buffer[12] = 0;
//
//
	int i;
	for(i=0; i < 5; i++) {
		mixer[i] = 0;
	}

	for (i=0; i < 4; i++) {
		//mixer[i] = hexStrToInt(buffer, 3*i, 3*i + 1);
		mixer[i] = (buffer[i*3] << 8) | buffer[i*3 + 1];
	}

	printf("mixer: \"%s\" -> %d %d %d %d %d\r\n", buffer, mixer[0], mixer[1], mixer[2], mixer[3], mixer[4]);

}

/**
 * Use the buttons to drive the pulse length of the channels
 */
void b_drive_pulse() {
	int* btns = (int *) XPAR_BTNS_BASEADDR;

	// Increment the pulse width by 5% throttle
	if (*btns & 0x1) {
		pulseW += 1000;
		if (pulseW > 200000)
			pulseW = 200000;
		pwm_write_all(pulseW);
		while (*btns & 0x1)
			;
	} //Decrease the pulse width by 5% throttle
	else if (*btns & 0x2) {
		pulseW -= 1000;
		if (pulseW < 100000) {
			pulseW = 100000;
		}
		pwm_write_all(pulseW);
		while (*btns & 0x2)
			;
	}
	// Set the pulses back to default
	else if (*btns & 0x4) {
		pulseW = MOTOR_0_PERCENT;
		pwm_write_all(pulseW);
	}
	// Read the pulse width of pwm_recorder 0
	else if (*btns & 0x8) {
		int i;
		for(i = 0; i < 4; i++){
			xil_printf("Channel %d:  %d\n", i, read_rec(i));
		}
		//xil_printf("%d\n",pulseW);
		while (*btns & 0x8)
			;
	}
}

/**
 * Creates a sine wave driving the motors from 0 to 100% throttle
 */
void sine_example(){

	int* btns = (int *) XPAR_BTNS_BASEADDR;
	/*        Sine Wave        */
	static double time = 0;

	time += .0001;
	pulseW = (int)fabs(sin(time)*(100000)) + 100000;
	//pulseW = (int) (sin(time) + 1)*50000 + 100000;
	if (*btns & 0x1){
		printf("%d", pulseW);
		printf("   %d\n", *(int*) (PWM_0_ADDR + PWM_PULSE));
	}
	pwm_write_all(pulseW);
	usleep(300);
}

void print_mixer(int * mixer){
	int i;
	for(i = 0; i < 4; i++){
		xil_printf("%d : %d			", i, mixer[i]);
	}
	xil_printf("\n");
}

/**
 * Argument is the reading from the pwm_recorder4 which is connected to the gear pwm
 * If the message from the receiver is 0 - gear, kill the system by sending a 1
 * Otherwise, do nothing
 */
int read_kill(int gear){
	if(gear > 115000 && gear < 125000)
		return 1;
	return 0;
}

int read_flap(int flap)
{
	// flap '0' is 108,000 CC (Up)
	// flap '1' is 192,000 CC (Down)
	// here we say if the reading is greater than 150,000 than its '1'; '0' otherwise
	if(flap > 150000)
		return 1;
	return 0;
}

/**
 * Turns off the motors
 */
void pwm_kill(){
	// Initializes the PWM pulse lengths to be 1 ms
	*(int*) (PWM_0_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_1_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_2_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_3_ADDR + PWM_PULSE) = pulse_throttle_low;
	xil_printf("Kill switch was touched, shutting off the motors and ending the program\n");
}

/**
 * Useful stuff in here in regards to PID tuning, and motor tuning
 * TODO : Scanf string stuff
 * TODO : Make adam do it :DDDDDDDD
 */
void bluetoothTunePID(char instr, gam_t* gam, PID_t* CFpid, PID_t* Gpid){
	int wasX = 0;
	int wasPid = 0;
	int wasSetpoint = 0;
	int wasLog = 0;
	char buf[100];

	switch (instr) {
	case 'P':
		// Change this if tuning other PIDs
		CFpid->Kp += .5;
		wasPid = 1;
		break;
	case 'p':
		CFpid->Kp -= .5;
		wasPid = 1;
		break;
	case 'O':
		CFpid->Kp += .2;
		wasPid = 1;
		break;
	case 'o':
		CFpid->Kp -= .2;
		wasPid = 1;
		break;

	case 'I':
		CFpid->Kp += 0.1;
		wasPid = 1;
		break;
	case 'i':
		CFpid->Kp -= 0.1;
		wasPid = 1;
		break;
	case 'W':
		Gpid->Kp += 1;
		wasPid = 1;
		break;
	case 'w':
		Gpid->Kp -= 1;
		wasPid = 1;
		break;
	case 'E':
		Gpid->Kp += 2;
		wasPid = 1;
		break;
	case 'e':
		Gpid->Kp -= 2;
		wasPid = 1;
		break;
	case 'R':
		Gpid->Kp += 5;
		wasPid = 1;
		break;
	case 'r':
		Gpid->Kp -= 5;
		wasPid = 1;
		break;
	case 'D':
		CFpid->Kd += .1;
		wasPid = 1;
		break;
	case 'd':
			CFpid->Kd -= .1;
			wasPid = 1;
			break;
	case 'S':
			CFpid->setpoint += 1;
			wasSetpoint = 1;
			break;
	case 's':
			CFpid->setpoint -= 1;
			wasSetpoint = 1;
			break;
	case '0':
		motor0_bias += 100;
		wasPid = 0;
		break;
	case '1':
		motor1_bias += 100;
		wasPid = 0;
		break;
	case '2':
		motor2_bias += 100;
		wasPid = 0;
		break;
	case '3':
		motor3_bias += 100;
		wasPid = 0;
		break;
	case ')':
		motor0_bias -= 100;
		wasPid = 0;
		break;
	case '!':
		motor1_bias -= 100;
		wasPid = 0;
		break;
	case '@':
		motor2_bias -= 100;
		wasPid = 0;
		break;
	case '#':
		motor3_bias -= 100;
		wasPid = 0;
		break;
	case 'x':
		wasX = 1;
		break;
	case ' ':
		wasLog = 1;
		break;
/*	case 'm':
		pid->setpoint = -45.0;
		// Change set point
		break;
	case 'n':
		pid->setpoint = 45.0;
	*/	// Change set point
	default:
		wasPid = 0;
		break;
	}

	if(wasX){
		return;
	}
	else if(wasSetpoint){
		sprintf(buf, "Setpoint: %4.1f\n\r", CFpid->setpoint);
		uart0_sendBytes(buf, strlen(buf));
		usleep(5000);
	}
	else if (wasPid) {
		/*sprintf(buf,
				"PAngle: %8.3f RAngle: %8.3f PID p: %8.3f  d: %8.3f\r\n",
				compY, compX, pitchPID.Kp, pitchPID.Kd);*/

		sprintf(buf, "CFP Coeff: %4.1f D %4.1f GP Coeff: %4.1f\n\r", CFpid->Kp, CFpid->Kd, Gpid->Kp);
		uart0_sendBytes(buf, strlen(buf));
		usleep(5000);
	}
	else if (wasLog){
		sprintf(buf, "CX %5.2f GP GX: %5.2f\n\r", 0.0, gam->gyro_xVel_p);
		uart0_sendBytes(buf, strlen(buf));
		usleep(5000);
	}
	else {
		sprintf(buf, "Motor bias's \t\t0: %d 1: %d 2: %d 3: %d \r\n", motor0_bias,
				motor1_bias, motor2_bias, motor3_bias);
		uart0_sendBytes(buf, strlen(buf));

//		sprintf(buf, "P: %3.2f I: %3.2f D: %3.2f\r\n", log_struct.ang_vel_pitch_PID_values.P, log_struct.ang_vel_pitch_PID_values.I, log_struct.ang_vel_pitch_PID_values.D);
		uart0_sendBytes(buf, strlen(buf));
		usleep(1e4);
	}
}

void flash_MIO_7_led(int how_many_times, int ms_between_flashes)
{
	if(how_many_times <= 0)
		return;

	while(how_many_times)
	{
		MIO7_led_on();

		usleep(ms_between_flashes * 500);

		MIO7_led_off();

		usleep(ms_between_flashes * 500);

		how_many_times--;
	}
}

void MIO7_led_off()
{
	XGpioPs Gpio;

	XGpioPs_Config * ConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);

	XGpioPs_SetDirectionPin(&Gpio, 7, 1);

	// Disable LED
	XGpioPs_WritePin(&Gpio, 7, 0x00);
}

void MIO7_led_on()
{
	XGpioPs Gpio;

	XGpioPs_Config * ConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);

	XGpioPs_SetDirectionPin(&Gpio, 7, 1);

	// Enable LED
	XGpioPs_WritePin(&Gpio, 7, 0x01);
}

void msleep(int msecs)
{
	usleep(msecs * 1000);
}

