/*
 * timer.h
 *
 *  Created on: Feb 24, 2016
 *      Author: ucart
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "log_data.h"
#include "xtime_l.h"
#include <xtmrctr.h>

extern XTime before;
extern XTime after;
extern XTmrCtr axi_timer;
extern float LOOP_TIME;
extern float time_stamp;

// desired loop time is not guaranteed (its possible that the loop may take longer than desired)
#define DESIRED_USEC_PER_LOOP 5000 // gives 5ms loops

#define PL_CLK_CNTS_PER_USEC 100

/**
 * @brief
 *      Initializes the items necessary for loop timing.
 *
 * @return
 *      error message
 *
 */
int timer_init();

/**
 * @brief
 *      Does processing of the loop timer at the beginning of the control loop.
 *
 * @return
 *      error message
 *
 */
int timer_start_loop();

/**
 * @brief
 *      Does processing of the loop timer at the end of the control loop.
 *
 * @return
 *      error message
 *
 */
int timer_end_loop(log_t *log_struct);

#endif /* TIMER_H_ */
