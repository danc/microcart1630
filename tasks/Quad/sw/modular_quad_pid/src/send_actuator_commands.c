/*
 * send_actuator_commands.c
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */
 
#include "send_actuator_commands.h"
 
int send_actuator_commands(log_t* log_struct, actuator_command_t* actuator_command_struct)
{
	int i;
	// write the PWMs to the motors
	for (i = 0; i < 4; i++)
		pwm_write_channel(actuator_command_struct->pwms[i], i);

    log_struct->motors[0] = actuator_command_struct->pwms[0];
    log_struct->motors[1] = actuator_command_struct->pwms[1];
    log_struct->motors[2] = actuator_command_struct->pwms[2];
    log_struct->motors[3] = actuator_command_struct->pwms[3];

    return 0;
}
 
