/*
 * struct_def.h
 *
 *  Created on: Mar 2, 2016
 *      Author: ucart
 */

#ifndef TYPE_DEF_H_
#define TYPE_DEF_H_

/**
 * @brief
 *      The modes for autonomous and manual flight.
 *
 */
enum flight_mode{
	AUTO_FLIGHT_MODE,
	MANUAL_FLIGHT_MODE
};

//----------------------------------------------------------------------------------------------
//     index||     0    |     1    |      2      |  3 & 4 |      5 & 6       |  7+  |   end    |
//---------------------------------------------------------------------------------------------|
// msg param|| beg char | msg type | msg subtype | msg id | data len (bytes) | data | checksum |
//-------------------------------------------------------------------------------------------- |
//     bytes||     1    |     1    |      1      |    2   |        2         | var  |    1     |
//----------------------------------------------------------------------------------------------
typedef struct {
	char begin_char;
	char msg_type;
	char msg_subtype;
	int msg_id;
	int data_len;
} metadata_t;


// String builder data type
typedef struct stringBuilder_s {
	char* buf;
	int length;
	int capacity;
	int maxCapacity;

	// Methods
	int (*addStr)(struct stringBuilder_s*, char*);
	int (*addStrAt)(struct stringBuilder_s*, char*, int);
	int (*addChar)(struct stringBuilder_s*, char);
	int (*addCharAt)(struct stringBuilder_s*, char, int);
	int (*removeCharAt)(struct stringBuilder_s*, int);
	void (*clear)(struct stringBuilder_s*);
} stringBuilder_t;

typedef struct {
	char** tokens;
	int numTokens;
} tokenList_t;

typedef struct commands{
	int pitch, roll, yaw, throttle;
}commands;

typedef struct raw{
	int x,y,z;
}raw;
typedef struct PID_Consts{
	float P, I, D;
}PID_Consts;

//Camera system info
typedef struct {
	int packetId;

	double y_pos;
	double x_pos;
	double alt_pos;

	double yaw;
	double roll;
	double pitch;
} quadPosition_t;

typedef struct {
	float yaw;
	float roll;
	float pitch;
	float throttle;
} quadTrims_t;

//Gyro, accelerometer, and magnetometer data structure
//Used for reading an instance of the sensor data
typedef struct {

	// GYRO
	//Xint16 raw_gyro_x, raw_gyro_y, raw_gyro_z;

	float gyro_xVel_p; // In degrees per second
	float gyro_yVel_q;
	float gyro_zVel_r;

	// ACCELEROMETER
	//Xint16 raw_accel_x, raw_accel_y, raw_accel_z;

	float accel_x; //In g
	float accel_y;
	float accel_z;

	float accel_roll;
	float accel_pitch;


	// MAG
	//Xint16 raw_mag_x, raw_mag_y, raw_mag_z;

	float heading; // In degrees

	float mag_x; //Magnetic north: ~50 uT
	float mag_y;
	float mag_z;



}gam_t;

typedef struct PID_t {
	double current_point;	// Current value of the system
	double setpoint;		// Desired value of the system
	float Kp;				// Proportional constant
	float Ki;				// Integral constant
	float Kd;				// Derivative constant
	double prev_error;		// Previous error
	double acc_error;		// Accumulated error
	double pid_correction;	// Correction factor computed by the PID
	float dt; 				// sample period
} PID_t;

typedef struct PID_values{
	float P;	// The P component contribution to the correction output
	float I;	// The I component contribution to the correction output
	float D;	// The D component contribution to the correction output
	float error; // the error of this PID calculation
	float change_in_error; // error change from the previous calc. to this one
	float pid_correction; // the correction output (P + I + D)
} PID_values;

///////// MAIN MODULAR STRUCTS
/**
 * @brief
 *      Holds the data inputed by the user
 *
 */
typedef struct user_input_t {
	int rc_commands[6]; 	// Commands from the RC transmitter


//	float cam_x_pos;	// Current x position from the camera system
//	float cam_y_pos;	// Current y position from the camera system
//	float cam_z_pos;	// Current z position from the camera system
//	float cam_roll;		// Current roll angle from the camera system
//	float cam_pitch;	// Current pitch angle from the camera system
//	float cam_yaw;		// Current yaw angle from the camera system

	float yaw_manual_setpoint;
	float roll_angle_manual_setpoint;
	float pitch_angle_manual_setpoint;

	int hasPacket;
	stringBuilder_t * sb;
} user_input_t;

/**
 * @brief
 *      Holds the log data to be sent to the ground station. It may hold the
 * timestamp of when a sensor's data was obtained.
 *
 */
typedef struct log_t {
	// Time
	float time_stamp;
	float time_slice;

	// Id
	int packetId;

	gam_t gam; 	// Raw and calculated gyro, accel, and mag values are all in gam_t
	float phi_dot, theta_dot, psi_dot; // gimbal equation values

	quadPosition_t currentQuadPosition;

	float roll_angle_filtered, pitch_angle_filtered;
	float lidar_altitude;

	float pid_P_component, pid_I_component, pid_D_component; // use these generically for any PID that you are testing

	// PID coefficients and errors
	PID_t local_x_PID, local_y_PID, altitude_PID;
	PID_t angle_yaw_PID, angle_roll_PID, angle_pitch_PID;
	PID_t ang_vel_yaw_PID, ang_vel_roll_PID, ang_vel_pitch_PID;

	PID_values local_x_PID_values, local_y_PID_values, altitude_PID_values;
	PID_values angle_yaw_PID_values, angle_roll_PID_values, angle_pitch_PID_values;
	PID_values ang_vel_yaw_PID_values, ang_vel_roll_PID_values, ang_vel_pitch_PID_values;

	// RC commands
	commands commands;

	//trimmed values
	quadTrims_t trims;

	int motors[4];

} log_t;

/**
 * @brief
 *      Holds the raw data from the sensors and the timestamp if available
 *
 */
typedef struct raw_sensor {
	int acc_x;		// accelerometer x data
	int acc_x_t;	// time of accelerometer x data

	int acc_y;		// accelerometer y data
	int acc_y_t;	// time of accelerometer y data

	int acc_z;		// accelerometer z data
	int acc_z_t;	// time of accelerometer z data


	int gyr_x;		// gyroscope x data
	int gyr_x_t;	// time of gyroscope x data

	int gyr_y;		// gyroscope y data
	int gyr_y_t;	// time of gyroscope y data

	int gyr_z;		// gyroscope z data
	int gyr_z_t;	// time of gyroscope z data

	int ldr_z;		//lidar z data (altitude)
	int ldr_z_t;	//time of lidar z data

	gam_t gam;

	// Structures to hold the current quad position & orientation
	quadPosition_t currentQuadPosition;

} raw_sensor_t;

/**
 * @brief
 *      Holds the processed data from the sensors and the timestamp if available
 *
 */
typedef struct sensor {
	int acc_x;		// accelerometer x data
	int acc_x_t;	// time of accelerometer x data

	int acc_y;		// accelerometer y data
	int acc_y_t;	// time of accelerometer y data

	int acc_z;		// accelerometer z data
	int acc_z_t;	// time of accelerometer z data


	int gyr_x;		// gyroscope x data
	int gyr_x_t;	// time of gyroscope x data

	int gyr_y;		// gyroscope y data
	int gyr_y_t;	// time of gyroscope y data

	int gyr_z;		// gyroscope z data
	int gyr_z_t;	// time of gyroscope z data

	int ldr_z;		//lidar z data (altitude)
	int ldr_z_t;	//time of lidar z data

	float pitch_angle_filtered;
	float roll_angle_filtered;
	float lidar_altitude;

	float phi_dot, theta_dot, psi_dot;

	// Structures to hold the current quad position & orientation
	quadPosition_t currentQuadPosition;
	quadTrims_t trims;

} sensor_t;

/**
 * @brief
 *      Holds the setpoints to be used in the controller
 *
 */
typedef struct setpoint_t {
	quadPosition_t desiredQuadPosition;
} setpoint_t;

/**
 * @brief
 *      Holds the parameters that are specific to whatever type of
 *      control algorithm is being used
 *
 */
typedef struct parameter_t {
	PID_t roll_angle_pid, roll_ang_vel_pid;
	PID_t pitch_angle_pid, pitch_ang_vel_pid;
	PID_t yaw_ang_vel_pid;
	PID_t local_x_pid;
	PID_t local_y_pid;
	PID_t yaw_angle_pid;
	PID_t alt_pid;
} parameter_t;

/**
 * @brief
 *      Holds user defined data for the controller
 *
 */
typedef struct user_defined_t {
	int flight_mode;
	int engaging_auto;
} user_defined_t;

/**
 * @brief
 *      Holds the raw actuator values before processing
 *
 */
typedef struct raw_actuator_t {

	int controller_corrected_motor_commands[6];

} raw_actuator_t;

/**
 * @brief
 *      Holds processed commands to go to the actuators
 *
 */
typedef struct actuator_command_t {
	int pwms[4];
} actuator_command_t;

/**
 * @brief
 * 		Structures to be used throughout
 */
typedef struct {
	user_input_t user_input_struct;
	log_t log_struct;
	raw_sensor_t raw_sensor_struct;
	sensor_t sensor_struct;
	setpoint_t setpoint_struct;
	parameter_t parameter_struct;
	user_defined_t user_defined_struct;
	raw_actuator_t raw_actuator_struct;
	actuator_command_t actuator_command_struct;
}modular_structs_t;

//////// END MAIN MODULAR STRUCTS

#endif /* TYPE_DEF_H_ */
