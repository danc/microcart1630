/*
 * log_data.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef LOG_DATA_H_
#define LOG_DATA_H_
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "PID.h"
#include "type_def.h"
#include "uart.h"
#include "sleep.h"
#include "communication.h"

#define LOG_STARTING_SIZE 4096 //262144 // 2^18      32768  2^15


/**
 * @brief 
 *      Logs the data obtained throughout the controller loop.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @return 
 *      error message
 *
 */
 int log_data(log_t* log_struct);

 /**
  * Fills up an xbox hueg amount of memory with log data
  */
 void updateLog(log_t log_struct);

 /**
  * Prints all the log information.
  */
 void printLogging();

 char* format(log_t log);

#endif /* LOG_DATA_H_ */
