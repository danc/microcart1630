################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/PID.c \
../src/actuator_command_processing.c \
../src/commands.c \
../src/communication.c \
../src/control_algorithm.c \
../src/controllers.c \
../src/conversion.c \
../src/iic_mpu9150_utils.c \
../src/initialize_components.c \
../src/log_data.c \
../src/main.c \
../src/new_log_data.c \
../src/packet_processing.c \
../src/platform.c \
../src/send_actuator_commands.c \
../src/sensor.c \
../src/sensor_processing.c \
../src/stringBuilder.c \
../src/timer.c \
../src/uart.c \
../src/update_gui.c \
../src/user_input.c \
../src/util.c 

LD_SRCS += \
../src/Copy\ of\ original\ lscript.ld \
../src/lscript.ld 

OBJS += \
./src/PID.o \
./src/actuator_command_processing.o \
./src/commands.o \
./src/communication.o \
./src/control_algorithm.o \
./src/controllers.o \
./src/conversion.o \
./src/iic_mpu9150_utils.o \
./src/initialize_components.o \
./src/log_data.o \
./src/main.o \
./src/new_log_data.o \
./src/packet_processing.o \
./src/platform.o \
./src/send_actuator_commands.o \
./src/sensor.o \
./src/sensor_processing.o \
./src/stringBuilder.o \
./src/timer.o \
./src/uart.o \
./src/update_gui.o \
./src/user_input.o \
./src/util.o 

C_DEPS += \
./src/PID.d \
./src/actuator_command_processing.d \
./src/commands.d \
./src/communication.d \
./src/control_algorithm.d \
./src/controllers.d \
./src/conversion.d \
./src/iic_mpu9150_utils.d \
./src/initialize_components.d \
./src/log_data.d \
./src/main.d \
./src/new_log_data.d \
./src/packet_processing.d \
./src/platform.d \
./src/send_actuator_commands.d \
./src/sensor.d \
./src/sensor_processing.d \
./src/stringBuilder.d \
./src/timer.d \
./src/uart.d \
./src/update_gui.d \
./src/user_input.d \
./src/util.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -Os -g3 -c -fmessage-length=0 -I../../system_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


