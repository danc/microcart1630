#include "uart.h"
#include "controllers.h"
#include "calibration.h"

extern int motor0_bias, motor1_bias, motor2_bias, motor3_bias;
/**
 * Adjust the bias on the motors
 */
void calibrate_motorspeed() {
	char instr = 0;
	if (uart0_hasData()) {
		instr = uart0_recvByte();

		switch (instr) {
		case '0':
			motor0_bias += 100;
			break;
		case '1':
			motor1_bias += 100;
			break;
		case '2':
			motor2_bias += 100;
			break;
		case '3':
			motor3_bias += 100;
			break;
		case ')':
			motor0_bias -= 100;
			break;
		case '!':
			motor1_bias -= 100;
			break;
		case '@':
			motor2_bias -= 100;
			break;
		case '#':
			motor3_bias -= 100;
			break;
		}
	}
}
