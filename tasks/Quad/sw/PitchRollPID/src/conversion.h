#ifndef _CONVERSION_H
#define _CONVERSION_H

/////// Measured PWM values

// **these are already defined in controllers.h
// in clock cycles measured from pwm_recorders
//#define MAX_YAW_RX_CMD 170300
//#define CENTER_YAW_RX_CMD 149000
//#define MIN_YAW_RX_CMD 129300
//
//#define MAX_ROLL_RX_CMD 170500
//#define CENTER_ROLL_RX_CMD 149900
//#define MIN_ROLL_RX_CMD 129500
//
//#define MAX_PITCH_RX_CMD 190900
//#define CENTER_PITCH_RX_CMD 149900
//#define MIN_PITCH_RX_CMD 129300

//////TARGETS

#define YAW_DEG_TARGET 60.0f
#define YAW_RAD_TARGET ((float) ((YAW_DEG_TARGET * 3.141592) / ((float) 180)))

#define ROLL_DEG_TARGET 15.0f
#define ROLL_RAD_TARGET ((float) ((ROLL_DEG_TARGET * 3.141592) / ((float) 180)))

#define PITCH_DEG_TARGET 15.0f
#define PITCH_RAD_TARGET ((float) ((PITCH_DEG_TARGET * 3.141592) / ((float) 180)))

float convert_from_receiver_cmd(int receiver_cmd, int max_receiver_cmd, int center_receiver_cmd, int min_receiver_cmd, float max_target, float min_target);
int convert_to_receiver_cmd(int var_to_convert, float max_var_to_convert, float min_var_to_convert, int center_receiver_cmd,  int max_receiver_cmd, int min_receiver_cmd);

int map_to_rx_cmd(float percentage, int min_rx_cmd, int center_rx_cmd, int max_rx_cmd);

#endif /* _CONVERSION_H */
