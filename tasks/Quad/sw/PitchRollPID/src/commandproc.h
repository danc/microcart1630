/*
 * commandproc.h
 *
 *  Created on: Sep 24, 2014
 *      Author: Adam
 */


#ifndef COMMANDPROC_H_
#define COMMANDPROC_H_

#include "quadposition.h"
#include "PID.h"

int processPacket(char*, quadPosition_t* currentQuadPosition, quadPosition_t* desiredQuadPosition, PID_t* yawCFPID, PID_t* local_x_pid, PID_t* local_y_pid, PID_t* local_z_pid);
int processUpdate(char*, quadPosition_t* currentQuadPosition);
int processCommand(char*, quadPosition_t* desiredQuadPosition, PID_t* yawCFPID, PID_t* local_x_pid, PID_t* local_y_pid, PID_t* local_z_pid);

#endif /* COMMANDPROC_H_ */
