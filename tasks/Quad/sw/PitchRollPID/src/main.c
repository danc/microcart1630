/*
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

/*
 * Accelerometer and gyro angles on Quad
 *
 * In X Config,
 *
 * R W		2 3
 * R W		0 1
 *
 * (Pitch, Roll) signs are
 *
 * +,+  +,-					This means as roll turns to the right, it's angle is negative and when left, positive
 *
 * -,+  -,-
 *
 */

#include <stdio.h>
#include "platform.h"
#include <xparameters.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "util.h"
#include "xtime_l.h"
#include "uart.h"
#include "controllers.h"
#include "iic_mpu9150_utils.h"
#include "PID.h"
#include "stringBuilder.h"
#include "terminal.h"
#include "commandproc.h"
#include "gam.h"
#include "quadposition.h"
#include "sleep.h"
#include "conversion.h"
#include <xgpiops.h>
#include <xtmrctr.h>

char *channel_types[4] = { "Throttle", "Roll    ", "Pitch   ", "Yaw     " };


#define PI_OVER_180 0.017453278

#define USE_GIMBAL

// desired loop time is not guaranteed (its possible that the loop may take longer than desired)
#define DESIRED_USEC_PER_LOOP 5000 // gives 5ms loops

#define PL_CLK_CNTS_PER_USEC 100

//#define ROLL_PITCH_MAX_ANGLE 0.1745 // 10 degrees (in radians)
#define ROLL_PITCH_MAX_ANGLE 0.3490 // 20 degrees

enum flight_mode{
	AUTO_FLIGHT_MODE,
	MANUAL_FLIGHT_MODE
};

/**
 * Global variables
 */
logging logs = { };

/**
 * Function Prototypes
 */
int initializeAllComponents();  // Initializes all required components
void setPIDCoeff(PID_t* p, float pValue, float iValue, float dValue); // Sets the given PID's coefficients
void waitForArming(); // not implemented yet

void MIO7_led_off();
void MIO7_led_on();
void flash_MIO_7_led(int how_many_times, int ms_between_flashes);

int main() {

	int i;
	int rc_commands[6]; // 6 "receiver_input" elements: Throttle, Pitch, Roll, Yaw, Gear, and Flap
	int pwms[4]; // one PWM for each motor
	double pitch_angle_filtered = 0, roll_angle_filtered = 0; 	// Complementary Roll and Pitch angles
	float time_stamp = 0;
	gam_t gam; 		// GAM struct to hold gyro, accelerometer, and magnetometer data


	// these variables help provide a way to test step responses without using ground station commands (use the flap switch to change setpoints)
	// flap_setpoint always starts at 0.0, so the first setpoint in step_target_list should be the first step change
	// the step_index will iterate through step_target_list as a circular array index
//	float step_target_list[] = {0.0175, 0.0, -0.0175, 0.0};
//	int step_index = 0;
//	int last_flap_reading;
//	float flap_setpoint = 0.0;

	//TODO

	// Structures to hold the current and desired quad position & orientation
	quadPosition_t currentQuadPosition = { };
	quadPosition_t desiredQuadPosition = { };
	quadPosition_t trimmedRCValues = { };

	MIO7_led_off();

	// HUMAN Piloted (RC) PID DEFINITIONS //////
	// RC PIDs for roll (2 loops: angle --> angular velocity)
	PID_t roll_angle_pid = { }, roll_ang_vel_pid = { };
	roll_angle_pid.dt = 0.005; roll_ang_vel_pid.dt = 0.005; // 5 ms calculation period

	// RC PIDs for pitch (2 loops: angle --> angular velocity)
	PID_t pitch_angle_pid = { }, pitch_ang_vel_pid = { };
	pitch_angle_pid.dt = 0.005; pitch_ang_vel_pid.dt = 0.005; // 5 ms calculation period

	// initialize Yaw PID_t and PID constants
	// RC PID for yaw (1 loop angular velocity)
	PID_t yaw_ang_vel_pid = { };
	yaw_ang_vel_pid.dt = 0.005; // 5 ms calculation period

	// CAMERA CONTROLLED (CAM) PID DEFINITIONS //////
	// Local X PID using a translation from X camera system data to quad local X position (3 loops: local y position --> angle --> angular velocity)
	PID_t local_x_pid = { };
	local_x_pid.dt = 0.100;

	// Local Y PID using a translation from Y camera system data to quad local Y position(3 loops: local x position --> angle --> angular velocity)
	PID_t local_y_pid = { };
	local_y_pid.dt = 0.100;

	// CAM PIDs for yaw (2 loops angle --> angular velocity)
	PID_t yaw_angle_pid = { };
	yaw_angle_pid.dt = 0.100;

	// CAM PID for altitude (1 loop altitude)
	PID_t alt_pid = { };
	alt_pid.dt = 0.100;

	// Initialize all required components:
	// Uart, PWM receiver/generator, I2C, Sensor Board
	int init_error = initializeAllComponents();
	if (init_error == -1) {
		printf("ERROR (main): Problem initializing...Goodbye\r\n");
		return init_error;
	}

	//javey: you can find the PID constants in PID.h

	// PID coeffiecients (Position)
	setPIDCoeff(&local_y_pid, YPOS_KP, YPOS_KI, YPOS_KD);
	setPIDCoeff(&local_x_pid, XPOS_KP, XPOS_KI, XPOS_KD);
	setPIDCoeff(&alt_pid, ALT_ZPOS_KP, ALT_ZPOS_KI, ALT_ZPOS_KD);

	// PID coefficients (Angle)
	setPIDCoeff(&pitch_angle_pid, PITCH_ANGLE_KP, PITCH_ANGLE_KI, PITCH_ANGLE_KD);
	setPIDCoeff(&roll_angle_pid, ROLL_ANGLE_KP, ROLL_ANGLE_KI, ROLL_ANGLE_KD);
	setPIDCoeff(&yaw_angle_pid, YAW_ANGLE_KP, YAW_ANGLE_KI, YAW_ANGLE_KD);

	// PID coefficients (Angular Velocity)
	setPIDCoeff(&pitch_ang_vel_pid, PITCH_ANGULAR_VELOCITY_KP, PITCH_ANGULAR_VELOCITY_KI, PITCH_ANGULAR_VELOCITY_KD);
	setPIDCoeff(&roll_ang_vel_pid, ROLL_ANGULAR_VELOCITY_KP, ROLL_ANGULAR_VELOCITY_KI, ROLL_ANGULAR_VELOCITY_KD);
	setPIDCoeff(&yaw_ang_vel_pid, YAW_ANGULAR_VELOCITY_KP, YAW_ANGULAR_VELOCITY_KI, YAW_ANGULAR_VELOCITY_KD);

	XTime before = 0, after = 0;

	// using a different timer (axi_timer core) because we've had problems with the Global Timer
	XTmrCtr axi_timer;
	XTmrCtr_Initialize(&axi_timer, XPAR_AXI_TIMER_0_DEVICE_ID);

	stringBuilder_t* sb = stringBuilder_create();
	usleep(1e6);

	int started = 0;
//	int loop_counter = 0;

	read_rec_all(rc_commands);

	// Protection Loops: //////////////
	// wait for RC receiver to connect to transmitter
	while(rc_commands[THROTTLE] < 75000)
		read_rec_all(rc_commands);

	// wait for ground station (bluetooth/WiFi) connection
//	while(!tryReceivePacket(sb, 0));

	// wait until throttle is low and the gear switch is engaged (so you don't immediately break out of the main loop below)
	// also wait for the flight mode to be set to manual
	while(rc_commands[THROTTLE] > 125000 || read_kill(rc_commands[GEAR]) || !read_flap(rc_commands[FLAP]))
		read_rec_all(rc_commands);

	//last_flap_reading = read_flap(rc_commands[FLAP]);

	// let the pilot/observers know that the quad is now active
	MIO7_led_on();

//	uart0_sendStr("This test is printing data every quarter sec. The type of data should be specified below. The camera system data (vrpnhandler) was not sent, on this test\n"
//			"in order to free the bluetooth for the quad to send live data to the ground station.\n");


	// initial loop uses accelerometer measured angles only
	XTime_GetTime(&before);

	// read sensor board and fill in gryo/accelerometer/magnetometer struct
	get_gam_reading(&gam);

	// Sets the first iteration to be at the accelerometer value since gyro initializes to {0,0,0} regardless of orientation
	pitch_angle_filtered = gam.accel_roll;
	roll_angle_filtered = gam.accel_pitch;

	XTime_GetTime(&after);

	// Main Flying Loop:
	// 1.Read in RC Receiver values
	// 2.Collect data from sensor board
	// 3.Calculate PID corrections (Outer and Inner)
	// 4.Output PWMs to motors
	// 5.Check if command from base station
	// 6.Update log
	while (1) {

		//timing code
		float LOOP_TIME = ((float)(after - before)) / ((float) COUNTS_PER_SECOND);
		//loop time debugging
//		char dMsg[1000] = {};
//		sprintf(dMsg, "%.6f\n", LOOP_TIME);
//		uart0_sendStr(dMsg);

		static float average_loop_time = 0;
		static float total_loop_time = 0;
		static int loop_counter = 1;
		total_loop_time += LOOP_TIME;
		average_loop_time = total_loop_time / ((float)loop_counter);

		XTime_GetTime(&before);
		XTmrCtr_Reset(&axi_timer, 0);
		XTmrCtr_Start(&axi_timer, 0);

		static int last_flight_mode = MANUAL_FLIGHT_MODE;
		static int throttle_hover = 0;
		static int engaging_auto = 0;
//		static int switched_to_auto = 0;
//		static int recvd_packet_auto_switch = 0;

		int controller_corrected_motor_commands[6];

		// Read in values from RC Receiver
		read_rec_all(rc_commands);

		// here for now so in case any flight command is not pid controlled, it will default to rc_command value:
		memcpy(controller_corrected_motor_commands, rc_commands, sizeof(int) * 6); // space for 6 ints in bytes

		//create setpoints for manual flight
		//if(manual flight) {

		// currently in units of radians
		float yaw_manual_setpoint = convert_from_receiver_cmd(rc_commands[YAW], YAW_MAX, YAW_CENTER, YAW_MIN, YAW_RAD_TARGET, -(YAW_RAD_TARGET));

		float roll_angle_manual_setpoint = convert_from_receiver_cmd(rc_commands[ROLL], ROLL_MAX, ROLL_CENTER, ROLL_MIN, ROLL_RAD_TARGET, -(ROLL_RAD_TARGET));
		float pitch_angle_manual_setpoint = convert_from_receiver_cmd(rc_commands[PITCH], PITCH_MAX, PITCH_CENTER, PITCH_MIN, PITCH_RAD_TARGET, -(PITCH_RAD_TARGET));


		//} //end manual flight


//		int current_flap_reading = read_flap(rc_commands[FLAP]);

		// if the flap has toggled change the setpoint
//		if(current_flap_reading != last_flap_reading)
//		{
//			flap_setpoint = step_target_list[step_index];
//			step_index++;
//			step_index = step_index % (sizeof(step_target_list)/ sizeof(float));
//		}
//
//		last_flap_reading = current_flap_reading;

//		if (!started && rc_commands[THROTTLE] > 150000) {
//			started = 1;
//
//		}

		// check if the gear switch is flipped (to kill)
		if (read_kill(rc_commands[GEAR])) {
			break;
		}

		//or the left stick is down and to the right (also kills the system)
//		else if(started && rc_commands[THROTTLE] < 118000 && rc_commands[YAW] > 170000){
//			break;
//		}

		// Listen on bluetooth and if there's a packet,
		// then receive the packet and process it.
		// Use the stringbuilder to keep track of data received
		int echo = 0;
		int hasPacket = tryReceivePacket(sb, echo);
		if (hasPacket) {
			processPacket(sb->buf, &currentQuadPosition, &desiredQuadPosition, &yaw_angle_pid, &local_x_pid, &local_y_pid, &alt_pid);
			sb->clear(sb);
			uart0_clearFIFOs();

			if(engaging_auto == 1)
				engaging_auto = 2;
		}

		// Read sensor board data (currently just saves accelerometer and gyro data and not magnetometer)
		get_gam_reading(&gam);
		gam.heading = 0;

		// read flight mode and handle

		// use the 'flap' switch as the flight mode selector
		int fm_switch = read_flap(rc_commands[FLAP]); // MANUAL_FLIGHT_MODE
		static int flight_mode = MANUAL_FLIGHT_MODE;

		// reset flight_mode to MANUAL right away if the flap switch is in manual position
		// to engage AUTO mode the code waits for a new packet after the flap is switched to auto
		// before actually engaging AUTO mode
		if(fm_switch == MANUAL_FLIGHT_MODE)
			flight_mode = MANUAL_FLIGHT_MODE;

		// flap switch was just toggled to auto flight mode
		if((last_flight_mode != fm_switch) && (fm_switch == AUTO_FLIGHT_MODE))
		{
			engaging_auto = 1;

			// Read in trimmed values because it should read trim values right when the pilot flips the flight mode switch
			trimmedRCValues.pitch = pitch_angle_manual_setpoint; //rc_commands[PITCH] - PITCH_CENTER;
			trimmedRCValues.roll = roll_angle_manual_setpoint; //rc_commands[ROLL] - ROLL_CENTER;
			//trimmedRCValues.yaw = yaw_manual_setpoint; //rc_commands[YAW] - YAW_CENTER;
			throttle_hover = rc_commands[THROTTLE];
		}


		// if the flap switch was toggled to AUTO_FLIGHT_MODE and we've received a new packet
		// then record the current position as the desired position
		// also reset the previous error and accumulated error from the position PIDs
		if((fm_switch == AUTO_FLIGHT_MODE) && (engaging_auto == 2))
		{
			// zero out the accumulated error so the I terms don't cause wild things to happen
			alt_pid.acc_error = 0.0;
			local_x_pid.acc_error = 0.0;
			local_y_pid.acc_error = 0.0;

			// make previous error equal to the current so the D term doesn't spike
			alt_pid.prev_error = 0.0;
			local_x_pid.prev_error = 0.0;
			local_y_pid.prev_error = 0.0;

			desiredQuadPosition.alt_pos = currentQuadPosition.alt_pos;
			desiredQuadPosition.x_pos = currentQuadPosition.x_pos;
			desiredQuadPosition.y_pos = currentQuadPosition.y_pos;
			desiredQuadPosition.yaw = 0.0;//currentQuadPosition.yaw;

			// reset the flag that engages auto mode
			engaging_auto = 0;

			flight_mode = AUTO_FLIGHT_MODE;
		}



		// Calculate Euler angles and velocities using Gimbal Equations below
		/////////////////////////////////////////////////////////////////////////
		// | Phi_d   |   |  1  sin(Phi)tan(theta)    cos(Phi)tan(theta) |  | p |
		// | theta_d | = |  0  cos(Phi)              -sin(Phi)		    |  | q |
		// | Psi_d   |   |  0  sin(Phi)sec(theta)    cos(Phi)sec(theat) |  | r |
		//
		// Phi_dot = p + q sin(Phi) tan(theta) + r cos(Phi) tan(theta)
		// theta_dot = q cos(Phi) - r sin(Phi)
		// Psi_dot = q sin(Phi) sec(theta) + r cos(Phi) sec(theta)
		///////////////////////////////////////////////////////////////////////////

		// javey:
		//
		// The gimbal equations are defined in the book "Flight Simulation" by Rolfe and Staples.
		// Find on page 46, equation 3.6

		// these are calculated to be used in the gimbal equations below
		// the variable roll(pitch)_angle_filtered is phi(theta)
		double sin_phi = sin(roll_angle_filtered);
		double cos_phi = cos(roll_angle_filtered);
		double tan_theta = tan(pitch_angle_filtered);
		double sec_theta = 1/cos(pitch_angle_filtered);

		/* javey:
		 *
		 * Gryo "p" is the angular velocity rotation about the x-axis (defined as var gyro_xVel_p in gam struct)
		 * Gyro "q" is the angular velocity rotation about the y-axis (defined as var gyro_xVel_q in gam struct)
		 * Gyro "r" is the angular velocity rotation about the z-axis (defined as var gyro_xVel_r in gam struct)
		 *
		 */

		// Gimbal Equations (find these in the Flight Simulation book page 46)

		// phi is the conventional symbol used for roll angle, so phi_dot is the roll velocity
		double phi_dot = gam.gyro_xVel_p + (gam.gyro_yVel_q*sin_phi*tan_theta) + (gam.gyro_zVel_r*cos_phi*tan_theta);

		// theta is the conventional symbol used for pitch angle, so theta_dot is the pitch velocity
		double theta_dot = (gam.gyro_yVel_q*cos_phi) - (gam.gyro_zVel_r*sin_phi);

		// psi is the conventional symbol used for yaw angle, so psi_dot is the yaw velocity
		double psi_dot = (gam.gyro_yVel_q*sin_phi*sec_theta)+ (gam.gyro_zVel_r*cos_phi*sec_theta);

		// Complementary Filter Calculations
		pitch_angle_filtered = 0.98 * (pitch_angle_filtered + theta_dot * LOOP_TIME)
				+ 0.02 * gam.accel_pitch;

		roll_angle_filtered = 0.98 * (roll_angle_filtered + phi_dot* LOOP_TIME)
				+ 0.02 * gam.accel_roll;


		//PIDS///////////////////////////////////////////////////////////////////////

		/* 					Position loop
		 * Reads current position, and outputs
		 * a pitch or roll for the angle loop PIDs
		 */

//		static int counter_between_packets = 0;

		if(hasPacket)
		{
			local_y_pid.current_point = currentQuadPosition.y_pos;
			local_y_pid.setpoint = desiredQuadPosition.y_pos;

			local_x_pid.current_point = currentQuadPosition.x_pos;
			local_x_pid.setpoint = desiredQuadPosition.x_pos;

			alt_pid.current_point = currentQuadPosition.alt_pos;
			alt_pid.setpoint = desiredQuadPosition.alt_pos;

			logs.local_y_PID_values = pid_computation(&local_y_pid);
			logs.local_x_PID_values = pid_computation(&local_x_pid);
			logs.altitude_PID_values = pid_computation(&alt_pid);

			// yaw angular position PID calculation
			yaw_angle_pid.current_point = currentQuadPosition.yaw;// in radians
			yaw_angle_pid.setpoint = desiredQuadPosition.yaw; // constant setpoint
			logs.angle_yaw_PID_values = pid_computation(&yaw_angle_pid);

			// for testing data update rate
			char debug_msg[1000] = {};
			sprintf(debug_msg, "x: %.5f\ty: %.5f\n",  currentQuadPosition.x_pos, currentQuadPosition.y_pos);
			uart0_sendStr(debug_msg);
//			counter_between_packets = 1;
		}
//		else
//		{
//			counter_between_packets++;
//		}


		/* 					Angle loop
		 * Calculates current orientation, and outputs
		 * a pitch, roll, or yaw velocity for the angular velocity loop PIDs
		 */

		//angle boundaries
		if(local_x_pid.pid_correction > ROLL_PITCH_MAX_ANGLE)
		{
			local_x_pid.pid_correction = ROLL_PITCH_MAX_ANGLE;
		}
		if(local_x_pid.pid_correction < -ROLL_PITCH_MAX_ANGLE)
		{
			local_x_pid.pid_correction = -ROLL_PITCH_MAX_ANGLE;
		}
		if(local_y_pid.pid_correction > ROLL_PITCH_MAX_ANGLE)
		{
			local_y_pid.pid_correction = ROLL_PITCH_MAX_ANGLE;
		}
		if(local_y_pid.pid_correction < -ROLL_PITCH_MAX_ANGLE)
		{
			local_y_pid.pid_correction = -ROLL_PITCH_MAX_ANGLE;
		}

		//TODO

		pitch_angle_pid.current_point = pitch_angle_filtered;
		pitch_angle_pid.setpoint = (flight_mode == AUTO_FLIGHT_MODE)? (-local_x_pid.pid_correction + trimmedRCValues.pitch) : pitch_angle_manual_setpoint;//pitch_angle_manual_setpoint; // need this to be negative because of the way Pitch works (forward is negative, backward is positive)
		//flap_setpoint

		roll_angle_pid.current_point = roll_angle_filtered;
		roll_angle_pid.setpoint = (flight_mode == AUTO_FLIGHT_MODE)? (local_y_pid.pid_correction + trimmedRCValues.roll) : roll_angle_manual_setpoint;//local_y_pid.pid_correction;


		logs.angle_pitch_PID_values = pid_computation(&pitch_angle_pid);
		logs.angle_roll_PID_values = pid_computation(&roll_angle_pid);


		/* 				Angular Velocity Loop
		 * Takes the desired angular velocity from the angle loop,
		 * and calculates a PID correction with the current angular velocity
		 */

		// theta_dot is the angular velocity about the y-axis
		// it is calculated from using the gimbal equations
		pitch_ang_vel_pid.current_point = theta_dot;
		pitch_ang_vel_pid.setpoint = pitch_angle_pid.pid_correction;

		// phi_dot is the angular velocity about the x-axis
		// it is calculated from using the gimbal equations
		roll_ang_vel_pid.current_point = phi_dot;
		roll_ang_vel_pid.setpoint = roll_angle_pid.pid_correction;

		// Yaw angular velocity PID
		// psi_dot is the angular velocity about the z-axis
		// it is calculated from using the gimbal equations
		yaw_ang_vel_pid.current_point = psi_dot;
		yaw_ang_vel_pid.setpoint = (flight_mode == AUTO_FLIGHT_MODE)? yaw_angle_pid.pid_correction : yaw_manual_setpoint; // no trim added because the controller already works well

		logs.ang_vel_pitch_PID_values = pid_computation(&pitch_ang_vel_pid);
		logs.ang_vel_roll_PID_values = pid_computation(&roll_ang_vel_pid);
		logs.ang_vel_yaw_PID_values = pid_computation(&yaw_ang_vel_pid);

		//END PIDs///////////////////////////////////////////////////////////////////////

		/*
		 * Motor corrections
		 */

		// convert back into CC units
//		int yaw_correction = convert_to_receiver_cmd(yaw_ang_vel_pid.pid_correction, YAW_DEG_TARGET, -YAW_DEG_TARGET, CENTER_YAW_RX_CMD, MAX_YAW_RX_CMD, MIN_YAW_RX_CMD);
//		int roll_correction = convert_to_receiver_cmd(roll_ang_vel_pid.pid_correction, POS_ROLL_DEG_SEC_TARGET, NEG_ROLL_DEG_SEC_TARGET, CENTER_ROLL_RX_CMD, MAX_ROLL_RX_CMD, MIN_ROLL_RX_CMD);
//		int pitch_correction = convert_to_receiver_cmd(pitch_ang_vel_pid.pid_correction, POS_PITCH_DEG_SEC_TARGET, NEG_PITCH_DEG_SEC_TARGET, CENTER_PITCH_RX_CMD, MAX_PITCH_RX_CMD, MIN_PITCH_RX_CMD);

		// don't use the PID corrections if the throttle is less than about 10% of its range
		if((rc_commands[THROTTLE] > 118000) || (flight_mode == AUTO_FLIGHT_MODE))
		{
			int roll_correction = map_to_rx_cmd(roll_ang_vel_pid.pid_correction, ROLL_MIN, ROLL_CENTER, ROLL_MAX);
			//int roll_correction = map_to_rx_cmd(roll_ang_vel_pid.pid_correction, 110000, 151000, 192000);

			// uses PITCH_CENTER + (PITCH_CENTER - PITCH_MIN) instead of PITCH_MAX to get equal ranges on both sides of PITCH_CENTER (PITCH_MAX is farther away from PITCH_CENTER than PITCH_MIN is)
			// could eventually change pitch center value instead of pitch max value to be the center point between PITCH_MIN/MAX, but for now the motor constants
			// work better for having pitch center value as PITCH_CENTER
			int pitch_correction = map_to_rx_cmd(pitch_ang_vel_pid.pid_correction, PITCH_MIN, PITCH_CENTER, PITCH_MAX);
			//int pitch_correction = map_to_rx_cmd(pitch_ang_vel_pid.pid_correction, 110000, 151000, 192000);

			// the yaw_angular_velocity correction now gets translated from an RC value into motor PWMs
			int yaw_correction = map_to_rx_cmd(yaw_ang_vel_pid.pid_correction, YAW_MIN, (YAW_MIN + YAW_MAX)/2, YAW_MAX);
			//int yaw_correction = map_to_rx_cmd(yaw_ang_vel_pid.pid_correction, 110000, 151000, 192000);

			int throttle_correction = ((int)alt_pid.pid_correction) + throttle_hover;

			controller_corrected_motor_commands[ROLL] = roll_correction;
			controller_corrected_motor_commands[PITCH] = pitch_correction;
			controller_corrected_motor_commands[YAW] = yaw_correction;
			if(flight_mode == AUTO_FLIGHT_MODE) controller_corrected_motor_commands[THROTTLE] = throttle_correction;
		}

		// Convert from the aero inputs to PWMS
		Aero_to_PWMS(pwms, controller_corrected_motor_commands);

		// write the PWMs to the motors
		for (i = 0; i < 4; i++)
			pwm_write_channel(pwms[i], i);

//		static int switch_flag = 1;
//		static int counter = 0;
//		//debugging
//		if(flap_setpoint > 0.1)
//		{
//			if(loop_counter == 50)
//			{
//				char msg[100] = {};
//				sprintf(msg, "cmd throttle: %d\tPWM_AVERAGE_CC: %d\tPWM_AVERAGE_PERCENT: %.2f%%\n", rc_commands[THROTTLE], (pwms[0] + pwms[1] + pwms[2] + pwms[3])/4, ((pwms[0] + pwms[1] + pwms[2] + pwms[3])/4)/222222.0 * 100.0);
//				uart0_sendStr(msg);
//			}
//			switch_flag = 1;
//		}
//		else if (switch_flag == 1)
//		{
//			switch_flag = 0;
//			counter++;
//			char msg[100] = {};
//			sprintf(msg, "-------------Trial %d--------------\n", counter);
//			uart0_sendStr(msg);
//		}

		// log every quarter sec
		//if (loop_counter == 50) {
		if(flight_mode == AUTO_FLIGHT_MODE)
		{
			//log the data from this loop
			logs.time_stamp = time_stamp;
			logs.time_slice = LOOP_TIME;

			logs.packetId = currentQuadPosition.packetId;

			logs.gam = gam; //Because C beats all

			logs.pitch_angle = pitch_angle_filtered;
			logs.roll_angle = roll_angle_filtered;

			logs.x_pos = currentQuadPosition.x_pos;
			logs.y_pos = currentQuadPosition.y_pos;
			logs.altitude_z = currentQuadPosition.alt_pos;

			logs.local_x_PID = local_x_pid;
			logs.local_y_PID = local_y_pid;
			logs.altitude_PID = alt_pid;

			logs.cam_roll = currentQuadPosition.roll;
			logs.cam_pitch = currentQuadPosition.pitch;
			logs.cam_yaw = currentQuadPosition.yaw;

			logs.phi_dot = phi_dot;
			logs.theta_dot = theta_dot;
			logs.psi_dot = psi_dot;

			logs.angle_roll_PID = roll_angle_pid;
			logs.angle_pitch_PID = pitch_angle_pid;
			logs.angle_yaw_PID = yaw_angle_pid;

			logs.ang_vel_roll_PID = roll_ang_vel_pid;
			logs.ang_vel_pitch_PID = pitch_ang_vel_pid;
			logs.ang_vel_yaw_PID = yaw_ang_vel_pid;

			logs.commands.throttle = controller_corrected_motor_commands[THROTTLE];
			logs.commands.roll = controller_corrected_motor_commands[ROLL];
			logs.commands.pitch = controller_corrected_motor_commands[PITCH];
			logs.commands.yaw = controller_corrected_motor_commands[YAW];

			logs.motors[0] = pwms[0];
			logs.motors[1] = pwms[1];
			logs.motors[2] = pwms[2];
			logs.motors[3] = pwms[3];

			updateLog();
		}

//			char msg[100] = {};
//			sprintf(msg,
//				"rc_roll: %d\trc_pitch: %d\trc_throttle: %d\trc_yaw: %d\n",
//				rc_commands[ROLL], rc_commands[PITCH], rc_commands[THROTTLE], rc_commands[YAW]);
//			uart0_sendStr(msg);


		//	loop_counter = 0;
		//}
		//else
//			++loop_counter;


		// javey: print test data via bluetooth
//		static char loop_count_flag = 0;
//		loop_count_flag++;
//		char msg[1000];
//		if(loop_count_flag == 50) // runs every quarter sec (250ms)
//		{
//			sprintf(msg, "theta: %.2f\tang_target: %.2f\tvel_target: %.2f\ttheta_dot: %.2f\tcorrection: %.2f\t",
//					pitch_angle_filtered,  pitch_angle_pid.setpoint, pitch_ang_vel_pid.setpoint, theta_dot, pitch_ang_vel_pid.pid_correction);
//			sprintf(msg, "theta_dot: %.2f\tphi_dot: %.2f\tpsi_dot: %.2f\tgyro_x: %.2f\tgyro_y: %.2f\tgyro_z: %.2f\t", theta_dot, phi_dot, psi_dot, gam.gyro_xVel_p, gam.gyro_yVel_q, gam.gyro_zVel_r);
//			sprintf(msg, "pwm0: %d\tpwm1: %d\tpwm2: %d\tpwm3: %d\t", pwms[0], pwms[1], pwms[2], pwms[3]);
//			sprintf(msg, "pitch: %d\troll: %d\tthrottle: %d\tyaw: %d\t", controller_corrected_motor_commands[PITCH], controller_corrected_motor_commands[ROLL], controller_corrected_motor_commands[THROTTLE], controller_corrected_motor_commands[YAW]);
//			uart0_sendStr(msg);
//
//			loop_count_flag = 0;
//		}

		// get number of useconds its taken to run the loop thus far
		int usec_loop = XTmrCtr_GetValue(&axi_timer, 0) / (PL_CLK_CNTS_PER_USEC);

		// attempt to make each loop run for the same amount of time
		while(usec_loop < DESIRED_USEC_PER_LOOP)
		{
			usec_loop = XTmrCtr_GetValue(&axi_timer, 0) / (PL_CLK_CNTS_PER_USEC);
		}

		//timing code
		XTime_GetTime(&after);
		time_stamp += LOOP_TIME;
		XTmrCtr_Stop(&axi_timer, 0);

		last_flight_mode = fm_switch;

	} //end while(1)

	stringBuilder_free(sb);

	pwm_kill();

	MIO7_led_off();

	printLogging();

	flash_MIO_7_led(10, 100);

	return 0;
} //main

void flash_MIO_7_led(int how_many_times, int ms_between_flashes)
{
	if(how_many_times <= 0)
		return;

	while(how_many_times)
	{
		MIO7_led_on();

		usleep(ms_between_flashes * 500);

		MIO7_led_off();

		usleep(ms_between_flashes * 500);

		how_many_times--;
	}
}

int initializeAllComponents() {

	int i2c_rc, mpu_rc;

	// Xilinx given initialization
	init_platform();

	// Initialize UART0 (Bluetooth)
	uart0_init(XPAR_PS7_UART_0_DEVICE_ID, 921600);
	uart0_clearFIFOs();

	// Initialize I2C controller and start the sensor board
	i2c_rc = initI2C0();
	mpu_rc = startMPU9150();

	if (i2c_rc == -1 || mpu_rc == -1) {
		return -1;
	}

	// Initialize PWM Recorders and Motor outputs
	pwm_init();

	printf("\nStarting\r\n");
	return 0;
}

void setPIDCoeff(PID_t* p, float pValue, float iValue, float dValue) {

	p->Kp = pValue;
	p->Ki = iValue;
	p->Kd = dValue;

}

/*void MIO7_led_off()
{
	XGpioPs Gpio;

	XGpioPs_Config * ConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);

	XGpioPs_SetDirectionPin(&Gpio, 7, 1);

	// Disable LED
	XGpioPs_WritePin(&Gpio, 7, 0x00);
}

void MIO7_led_on()
{
	XGpioPs Gpio;

	XGpioPs_Config * ConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);

	XGpioPs_SetDirectionPin(&Gpio, 7, 1);

	// Enable LED
	XGpioPs_WritePin(&Gpio, 7, 0x01);
}*/
