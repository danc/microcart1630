#ifndef _QUADPOSITION_H
#define _QUADPOSITION_H

//Camera system info
typedef struct {
	int packetId;

	double y_pos;
	double x_pos;
	double alt_pos;

	double yaw;
	double roll;
	double pitch;
} quadPosition_t;

#endif /* _QUADPOSITION_H */
