/*
 * util.c
 *
 *  Created on: Oct 11, 2014
 *      Author: Tyler
 */

#include "util.h"
#include "controllers.h"
#include "xparameters.h"
#include <stdio.h>
#include <math.h>
#include <sleep.h>
#include <string.h>
#include "uart.h"
#include <stdlib.h>


extern int motor0_bias, motor1_bias, motor2_bias, motor3_bias;
//Global variable representing the current pulseW
int pulseW = pulse_throttle_low;

// Current index of the log array
int arrayIndex = 0;
// Size of the array
int arraySize = LOG_STARTING_SIZE;

// The number of times we resized the array
int resizeCount = 0;
// Pointer to point to the array with all the logging information
logging* logArray = NULL;



/**
 * Initializes the PWM output components.
 * Default pulse length  = 1 ms
 * Default period length = 2.33 ms
 */
void pwm_init() {
	printf("Period initialization starting\r\n");

	int* pwm_0 = (int*) PWM_0_ADDR + PWM_PERIOD;
	int* pwm_1 = (int*) PWM_1_ADDR + PWM_PERIOD;
	int* pwm_2 = (int*) PWM_2_ADDR + PWM_PERIOD;
	int* pwm_3 = (int*) PWM_3_ADDR + PWM_PERIOD;

	// Initializes all the PWM address to have the correct period width at 450 hz
	*pwm_0 = period_width;
	*pwm_1 = period_width;
	*pwm_2 = period_width;
	*pwm_3 = period_width;
	printf("Period initialization successful %d\n", period_width);
	// Initializes the PWM pulse lengths to be 1 ms
	*(int*) (PWM_0_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_1_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_2_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_3_ADDR + PWM_PULSE) = pulse_throttle_low;
	printf("Pulse initialization successful %d\n", pulse_throttle_low);

#ifdef X_CONFIG
	printf("In x config mode\n");
#else
	printf("In + config mode\n");
#endif
	usleep(100000);
}

/**
 * Writes all PWM components to be the same given pulsewidth
 */
void pwm_write_all(int pulseWidth) {
	// Check lower and upper bounds
	if (pulseWidth > pulse_throttle_high)
		pulseWidth = pulse_throttle_high;
	if (pulseWidth < pulse_throttle_low)
		pulseWidth = pulse_throttle_low;
	// Set all the pulse widths
	*(int*) (PWM_0_ADDR + PWM_PULSE) = pulseWidth;
	*(int*) (PWM_1_ADDR + PWM_PULSE) = pulseWidth;
	*(int*) (PWM_2_ADDR + PWM_PULSE) = pulseWidth;
	*(int*) (PWM_3_ADDR + PWM_PULSE) = pulseWidth;
}
/**
 * Write a given pulseWidth to a channel
 */
void pwm_write_channel(int pulseWidth, int channel){
	// Check lower and upper bounds
		if (pulseWidth > pulse_throttle_high)
			pulseWidth = pulse_throttle_high;
		if (pulseWidth < pulse_throttle_low)
			pulseWidth = pulse_throttle_low;

		switch(channel){
		case 0:
			*(int*) (PWM_0_ADDR + PWM_PULSE) = pulseWidth;
			break;
		case 1:
			*(int*) (PWM_1_ADDR + PWM_PULSE) = pulseWidth;
			break;
		case 2:
			*(int*) (PWM_2_ADDR + PWM_PULSE) = pulseWidth;
			break;
		case 3:
			*(int*) (PWM_3_ADDR + PWM_PULSE) = pulseWidth;
			break;
		default:
			break;
		}
}
/**
 * Reads the registers from the PWM_Recorders, and returns the pulse width
 * of the last PWM signal to come in
 */
int read_rec(int channel) {
	switch (channel) {
	case 0:
		return *((int*) PWM_REC_0_ADDR);
	case 1:
		return *((int*) PWM_REC_1_ADDR);
	case 2:
		return *((int*) PWM_REC_2_ADDR);
	case 3:
		return *((int*) PWM_REC_3_ADDR);
	case 4:
		return *((int*) PWM_REC_4_ADDR);
	case 5:
		return *((int*) PWM_REC_5_ADDR);
	default:
		return 0;
	}
}
/**
 * Reads all 4 receiver channels at once
 */
void read_rec_all(int* mixer){
	int i;
	for(i = 0; i < 6; i++){
		mixer[i] = read_rec(i);
	}

	//mixer[PITCH] = mixer[PITCH] / 2 + 75000;
	//mixer[ROLL]  = mixer[ROLL]  / 2 + 73500;

//	mixer[PITCH] = ((mixer[PITCH]-100000)/10.0)*3.0 + 135000;
//	mixer[ROLL] = ((mixer[ROLL]-100000)/10.0)*3.0 + 132500;

	logs.commands.pitch    = mixer[PITCH];
	logs.commands.roll     = mixer[ROLL];
	logs.commands.throttle = mixer[THROTTLE];
	logs.commands.yaw      = mixer[YAW];
}

int hexStrToInt(char *buf, int startIdx, int endIdx) {
	int result = 0;
	int i;
	int power = 0;
	for (i=endIdx; i >= startIdx; i--) {
		int value = buf[i];
		if ('0' <= value && value <= '9') {
			value -= '0';
		} else if ('a' <= value && value <= 'f') {
			value -= 'a';
			value += 10;
		} else if ('A' <= value && value <= 'F') {
			value -= 'A';
			value += 10;
		}

		result += (2 << (4 * power)) * value;
		power++;
	}

	return result;
}

void read_bluetooth_all(int* mixer) {
	char buffer[32];

	int done = 0;
	int gotS = 0;
	char c;
	while (!done) {
		int counter = 0;
		if (!gotS) {
			c = uart0_recvByte();
		}
		if (c == 'S') {

			while (1) {
				char cc = uart0_recvByte();
				if (cc == 'S') {
									counter = 0;
									gotS = 1;
									break;
								}
				printf("C=%c,\r\n",cc);
				buffer[counter++] = cc;


				if (counter == 12) {
					buffer[12] = 0;
					done = 1;
					gotS = 0;
				}
			}
			//uart0_recvBytes(buffer, 12);
			//buffer[12] = 0;


		}
	}

//	// data := "XX XX XX XX XX"
//	uart0_recvBytes(buffer, 12);
//	buffer[12] = 0;
//
//
	int i;
	for(i=0; i < 5; i++) {
		mixer[i] = 0;
	}

	for (i=0; i < 4; i++) {
		//mixer[i] = hexStrToInt(buffer, 3*i, 3*i + 1);
		mixer[i] = (buffer[i*3] << 8) | buffer[i*3 + 1];
	}

	printf("mixer: \"%s\" -> %d %d %d %d %d\r\n", buffer, mixer[0], mixer[1], mixer[2], mixer[3], mixer[4]);

}

/**
 * Use the buttons to drive the pulse length of the channels
 */
void b_drive_pulse() {
	int* btns = (int *) XPAR_BTNS_BASEADDR;

	// Increment the pulse width by 5% throttle
	if (*btns & 0x1) {
		pulseW += 1000;
		if (pulseW > 200000)
			pulseW = 200000;
		pwm_write_all(pulseW);
		while (*btns & 0x1)
			;
	} //Decrease the pulse width by 5% throttle
	else if (*btns & 0x2) {
		pulseW -= 1000;
		if (pulseW < 100000) {
			pulseW = 100000;
		}
		pwm_write_all(pulseW);
		while (*btns & 0x2)
			;
	}
	// Set the pulses back to default
	else if (*btns & 0x4) {
		pulseW = MOTOR_0_PERCENT;
		pwm_write_all(pulseW);
	}
	// Read the pulse width of pwm_recorder 0
	else if (*btns & 0x8) {
		int i;
		for(i = 0; i < 4; i++){
			xil_printf("Channel %d:  %d\n", i, read_rec(i));
		}
		//xil_printf("%d\n",pulseW);
		while (*btns & 0x8)
			;
	}
}

/**
 * Creates a sine wave driving the motors from 0 to 100% throttle
 */
void sine_example(){

	int* btns = (int *) XPAR_BTNS_BASEADDR;
	/*        Sine Wave        */
	static double time = 0;

	time += .0001;
	pulseW = (int)fabs(sin(time)*(100000)) + 100000;
	//pulseW = (int) (sin(time) + 1)*50000 + 100000;
	if (*btns & 0x1){
		printf("%d", pulseW);
		printf("   %d\n", *(int*) (PWM_0_ADDR + PWM_PULSE));
	}
	if(!(*btns & 0x2))
		pwm_write_all(pulseW);
	usleep(300);
}

void print_mixer(int * mixer){
	int i;
	for(i = 0; i < 4; i++){
		xil_printf("%d : %d			", i, mixer[i]);
	}
	xil_printf("\n");
}

/**
 * Argument is the reading from the pwm_recorder4 which is connected to the gear pwm
 * If the message from the receiver is 0 - gear, kill the system by sending a 1
 * Otherwise, do nothing
 */
int read_kill(int gear){
	if(gear > 115000 && gear < 125000)
		return 1;
	return 0;
}

int read_flap(int flap)
{
	// flap '0' is 108,000 CC (Up)
	// flap '1' is 192,000 CC (Down)
	// here we say if the reading is greater than 150,000 than its '1'; '0' otherwise
	if(flap > 150000)
		return 1;
	return 0;
}

/**
 * Turns off the motors
 */
void pwm_kill(){
	// Initializes the PWM pulse lengths to be 1 ms
	*(int*) (PWM_0_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_1_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_2_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_3_ADDR + PWM_PULSE) = pulse_throttle_low;
	xil_printf("Kill switch was touched, shutting off the motors and ending the program\n");
}


/**
 * Fills up an xbox hueg amount of memory with log data
 */
void updateLog(){
	// If the first iteration, allocate enough memory for "arraySize" elements of logging
	if(logArray == NULL){
		logArray = malloc(LOG_STARTING_SIZE * sizeof(logging)); // size in memory is 1,736,704 bytes (1.656 megabytes) because logging struct is 424 bytes each
	}

	// for now the log is not dynamic
	// log is big enough that it should last for 17 min
	if(arrayIndex >= arraySize){
		return;
	}

	// Add log to the array
	logArray[arrayIndex++] = logs;

	// If the index is too big, reallocate memory to double the size as before
	if(arrayIndex == arraySize){
		arraySize *= 2;
		logArray = (logging *) realloc(logArray, arraySize * sizeof(logging));
		++resizeCount;
	}
	else if(arrayIndex > arraySize){
		// Something fishy has occured
		xil_printf("Array index is out of bounds. This shouldn't happen but somehow you did the impossible\n\r");
	}
}


/**
 * Prints all the log information.
 *
 * TODO: This should probably be transmitting in binary instead of ascii
 */

void printLogging(){
	int i;
	char buf[1<<17] = {};
	char comments[1 << 8];
	char header[1<<16];
	char units [1<<16];


//	sprintf(comments, "# MicroCART On-board Quad Log\r\n# Sample size: %d\r\n", arrayIndex);
//	sprintf(header, "%%Time\t" "LoopPeriod\t"
//
//					"PacketID\t"
//
//					"P\tI\tD\t"
//					"Error\t"
//					"Change_In_Error\t"
//					"Correction\t"
//
//					"GryoX_p\t" "GyroY_q\t" "GyroZ_r\t"
//					"Accel_roll\t" "Accel_pitch\t"
//
//					"Phi_dot\t" "Theta_dot\t" "Psi_dot\t"
//					"CompRoll_Angle\t" "CompPitch_Angle\t"
//
//					"XPosition\t" "YPosition\t" "ZPosition\t"
//
//					"Cam_Meas_Roll\tCam_Meas_Pitch\tCam_Meas_Yaw\n"
////
////					"angle_Roll_setpoint\t"
////					"angle_Pitch_setpoint\t"
////					"angle_Yaw_setpoint\t"
////
////					"angular_ROLL_setpoint\t"
////					"angular_PITCH_setpoint\t"
////					"angular_Yaw_setpoint\t"
////
////					"angle_roll_perror\t"
////					"angle_pitch_perror\t"
////					"angle_yaw_perror\t"
////
////					"angular_roll_perror\t"
////					"angular_pitch_perror\t"
////					"angular_yaw_perror\t"
////
////					"rc_cmd_pitch\t""rc_cmd_roll\t" "rc_cmd_yaw\t""rc_cmd_throttle\n"
//
//					);
//
//
//	sprintf(units,	"&sec\tsec\t"
//
//					"none\t"
//
//					"none\tnone\tnone\t"
//					"meters\t"
//					"meters\t"
//					"degrees\t"
//
//					"degrees/sec\tdegrees/sec\tdegrees/sec\t"
//					"none\tnone\t"
//
//					"degrees/sec\tdegrees/sec\tdegrees/sec\t"
//					"degrees\tdegrees\t"
//
//					"meters\tmeters\tmeters\t"
//
//					"degrees\tdegrees\tdegrees\n"
//
////					"degrees\t"
////					"degrees\t"
////					"degrees\t"
////
////					"degrees\t"
////					"degrees\t"
////					"degrees\t"
////
////					"degrees\t"
////					"degrees\t"
////					"degrees\t"
////
////					"degrees\t"
////					"degrees\t"
////					"degrees\t"
////
////					"none\t"
////					"none\t"
////					"none\t"
////					"none\n"
//					);
	sprintf(comments, "# MicroCART On-board Quad Log\r\n# Sample size: %d\r\n", arrayIndex);
	sprintf(header, "%%Time\t" "LoopPeriod\t"

					"kP_roll\tkI_roll\tkD_roll\t"
					"kP_pitch\tkI_pitch\tkD_pitch\t"

					"P_term_roll\t"
					"P_term_pitch\t"

					"I_term_roll\t"
					"I_term_pitch\t"

					"D_term_roll\t"
					"D_term_pitch\t"

					"roll_error\t"
					"pitch_error\t"

					"PID_Correction_roll\t"
					"PID_Correction_pitch\t"

					"Setpoint_roll\t"
					"Setpoint_pitch\t"

					"Comp_Roll\tComp_Pitch\t"
					"Cam_Meas_Roll\tCam_Meas_Pitch\t"

					"Roll_Velocty_setpoint\tPitch_Velocty_setpoint\n"

					);


	sprintf(units,	"&sec\tsec\t"

					"none\tnone\tnone\t"
					"none\tnone\tnone\t"

					"radians\/sec\tradians\/sec\t"
					"radians\/sec\tradians\/sec\t"
					"radians\/sec\tradians\/sec\t"

					"radians\tradians\t"

					"radians\/sec\tradians\/sec\t"

					"radians\tradians\t"

					"radians\tradians\t"
					"radians\tradians\t"

					"radians\/sec\tradians\/sec\n"

					);

	strcat(buf,comments);
	strcat(buf,header);
	strcat(buf,units);

	uart0_sendBytes(buf, strlen(buf));
	usleep(1000000);

	/*************************/
	/* print & send log data */
	for(i = 0; i < arrayIndex; i++){
			char* logLine = format(logArray[i]);
			uart0_sendBytes(logLine, strlen(logLine));
			usleep(10000);
			//break;
	}
}


char* format(logging log){
	static char retString[4096];
//	int i, buf_count = 0;


//	Xuint8 buf_size = sizeof(log);

//	Xuint8* buffer = &log;

	/*
	retString[0] = 'L';
	for(i=1; i < buf_size; ++i){
		retString[i] = 'f';
		retString[i+1] = buffer[buf_count++];
		retString[i+2] = buffer[buf_count++];
		retString[i+3] = buffer[buf_count++];
		retString[i+3] = buffer[buf_count++];
	}
	retString[1] = 'f'
	retString[0] = 'L';
	retString[1] = buf_size;
	memcpy(&retString[2], &log, buf_size);


	uart0_sendBytes(retString, buf_size + 2);
*/
//		sprintf(retString, 	"%.3f\t%.4f\t" //Time and TimeSlice
//
//							"%d\t"	// Packet ID
//
//							"%.3f\t%.2f\t%.3f\t" // P, I, D
//							"%.3f\t" // error
//							"%.3f\t" // change in error
//							"%.3f\t" // generic setpoint (currently pitch angle setpoint)
//
//							"%.3f\t%.3f\t%.3f\t" // Gyro velocities
//							"%.3f\t%.3f\t" // Accel angles
////
//							"%.3f\t%.3f\t%.3f\t" // gimbal angular velocities
//							"%.3f\t%.3f\t" // Complimentary roll and pitch angles
////
//							"%.3f\t%.3f\t%.3f\t" // x position (east), y position (north), z position (altitude)
////
//							"%.3f\t%.3f\t%.3f\n", // camera measured roll, cam pitch, cam yaw
////
////							"%.3f\t"  // angle Roll PID setpoint
////							"%.3f\t"  // angle Pitch PID setpoint
////							"%.3f\t"  // angle Yaw PID setpoint
////
////							"%.3f\t"  // angle Roll PID setpoint
////							"%.3f\t"  // angle Pitch PID setpoint
////							"%.3f\t"  // angle Yaw PID setpoint
////
////							"%.3f\t"  // angle Roll PID setpoint
////							"%.3f\t"  // angle Pitch PID setpoint
////							"%.3f\t"  // angle Yaw PID setpoint
////
////							"%.3f\t"  // angle Roll PID setpoint
////							"%.3f\t"  // angle Pitch PID setpoint
////							"%.3f\t"  // angle Yaw PID setpoint
////
////							"%.3f\t"  // angle Roll PID setpoint
////							"%.3f\t"  // angle Pitch PID setpoint
////							"%.3f\t"  // angle Yaw PID setpoint
////							"%.3f\n",  // angle Yaw PID setpoint
//
//							log.time_stamp, log.time_slice,
//
//							log.packetId,
//
//							log.local_x_PID_values.P, log.local_x_PID_values.I, log.local_x_PID_values.D,
//							log.local_x_PID_values.error,
//							log.local_x_PID_values.change_in_error,
//							log.local_x_PID_values.P + log.local_x_PID_values.I + log.local_x_PID_values.D,
//
//							log.gam.gyro_xVel_p, log.gam.gyro_yVel_q, log.gam.gyro_zVel_r,
//							log.gam.accel_roll, log.gam.accel_pitch,
////
//							log.phi_dot, log.theta_dot, log.psi_dot,
//							log.roll_angle, log.pitch_angle,
//
//							log.x_pos, log.y_pos, log.altitude_z,
////
//							log.cam_roll, log.cam_pitch, log.cam_yaw
////
////							log.angle_roll_PID.setpoint,
////							log.angle_pitch_PID.setpoint,
////							log.angle_yaw_PID.setpoint,
////
////							log.angle_roll_PID.setpoint,
////							log.angle_pitch_PID.setpoint,
////							log.angle_yaw_PID.setpoint,
////
////							log.angle_roll_PID.setpoint,
////							log.angle_pitch_PID.setpoint,
////							log.angle_yaw_PID.setpoint,
////
////							log.angle_roll_PID.setpoint,
////							log.angle_pitch_PID.setpoint,
////							log.angle_yaw_PID.setpoint,
////
////							log.angle_roll_PID.setpoint,
////							log.angle_pitch_PID.setpoint,
////							log.angle_yaw_PID.setpoint,
////							log.angle_yaw_PID.setpoint
//		);

			sprintf(retString, 	"%.3f\t%.4f\t" 	//Time and TimeSlice
					"%.3f\t%.3f\t%.3f\t" //"kP_roll\tkI_roll\tkD_roll\t"
					"%.3f\t%.3f\t%.3f\t" //"kP_pitch\tkI_pitch\tkD_pitch\t"

					"%.3f\t%.3f\t"//"P_term_roll\t" "P_term_pitch\t"

					"%.3f\t%.3f\t"//"I_term_roll\t" "I_term_pitch\t"

					"%.3f\t%.3f\t"//"D_term_roll\t" "D_term_pitch\t"

					"%.3f\t%.3f\t"//"roll_error\t" "pitch_error\t"

					"%.3f\t"//"PID_Correction_roll\t"
					"%.3f\t"//"PID_Correction_pitch\t"

					"%.3f\t"//"Setpoint_roll\t"
					"%.3f\t"//"Setpoint_pitch\t"

					"%.3f\t%.3f\t"//"Comp_Roll\tComp_Pitch\t"
					"%.3f\t%.3f\t"//"Cam_Meas_Roll\tCam_Meas_Pitch\t"

					"%.3f\t%.3f\n"//"Roll_Velocty_setpoint\tPitch_Velocty_setpoint\n"

								,log.time_stamp, log.time_slice,
								log.angle_roll_PID.Kp, log.angle_roll_PID.Ki, log.angle_roll_PID.Kd,
								log.angle_pitch_PID.Kp, log.angle_pitch_PID.Ki, log.angle_pitch_PID.Kd,
								log.angle_roll_PID_values.P, log.angle_pitch_PID_values.P,
								log.angle_roll_PID_values.I, log.angle_pitch_PID_values.I,
								log.angle_roll_PID_values.D, log.angle_pitch_PID_values.D,
								log.angle_roll_PID_values.error, log.angle_pitch_PID_values.error,
								log.angle_roll_PID_values.pid_correction, log.angle_pitch_PID_values.pid_correction,
								log.angle_roll_PID.setpoint, log.angle_pitch_PID.setpoint,
								log.roll_angle, log.pitch_angle,
								log.cam_roll, log.cam_pitch,
								log.ang_vel_roll_PID.setpoint, log.ang_vel_pitch_PID.setpoint
			);


		return retString;
}


/**
 * Useful stuff in here in regards to PID tuning, and motor tuning
 * TODO : Scanf string stuff
 * TODO : Make adam do it :DDDDDDDD
 */
void bluetoothTunePID(char instr, gam_t* gam, PID_t* CFpid, PID_t* Gpid){
	int wasX = 0;
	int wasPid = 0;
	int wasSetpoint = 0;
	int wasLog = 0;
	char buf[100];

	switch (instr) {
	case 'P':
		// Change this if tuning other PIDs
		CFpid->Kp += .5;
		wasPid = 1;
		break;
	case 'p':
		CFpid->Kp -= .5;
		wasPid = 1;
		break;
	case 'O':
		CFpid->Kp += .2;
		wasPid = 1;
		break;
	case 'o':
		CFpid->Kp -= .2;
		wasPid = 1;
		break;

	case 'I':
		CFpid->Kp += 0.1;
		wasPid = 1;
		break;
	case 'i':
		CFpid->Kp -= 0.1;
		wasPid = 1;
		break;
	case 'W':
		Gpid->Kp += 1;
		wasPid = 1;
		break;
	case 'w':
		Gpid->Kp -= 1;
		wasPid = 1;
		break;
	case 'E':
		Gpid->Kp += 2;
		wasPid = 1;
		break;
	case 'e':
		Gpid->Kp -= 2;
		wasPid = 1;
		break;
	case 'R':
		Gpid->Kp += 5;
		wasPid = 1;
		break;
	case 'r':
		Gpid->Kp -= 5;
		wasPid = 1;
		break;
	case 'D':
		CFpid->Kd += .1;
		wasPid = 1;
		break;
	case 'd':
			CFpid->Kd -= .1;
			wasPid = 1;
			break;
	case 'S':
			CFpid->setpoint += 1;
			wasSetpoint = 1;
			break;
	case 's':
			CFpid->setpoint -= 1;
			wasSetpoint = 1;
			break;
	case '0':
		motor0_bias += 100;
		wasPid = 0;
		break;
	case '1':
		motor1_bias += 100;
		wasPid = 0;
		break;
	case '2':
		motor2_bias += 100;
		wasPid = 0;
		break;
	case '3':
		motor3_bias += 100;
		wasPid = 0;
		break;
	case ')':
		motor0_bias -= 100;
		wasPid = 0;
		break;
	case '!':
		motor1_bias -= 100;
		wasPid = 0;
		break;
	case '@':
		motor2_bias -= 100;
		wasPid = 0;
		break;
	case '#':
		motor3_bias -= 100;
		wasPid = 0;
		break;
	case 'x':
		wasX = 1;
		break;
	case ' ':
		wasLog = 1;
		break;
/*	case 'm':
		pid->setpoint = -45.0;
		// Change set point
		break;
	case 'n':
		pid->setpoint = 45.0;
	*/	// Change set point
	default:
		wasPid = 0;
		break;
	}

	if(wasX){
		return;
	}
	else if(wasSetpoint){
		sprintf(buf, "Setpoint: %4.1f\n\r", CFpid->setpoint);
		uart0_sendBytes(buf, strlen(buf));
		usleep(5000);
	}
	else if (wasPid) {
		/*sprintf(buf,
				"PAngle: %8.3f RAngle: %8.3f PID p: %8.3f  d: %8.3f\r\n",
				compY, compX, pitchPID.Kp, pitchPID.Kd);*/

		sprintf(buf, "CFP Coeff: %4.1f D %4.1f GP Coeff: %4.1f\n\r", CFpid->Kp, CFpid->Kd, Gpid->Kp);
		uart0_sendBytes(buf, strlen(buf));
		usleep(5000);
	}
	else if (wasLog){
		sprintf(buf, "CX %5.2f GP GX: %5.2f\n\r", 0.0, gam->gyro_xVel_p);
		uart0_sendBytes(buf, strlen(buf));
		usleep(5000);
	}
	else {
		sprintf(buf, "Motor bias's \t\t0: %d 1: %d 2: %d 3: %d \r\n", motor0_bias,
				motor1_bias, motor2_bias, motor3_bias);
		uart0_sendBytes(buf, strlen(buf));

		sprintf(buf, "P: %3.2f I: %3.2f D: %3.2f\r\n", logs.ang_vel_pitch_PID_values.P, logs.ang_vel_pitch_PID_values.I, logs.ang_vel_pitch_PID_values.D);
		uart0_sendBytes(buf, strlen(buf));
		usleep(1e4);
	}
}

void msleep(int msecs)
{
	usleep(msecs * 1000);
}
