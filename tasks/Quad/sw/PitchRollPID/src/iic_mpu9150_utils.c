/**
 * IIC_MPU9150_UTILS.c
 *
 * Utility functions for using I2C on a Diligent Zybo board and
 * focused on the SparkFun MPU9150
 *
 * For function descriptions please see iic_mpu9150_utils.h
 *
 * Author: 	Paul Gerver (pfgerver@gmail.com)
 * Created: 01/20/2015
 */

#include <stdio.h>
#include <sleep.h>

#include "xparameters.h"
#include "iic_mpu9150_utils.h"
#include "xbasic_types.h"
#include "xiicps.h"
#include "math.h"
#include "gam.h"

XIicPs_Config* i2c_config;
XIicPs I2C0;
double magX_correction = -1, magY_correction, magZ_correction;

int initI2C0(){

	//Make sure CPU_1x clk is enabled for I2C controller
	Xuint16* aper_ctrl = (Xuint16*) IO_CLK_CONTROL_REG_ADDR;

	if(*aper_ctrl & 0x00040000){
		xil_printf("CPU_1x is set to I2C0\r\n");
	}

	else{
		xil_printf("CPU_1x is not set to I2C0..Setting now\r\n");
		*aper_ctrl |= 0x00040000;
	}


	// Look up
	i2c_config = XIicPs_LookupConfig(XPAR_PS7_I2C_0_DEVICE_ID);

	XStatus status = XIicPs_CfgInitialize(&I2C0, i2c_config, i2c_config->BaseAddress);

	// Check if initialization was successful
	if(status != XST_SUCCESS){
		printf("ERROR (initI2C0): Initializing I2C0\r\n");
		return -1;
	}

	// Reset the controller and set the clock to 400kHz
	XIicPs_Reset(&I2C0);
	XIicPs_SetSClk(&I2C0, 400000);


	return 0;
}

int startMPU9150(){

	// Device Reset & Wake up
	iic0Write(0x6B, 0x80);
	usleep(5000);

	// Set clock reference to Z Gyro
	iic0Write(0x6B, 0x03);
	// Configure Digital Low/High Pass filter
	iic0Write(0x1A,0x06); // Level 4 low pass on gyroscope

	// Configure Gyro to 2000dps, Accel. to +/-8G
	iic0Write(0x1B, 0x18);
	iic0Write(0x1C, 0x10);

	// Enable I2C bypass for AUX I2C (Magnetometer)
	iic0Write(0x37, 0x02);

	// Setup Mag
	iic0Write(0x37, 0x02);             //INT_PIN_CFG   -- INT_LEVEL=0 ; INT_OPEN=0 ; LATCH_INT_EN=0 ; INT_RD_CLEAR=0 ; FSYNC_INT_LEVEL=0 ; FSYNC_INT_EN=0 ; I2C_BYPASS_EN=0 ; CLKOUT_EN=0

	usleep(100000);

	int i;
	gam_t temp_gam;

	// Do about 20 reads to warm up the device
	for(i=0; i < 20; ++i){
		if(get_gam_reading(&temp_gam) == -1){
			printf("ERROR (startMPU9150): error occured while getting GAM data\r\n");
			return -1;
		}
		usleep(1000);
	}

	return 0;
}

void stopMPU9150(){

	//Put MPU to sleep
	iic0Write(0x6B, 0b01000000);
}

void iic0Write(u8 register_addr, u8 data){

	u16 device_addr = MPU9150_DEVICE_ADDR;
	u8 buf[] = {register_addr, data};

	// Check if within register range
	if(register_addr < 0 || register_addr > 0x75){
		printf("ERROR (iic0Write) : Cannot write to register address, 0x%x: out of bounds\r\n", register_addr);
		return;
	}

	if(register_addr <= 0x12){
		device_addr = MPU9150_COMPASS_ADDR;
	}

	XIicPs_MasterSendPolled(&I2C0, buf, 2, device_addr);

}

void iic0Read(u8* recv_buffer, u8 register_addr, int size){

	u16 device_addr = MPU9150_DEVICE_ADDR;
	u8 buf[] = {register_addr};

	// Check if within register range
	if(register_addr < 0 || register_addr > 0x75){
		printf("ERROR (iic0Read): Cannot read register address, 0x%x: out of bounds\r\n", register_addr);
	}

	// Set device address to the if 0x00 <= register address <= 0x12
	if(register_addr <= 0x12){
		device_addr = MPU9150_COMPASS_ADDR;
	}


	XIicPs_MasterSendPolled(&I2C0, buf, 1, device_addr);
	XIicPs_MasterRecvPolled(&I2C0, recv_buffer,size,device_addr);
}

void CalcMagSensitivity(){

	u8 buf[3];
	u8 ASAX, ASAY, ASAZ;

	// Quickly read from the factory ROM to get correction coefficents
	iic0Write(0x0A, 0x0F);
	usleep(10000);

	// Read raw adjustment values
	iic0Read(buf, 0x10,3);
	ASAX = buf[0];
	ASAY = buf[1];
	ASAZ = buf[2];

	// Set the correction coefficients
	magX_correction = (ASAX-128)*0.5/128 + 1;
	magY_correction = (ASAY-128)*0.5/128 + 1;
	magZ_correction = (ASAZ-128)*0.5/128 + 1;
}


void ReadMag(gam_t* gam){

	u8 mag_data[6];
	Xint16 raw_magX, raw_magY, raw_magZ;

	// Grab calibrations if not done already
	if(magX_correction == -1){
		CalcMagSensitivity();
	}

	// Set Mag to single read mode
	iic0Write(0x0A, 0x01);
	usleep(10000);
	mag_data[0] = 0;

	// Keep checking if data is ready before reading new mag data
	while(mag_data[0] == 0x00){
		iic0Read(mag_data, 0x02, 1);
	}

	// Get mag data
	iic0Read(mag_data, 0x03, 6);

	raw_magX = (mag_data[1] << 8) | mag_data[0];
	raw_magY = (mag_data[3] << 8) | mag_data[2];
	raw_magZ = (mag_data[5] << 8) | mag_data[4];

	// Set magnetometer data to output
	gam->mag_x = raw_magX * magX_correction;
	gam->mag_y = raw_magY * magY_correction;
	gam->mag_z = raw_magZ * magZ_correction;

}

/**
 * Get Gyro Accel Mag (GAM) information
 */
int get_gam_reading(gam_t* gam) {

	Xint16 raw_accel_x, raw_accel_y, raw_accel_z;
	Xint16 gyro_x, gyro_y, gyro_z;

	Xuint8 sensor_data[ACCEL_GYRO_READ_SIZE] = {};


	// We should only get mag_data ~10Hz
	//Xint8 mag_data[6] = {};

	//readHandler = iic0_read_bytes(sensor_data, ACCEL_GYRO_BASE_ADDR, ACCEL_GYRO_READ_SIZE);
	iic0Read(sensor_data, ACCEL_GYRO_BASE_ADDR, ACCEL_GYRO_READ_SIZE);

	//Calculate accelerometer data
	raw_accel_x = sensor_data[ACC_X_H] << 8 | sensor_data[ACC_X_L];
	raw_accel_y = sensor_data[ACC_Y_H] << 8 | sensor_data[ACC_Y_L];
	raw_accel_z = sensor_data[ACC_Z_H] << 8 | sensor_data[ACC_Z_L];

	// put in G's
	gam->accel_x = (raw_accel_x / 4096.0) + ACCEL_X_BIAS; // 4,096 is the gain per LSB of the measurement reading based on a configuration range of +-8g
	gam->accel_y = (raw_accel_y / 4096.0) + ACCEL_Y_BIAS;
	gam->accel_z = (raw_accel_z / 4096.0) + ACCEL_Z_BIAS;

	//Get X and Y angles
	// javey: this assigns accel_(pitch/roll) in units of radians
	gam->accel_pitch = atan(gam->accel_x / sqrt(gam->accel_y*gam->accel_y + gam->accel_z*gam->accel_z));
	gam->accel_roll = -atan(gam->accel_y / sqrt(gam->accel_x*gam->accel_x + gam->accel_z*gam->accel_z)); // negative because sensor board is upside down

	//Convert gyro data to rate (we're only using the most 12 significant bits)
	gyro_x = (sensor_data[GYR_X_H] << 8) | (sensor_data[GYR_X_L]); //* G_GAIN;
	gyro_y = (sensor_data[GYR_Y_H] << 8 | sensor_data[GYR_Y_L]);// * G_GAIN;
	gyro_z = (sensor_data[GYR_Z_H] << 8 | sensor_data[GYR_Z_L]);// * G_GAIN;

	//Get the number of degrees
	//javey: converted to radians to following SI units
	gam->gyro_xVel_p = ((gyro_x / GYRO_SENS) * DEG_TO_RAD) + GYRO_X_BIAS;
	gam->gyro_yVel_q = ((gyro_y / GYRO_SENS) * DEG_TO_RAD) + GYRO_Y_BIAS;
	gam->gyro_zVel_r = ((gyro_z / GYRO_SENS) * DEG_TO_RAD) + GYRO_Z_BIAS;

	return 0;

}
