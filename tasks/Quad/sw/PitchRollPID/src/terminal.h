/*
 * terminal.h
 *
 *  Created on: Sep 24, 2014
 *      Author: Adam
 */

#ifndef _TERMINAL_H_
#define _TERMINAL_H_

#include "stringBuilder.h"
#define UPDATE_SIZE 28

#define NUM_TERMINAL_LINES_TO_CLEAR 8

// ascii 002: start of text
#define PACKET_START_CHAR 2

// ascii 003: end of text
#define PACKET_END_CHAR 3

void receivePacket(stringBuilder_t* sb);

int tryReceivePacket(stringBuilder_t* sb, int localEcho);

void clearTerminal();

int runTerminal(int (*)(char*));

#endif /* _TERMINAL_H_ */
