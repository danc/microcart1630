/*
 * zybo_terminal.c: simple terminal application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "stringBuilder.h"
#include "terminal.h"
#include "uart.h"
#include "sleep.h"


#define DEBUG 0

//#define UPDATE_SIZE 28
int alive = 1;

int readingPacket = 0;

stringBuilder_t* sbPrev;


void clearTerminal() {
	int i;
	for (i = 0; i < NUM_TERMINAL_LINES_TO_CLEAR; i++) {
		printf("\r\n");
	}
}

int runTerminal(int (*processCommand)(char*)) {
	// Turn off buffering
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);

	clearTerminal();

	printf("===== ZYBOTERM (TM) =====\r\n");
	printf("\r\nWelcome!\r\n");

	sbPrev = stringBuilder_create();
	stringBuilder_t* sb = stringBuilder_create();
	{
		do {
			receivePacket(sb);
			if (alive) {
				int ret = processCommand(sb->buf);
				if (ret) {
					alive = 0;
				}

				sb->clear(sb);
			}
		} while (alive);
	}
	stringBuilder_free(sb);
	stringBuilder_free(sbPrev);

	return 0;
}

int tryReceivePacket(stringBuilder_t* sb, int echo) {

	while (uart0_hasData()) {
		char c = uart0_recvByte();
		if (c == PACKET_START_CHAR) {
#if DEBUG
			uart0_sendStr("Start ");
#endif
			c = uart0_recvByte();
			char type = c;
			sb->addChar(sb, type);
			int count = 0;

			// if it's a 'C' packet (a command for the quad), then
			// wait for the packet end char. Else, if it's a 'U' packet
			// which is an update from the camera system, then we read
			// a fixed number of bytes (UPDATE_SIZE # of bytes)
			if(type == 'C')
			{
				while (c != PACKET_END_CHAR)
				{
					c = uart0_recvByte();
					count++;
					if(c == PACKET_END_CHAR)
						break;
					if ((c != 0 && c != '\r' && c != '\n'))
					{
						sb->addChar(sb, c);
						if (echo) {
							uart0_sendByte(c);
						}
					}
				}
			}

			if(type == 'U')
			{
				while(count < UPDATE_SIZE)
				{
					c = uart0_recvByte();
					count++;

					sb->addChar(sb, c);
					if (echo) {
						uart0_sendByte(c);
					}
				}
			}

#if DEBUG
			uart0_sendStr("End:[");
			uart0_sendStr(sb->buf);
			uart0_sendStr("] ");
#endif
			return 1;
		}
	}

	return 0;
}

void receivePacket(stringBuilder_t* sb) {
	while (1) {
		char c = uart0_recvByte();

		// hacky way of detecting up arrow ("^[A"), will cause problems with ESC
		if (c == 27) {
			c = getc(stdin);
			c = getc(stdin);
			c = getc(stdin);
			c = getc(stdin);

			if (c == 65) {
				c = getc(stdin);

				uart0_sendStr(sbPrev->buf);
				sb->clear(sb);
				sb->addStr(sb, sbPrev->buf);
			}
		} else {
			uart0_sendByte(c);
		}
#if DEBUG
		printf("CHAR READ: %d, %d\r\n", numRead, c);
#endif

		// Backspace
		if (c == '\b' || c == 127) {
			if (sb->length > 0) {
				sb->removeCharAt(sb, sb->length - 1);
			}
		}

		// Stop reading at newline
		else if (c == '\n' || c == '\r') {
#if DEBUG
			char buf[500];
			sprintf(buf, "\r\n%s read\r\n", sb->buf);
			uart0_sendStr(buf);
#endif
			// Save it if string not empty
			if (sb->buf[0]) {
				sbPrev->clear(sbPrev);
				sbPrev->addStr(sbPrev, sb->buf);
			}
			return;
		}

		// Else add as normal
		else if (c) {
			sb->addChar(sb, c);
		}
	}
}
