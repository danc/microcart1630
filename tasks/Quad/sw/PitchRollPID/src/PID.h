/*
 * PID.h
 *
 *  Created on: Nov 10, 2014
 *      Authors: imciner2, javey
 */

#ifndef PID_H_
#define PID_H_

// Yaw constants

// when using units of degrees
//#define YAW_ANGULAR_VELOCITY_KP 40.0f
//#define YAW_ANGULAR_VELOCITY_KI 0.0f
//#define YAW_ANGULAR_VELOCITY_KD 0.0f
//#define YAW_ANGLE_KP 2.6f
//#define YAW_ANGLE_KI 0.0f
//#define YAW_ANGLE_KD 0.0f

// when using units of radians
#define YAW_ANGULAR_VELOCITY_KP 2292.0f
#define YAW_ANGULAR_VELOCITY_KI 0.0f
#define YAW_ANGULAR_VELOCITY_KD 0.0f
#define YAW_ANGLE_KP 2.6f
#define YAW_ANGLE_KI 0.0f
#define YAW_ANGLE_KD 0.0f

// Roll constants
//#define ROLL_ANGULAR_VELOCITY_KP 0.95f
//#define ROLL_ANGULAR_VELOCITY_KI 0.0f
//#define ROLL_ANGULAR_VELOCITY_KD 0.13f//0.4f//0.7f
//#define ROLL_ANGLE_KP 17.0f //9.0f
//#define ROLL_ANGLE_KI 0.0f
//#define ROLL_ANGLE_KD 0.3f // 0.2f
//#define YPOS_KP 0.0f
//#define YPOS_KI 0.0f
//#define YPOS_KD 0.0f

// when using units of radians
#define ROLL_ANGULAR_VELOCITY_KP 46.0f//35.0f
#define ROLL_ANGULAR_VELOCITY_KI 0.0f
#define ROLL_ANGULAR_VELOCITY_KD 6.5f //5.0f//3.25
#define ROLL_ANGLE_KP 15.0//23.0f
#define ROLL_ANGLE_KI 0.0f
#define ROLL_ANGLE_KD 0.3f//1.0f//2.5f
#define YPOS_KP 0.015f //0.08f
#define YPOS_KI 0.00f//0.00f
#define YPOS_KD 0.03f // 0.0f


//Pitch constants

// when using units of degrees
//#define PITCH_ANGULAR_VELOCITY_KP 0.95f
//#define PITCH_ANGULAR_VELOCITY_KI 0.0f
//#define PITCH_ANGULAR_VELOCITY_KD 0.13f//0.35f//0.7f
//#define PITCH_ANGLE_KP 17.0f // 7.2f
//#define PITCH_ANGLE_KI 0.0f
//#define PITCH_ANGLE_KD 0.3f //0.3f
//#define XPOS_KP 40.0f
//#define XPOS_KI 0.0f
//#define XPOS_KD 10.0f//0.015f

// when using units of radians
#define PITCH_ANGULAR_VELOCITY_KP 46.0f //35.0f
#define PITCH_ANGULAR_VELOCITY_KI 0.0f
#define PITCH_ANGULAR_VELOCITY_KD 6.5f //5.0f//3.25f
#define PITCH_ANGLE_KP 15.0f//23.0f
#define PITCH_ANGLE_KI 0.0f
#define PITCH_ANGLE_KD 0.3f//1.0f
#define XPOS_KP 0.015f //0.08f
#define XPOS_KI 0.00f//0.00f
#define XPOS_KD 0.03f //0.0f


//Throttle constants
#define ALT_ZPOS_KP -9804.0f
#define ALT_ZPOS_KI -817.0f
#define ALT_ZPOS_KD -7353.0f

typedef struct PID_t {
	double current_point;	// Current value of the system
	double setpoint;		// Desired value of the system
	float Kp;				// Proportional constant
	float Ki;				// Integral constant
	float Kd;				// Derivative constant
	double prev_error;		// Previous error
	double acc_error;		// Accumulated error
	double pid_correction;	// Correction factor computed by the PID
	float dt; 				// sample period
} PID_t;

typedef struct PID_values{
	float P;	// The P component contribution to the correction output
	float I;	// The I component contribution to the correction output
	float D;	// The D component contribution to the correction output
	float error; // the error of this PID calculation
	float change_in_error; // error change from the previous calc. to this one
	float pid_correction; // the correction output (P + I + D)
} PID_values;

// Computes control error and correction
PID_values pid_computation(PID_t *pid);

#endif /* PID_H_ */
