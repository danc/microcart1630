/*
 * stringBuffer.h
 *
 *  Created on: Sep 24, 2014
 *      Author: Adam
 */

#ifndef STRINGBUILDER_H_
#define STRINGBUILDER_H_

#define STRINGBUILDER_DEFAULT_INITIAL_CAPACITY 16
#define STRINGBUILDER_DEFAULT_MAX_CAPACITY 1024

// String builder data type
typedef struct stringBuilder_s {
	char* buf;
	int length;
	int capacity;
	int maxCapacity;

	// Methods
	int (*addStr)(struct stringBuilder_s*, char*);
	int (*addStrAt)(struct stringBuilder_s*, char*, int);
	int (*addChar)(struct stringBuilder_s*, char);
	int (*addCharAt)(struct stringBuilder_s*, char, int);
	int (*removeCharAt)(struct stringBuilder_s*, int);
	void (*clear)(struct stringBuilder_s*);
} stringBuilder_t;

enum {
	STRINGBUILDER_SUCCESS = 0,
	STRINGBUILDER_AT_MAX_CAPACITY = 1,
	STRINGBUILDER_NO_MEM_FOR_EXPANSION = 2,
	STRINGBUILDER_ILLEGALARGUMENT = 3
};

/***** Constructors *****/

/** Create a StringBuilder with a max/initial capacity specified */
stringBuilder_t* stringBuilder_createWithMaxCapacity(int, int);

/** Create a StringBuilder with an initial capacity specified */
stringBuilder_t* stringBuilder_createWithInitialCapacity(int);

/** Create a StringBuilder with default initial capacity/max capacity */
stringBuilder_t* stringBuilder_create();


/** Free a StringBuilder */
void stringBuilder_free(stringBuilder_t*);


/***** Methods *****/

/** Add a string to the end of the StringBuilder */
int stringBuilder_addStr(stringBuilder_t*, char*);

/** Add a string to the StringBuilder at the specified index */
int stringBuilder_addStrAt(stringBuilder_t*, char*, int);

/** Add a character to end of the StringBuilder */
int stringBuilder_addChar(stringBuilder_t*, char);

/** Add a character to the StringBuilder at the specified index */
int stringBuilder_addCharAt(stringBuilder_t*, char, int);

/** Remove a character from the StringBuilder at the specified index */
int stringBuilder_removeCharAt(stringBuilder_t*, int);

/** Clear a stringBuilder */
void stringBuilder_clear(stringBuilder_t*);

#endif /* STRINGBUILDER_H_ */
