/*
 * controllers.h
 *
 *  Created on: Oct 11, 2014
 *      Author: Tyler
 */
#ifndef _CONTROLLERS_H
#define _CONTROLLERS_H

#include "util.h"
#include "gam.h"
#include "quadposition.h"

/**
 *
 *  USING PLUS CONFIGURATION
 *              0           R            CW               E
 *            1 + 2       W   R      CCW    CCW        N     S
 *              3           W            CW               W
 *
 *
 *  USING X CONFIGURATION
 *
 *
 *         0   2           R   R        CW    CCW
 *           x
 *         1   3           W   W        CCW   CW
 */
#define X_CONFIG

/**
 * Pin hook ups
 *
 * PWM Recorder port mapping
 * 3.3 V || GND || PWM_REC_3 || PWM_REC_2 || PWM_REC_1 || PWM_REC_0
 *
 * Rx PINS
 * GEAR     -> JD7
 * THROTTLE -> JE1
 * AILE     -> JE2
 * ELEV     -> JE3
 * RUDD     -> JE4
 * GND      -> JE5
 *
 * JE PMOD  TOP PINS
 * Unused   || GND  || YAW  || PITCH || ROLL || THROTTLE
 *
 * BOTTOM PINS
 *
 *  Unused  || GND  || PWM3 || PWM2  || PWM1 || PWM0
 */

/**
 * Gear settings
 * 1 - F mode = 171135
 * 0 - Gear   = 118363
 * Kill if gear is around 118363
 */


/*
 * Aero channel declaration
 */

#define THROTTLE 0
#define ROLL     1
#define PITCH    2
#define YAW      3
#define GEAR 	 4
#define FLAP 	 5

/**
 * Signals from the Rx mins, maxes and ranges
 */
#define THROTTLE_MAX  191900
#define THROTTLE_MIN  110200
#define THROTTLE_RANGE THROTTLE_MAX - THROTTLE_MIN

#define ROLL_MAX      170200
//#define ROLL_MAX      167664
//#define ROLL_CENTER   148532
#define ROLL_MIN      129400
#define ROLL_CENTER   149800
#define ROLL_RANGE    ROLL_MAX - ROLL_MIN

//#define PITCH_MAX     190800
#define PITCH_MAX     169900
//#define PITCH_MIN     135760
#define PITCH_MIN     129500
//#define PITCH_CENTER  152830
#define PITCH_CENTER  149700
#define PITCH_RANGE   PITCH_MAX - PITCH_MIN

#define YAW_MAX       169400
#define YAW_MIN       129300
#define YAW_CENTER   149800
#define YAW_RANGE     YAW_MAX - YAW_MIN

#define GEAR_1	170800
#define GEAR_0	118300

#define FLAP_1		192000
#define FLAP_0		107600

#define GEAR_KILL     GEAR_0 // The kill point for the program
#define BASE          150000

#define min 100000
#define max 200000

void filter_PWMs(int* mixer);
void PWMS_to_Aero(int* PWMs, int* aero); // <= javey: unused
void Aero_to_PWMS(int* PWMs, int* aero);

#endif /* _CONTROLLERS_H */
