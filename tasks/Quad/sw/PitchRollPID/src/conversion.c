
#include "conversion.h"
/*
 * Converts an RC receiver command to whatever units that max_target and min_target are in. This function first centers the receiver command at 0.
 * It creates a windowed linear function, based on the sign of the centered receiver command.
 *
 * 										       -
 * 									          / (x - center_receiver_cmd) * (min_target / (min_receiver_cmd - center_receiver_cmd)) when x <=  0
 * convert_receiver_cmd(x = receiver_cmd) =  <
 * 										      \ (x - center_receiver_cmd) * (max_target / (max_receiver_cmd - center_receiver_cmd) when x > 0
 * 										       -
 *
 */
float convert_from_receiver_cmd(int receiver_cmd, int max_receiver_cmd, int center_receiver_cmd, int min_receiver_cmd, float max_target, float min_target)
{
	// centers the receiver command by subtracting the given center value. This means that if receiver_cmd == center then receiver_cmd_centered should be 0.
	int receiver_cmd_centered = receiver_cmd - center_receiver_cmd;

	if(receiver_cmd_centered <= 0) {
		float ret = ((float)(receiver_cmd_centered * min_target)) / ((float) (min_receiver_cmd - center_receiver_cmd));

		if(ret < min_target)
			ret = min_target;

		return ret;
	}

	else {
		float ret = ((float)(receiver_cmd_centered * max_target)) / ((float) (max_receiver_cmd - center_receiver_cmd));

		if(ret > max_target)
			ret = max_target;

		return ret;
	}

	return 0.0;
}

// takes a floating point percentage and converts to a
// receiver command in the range min_rx_cmd to max_rx_cmd
// if percentage is < 0 then returns a value less than
//		center_rx_cmd but >= min_rx_cmd
// if percentage is > 0 then returns a value greater than
//		center_rx_cmd but <= max_rx_cmd
// if percentage is = 0 then returns center_rx_cmd
// acceptable range of values for percentage: [-100, 100]
int map_to_rx_cmd(float percentage, int min_rx_cmd, int center_rx_cmd,
	int max_rx_cmd)
{
	//bounds checking
	// imagine a human flying and the stick is minimum
	if(percentage >= 100.0)
		return max_rx_cmd;

	//bounds checking
	// imagine a human flying and the stick is minimum
	if(percentage <= -100.0)
		return min_rx_cmd;

	// 0 percentage is center cmd
	// imagine a human flying and not touching the stick
	if(percentage == 0)
		return center_rx_cmd;

	// calculate and return a percentage of the max/min command
	if(percentage < 0)
	{
		return center_rx_cmd + ((int) (percentage/100.0 *
				((float) max_rx_cmd - center_rx_cmd)));
	}
	else
	{
		return center_rx_cmd + ((int) (percentage/100.0 * (
				(float) center_rx_cmd - min_rx_cmd)));
	}

	return 0;
}

int convert_to_receiver_cmd(int var_to_convert, float max_var_to_convert, float min_var_to_convert, int center_receiver_cmd,  int max_receiver_cmd, int min_receiver_cmd)
{

	if(var_to_convert <= 0) {
		int ret = ((int) ((float)(min_receiver_cmd - center_receiver_cmd))/min_var_to_convert * var_to_convert) + center_receiver_cmd;

		if(ret < min_receiver_cmd)
			ret = min_receiver_cmd;

		return ret;
	}

	else {
		int ret = ((int) ((float)(max_receiver_cmd - center_receiver_cmd))/max_var_to_convert * var_to_convert) + center_receiver_cmd;

		if(ret > max_receiver_cmd)
			ret = max_receiver_cmd;

		return ret;
	}

	return 0;
}
