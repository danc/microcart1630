/*
 * util.h
 *
 *  Created on: Oct 11, 2014
 *      Author: Tyler
 */
#include "PID.h"
#include "gam.h"
#ifndef _UTIL_H
#define _UTIL_H

#define clock_rate          100000000
#define frequency           450
#define period_width        clock_rate/frequency
#define pulse_throttle_low  clock_rate / 1000
#define pulse_throttle_high clock_rate / 500
#define MOTOR_0_PERCENT     115000


#define XPAR_BTNS_BASEADDR 0x41200000

/**
 * Various Addresses of custom IP components
 */
#define PWM_0_ADDR     XPAR_PWM_SIGNAL_OUT_WKILLSWITCH_0_BASEADDR
#define PWM_1_ADDR     XPAR_PWM_SIGNAL_OUT_WKILLSWITCH_1_BASEADDR
#define PWM_2_ADDR     XPAR_PWM_SIGNAL_OUT_WKILLSWITCH_2_BASEADDR
#define PWM_3_ADDR     XPAR_PWM_SIGNAL_OUT_WKILLSWITCH_3_BASEADDR
#define PWM_REC_0_ADDR XPAR_PWM_RECORDER_0_BASEADDR
#define PWM_REC_1_ADDR XPAR_PWM_RECORDER_1_BASEADDR
#define PWM_REC_2_ADDR XPAR_PWM_RECORDER_2_BASEADDR
#define PWM_REC_3_ADDR XPAR_PWM_RECORDER_3_BASEADDR
#define PWM_REC_4_ADDR XPAR_PWM_RECORDER_4_BASEADDR
#define PWM_REC_5_ADDR XPAR_PWM_RECORDER_5_BASEADDR

/**
 * Register offsets within the custom IP
 */
#define PWM_PERIOD     0
#define PWM_PULSE      4


typedef struct commands{
	int pitch, roll, yaw, throttle;
}commands;

typedef struct raw{
	int x,y,z;
}raw;
typedef struct PID_Consts{
	float P, I, D;
}PID_Consts;

#define LOG_STARTING_SIZE 4096 //262144 // 2^18      32768  2^15

typedef struct logging{

	// Time
	float time_stamp;
	float time_slice;

	// Id
	int packetId;

	gam_t gam; 									// Raw and calculated gyro, accel, and mag values are all in gam_t
	float phi_dot, theta_dot, psi_dot; // gimbal equation values
	float roll_angle, pitch_angle, yaw_heading; // Core quad values (calculated from gam readings) complementary filter

	float x_pos, y_pos, altitude_z;

	float cam_roll, cam_pitch, cam_yaw;

	float pid_P_component, pid_I_component, pid_D_component; // use these generically for any PID that you are testing

	// PID coefficients and errors
	PID_t local_x_PID, local_y_PID, altitude_PID;
	PID_t angle_yaw_PID, angle_roll_PID, angle_pitch_PID;
	PID_t ang_vel_yaw_PID, ang_vel_roll_PID, ang_vel_pitch_PID;

	PID_values local_x_PID_values, local_y_PID_values, altitude_PID_values;
	PID_values angle_yaw_PID_values, angle_roll_PID_values, angle_pitch_PID_values;
	PID_values ang_vel_yaw_PID_values, ang_vel_roll_PID_values, ang_vel_pitch_PID_values;

	// RC commands
	commands commands;

	int motors[4];


}logging;

extern logging logs;

void pwm_init();
void pwm_write_all(int pulseWidth);
void pwm_write_channel(int pulseWidth, int channel);

int read_rec(int channel);
void read_rec_all(int* mixer);

void read_bluetooth_all(int* mixer);

void b_drive_pulse();

void sine_example();

void print_mixer(int* mixer);
int read_kill(int gear);
int read_flap(int flap);
void pwm_kill();

char* format(logging log);
void printLogging();
void updateLog();
void bluetoothTunePID(char instr, gam_t* gam, PID_t* CFpid, PID_t* Gpid);

#endif //_UTIL_H
