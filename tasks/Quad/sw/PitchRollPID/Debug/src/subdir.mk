################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/PID.c \
../src/calibration.c \
../src/commandproc.c \
../src/controllers.c \
../src/conversion.c \
../src/iic_mpu9150_utils.c \
../src/main.c \
../src/platform.c \
../src/stringBuilder.c \
../src/terminal.c \
../src/uart.c \
../src/util.c 

LD_SRCS += \
../src/lscript.ld 

OBJS += \
./src/PID.o \
./src/calibration.o \
./src/commandproc.o \
./src/controllers.o \
./src/conversion.o \
./src/iic_mpu9150_utils.o \
./src/main.o \
./src/platform.o \
./src/stringBuilder.o \
./src/terminal.o \
./src/uart.o \
./src/util.o 

C_DEPS += \
./src/PID.d \
./src/calibration.d \
./src/commandproc.d \
./src/controllers.d \
./src/conversion.d \
./src/iic_mpu9150_utils.d \
./src/main.d \
./src/platform.d \
./src/stringBuilder.d \
./src/terminal.d \
./src/uart.d \
./src/util.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -I../../system_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


