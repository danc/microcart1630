################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/actuator_command_processing.c \
../src/control_algorithm.c \
../src/initialize_components.c \
../src/log_data.c \
../src/main.c \
../src/send_actuator_commands.c \
../src/sensor.c \
../src/sensor_processing.c \
../src/timer.c \
../src/update_gui.c \
../src/user_input.c 

LD_SRCS += \
../src/lscript.ld 

OBJS += \
./src/actuator_command_processing.o \
./src/control_algorithm.o \
./src/initialize_components.o \
./src/log_data.o \
./src/main.o \
./src/send_actuator_commands.o \
./src/sensor.o \
./src/sensor_processing.o \
./src/timer.o \
./src/update_gui.o \
./src/user_input.o 

C_DEPS += \
./src/actuator_command_processing.d \
./src/control_algorithm.d \
./src/initialize_components.d \
./src/log_data.d \
./src/main.d \
./src/send_actuator_commands.d \
./src/sensor.d \
./src/sensor_processing.d \
./src/timer.d \
./src/update_gui.d \
./src/user_input.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -I../../system_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


