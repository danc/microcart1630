/*
 * main.c
 *
 *  Created on: Nov 11, 2015
 *      Author: Amy Seibert
 */
#include <stdio.h>

#include "timer.h"
#include "log_data.h"
#include "initialize_components.h"
#include "user_input.h"
#include "sensor.h"
#include "sensor_processing.h"
#include "control_algorithm.h"
#include "actuator_command_processing.h"
#include "send_actuator_commands.h"
#include "update_gui.h"

int main()
{
	// Structures to be used throughout
	user_input_t user_input_struct = { };
	log_t log_struct = { };
	raw_sensor_t raw_sensor_struct = { };
	sensor_t sensor_struct = { };
	setpoint_t setpoint_struct = { };
	parameter_t parameter_struct = { };
	user_defined_t user_defined_struct = { };
	raw_actuator_t raw_actuator_struct = { };
	actuator_command_t actuator_command_struct = { };
	// TODO add more structs


	// Initialize all required components:
	// Uart, PWM receiver/generator, I2C, Sensor Board
	int init_error = initializeAllComponents();
	if (init_error == -1) {
		printf("ERROR (main): Problem initializing...Goodbye\r\n");
		return init_error;
	}

	// Loops to make sure the quad is responding correctly before starting the control loop
	protection_loops();

	// Main control loop
	while(1)
	{
		// Processing of loop timer at the beginning of the control loop
		timer_start_loop();

		// Get the user input and put it into user_input_struct
		get_user_input(&log_struct, &user_input_struct);

		// Get data from the sensors and put it into raw_sensor_struct
		get_sensors(&log_struct, &user_input_struct, &raw_sensor_struct);

		// Process the sensor data and put it into sensor_struct
		sensor_processing(&log_struct, &raw_sensor_struct, &sensor_struct);

		// Run the control algorithm
		control_algorithm(&log_struct, &sensor_struct, &setpoint_struct, &parameter_struct, &user_defined_struct, &raw_actuator_struct);

		// Process the commands going to the actuators
		actuator_command_processing(&log_struct, &raw_actuator_struct, &actuator_command_struct);

		// send the actuator commands
		send_actuator_commands(&log_struct, &actuator_command_struct);

		// update the GUI
		update_GUI(&log_struct);

		// Log the data collected in this loop
		log_data(&log_struct);

		// Processing of loop timer at the end of the control loop
		timer_end_loop();
	}
	return 0;
}
