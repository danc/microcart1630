/*
 * initialize_components.c
 *
 *  Created on: Nov 12, 2015
 *      Author: Amy Seibert
 */
 
 #include "initialize_components.h"

int protection_loops()
{
	return 0;
}
 
int initializeAllComponents()
{
	// Initialize the controller
	control_algorithm_init();

	// Initialize the loop timer
	timer_init();

    return 0;
}
 
