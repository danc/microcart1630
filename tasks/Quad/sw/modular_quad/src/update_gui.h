/*
 * update_gui.h
 *
 *  Created on: Nov 11, 2015
 *      Author: Amy Seibert
 */

#ifndef UPDATE_GUI_H_
#define UPDATE_GUI_H_

#include "log_data.h"

/**
 * @brief 
 *      Updates the user interface.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @return 
 *      error message
 *
 */
int update_GUI(log_t* log_struct);

#endif /* UPDATE_GUI_H_ */
