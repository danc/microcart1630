/*
 * sensor.h
 *
 *  Created on: Nov 11, 2015
 *      Author: Amy Seibert
 */

#ifndef SENSOR_H_
#define SENSOR_H_

#include "log_data.h"
#include "user_input.h"

/**
 * @brief 
 *      Holds the raw data from the sensors and the timestamp if available
 *
 */
typedef struct raw_sensor_t {
	int acc_x;		// accelerometer x data
	int acc_x_t;	// time of accelerometer x data

	int acc_y;		// accelerometer y data
	int acc_y_t;	// time of accelerometer y data

	int acc_z;		// accelerometer z data
	int acc_z_t;	// time of accelerometer z data


	int gyr_x;		// gyroscope x data
	int gyr_x_t;	// time of gyroscope x data

	int gyr_y;		// gyroscope y data
	int gyr_y_t;	// time of gyroscope y data

	int gyr_z;		// gyroscope z data
	int gyr_z_t;	// time of gyroscope z data
} raw_sensor_t;

/**
 * @brief 
 *      Recieves data from the sensors.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param user_input_struct
 *      structure of the input from the user
 *
 * @param raw_sensor_struct
 *      structure of the raw values from the sensors
 *
 * @return 
 *      error message
 *
 */
int get_sensors(log_t* log_struct, user_input_t* user_input_struct, raw_sensor_t* raw_sensor_struct);

#endif /* SENSOR_TEMPLATE_H_ */
