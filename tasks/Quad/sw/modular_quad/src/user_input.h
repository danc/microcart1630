/*
 * user_input.h
 *
 *  Created on: Nov 11, 2015
 *      Author: Amy Seibert
 */

#ifndef USER_INPUT_H_
#define USER_INPUT_H_

#include "log_data.h"
 
/**
 * @brief 
 *      Holds the data inputted by the user
 *
 */
typedef struct user_input_t {
	float x_position;		// Current x position from the camera system
	float y_position;		// Current y position from the camera system
	float z_position;		// Current z position from the camera system
	float roll;				// Current roll angle from the camera system
	float pitch;			// Current pitch angle from the camera system
	float yaw;				// Current yaw angle from the camera system
} user_input_t;

/**
 * @brief 
 *      Recieves user input to the system.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param user_input_struct
 *      structure of the data inputted by the user
 *
 * @return 
 *      error message
 *
 */
int get_user_input(log_t* log_struct,  user_input_t* user_input_struct);


#endif /* USER_INPUT_H_ */
