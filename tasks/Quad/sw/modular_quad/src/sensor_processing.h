/*
 * sensor_processing.h
 *
 *  Created on: Nov 11, 2015
 *      Author: Amy Seibert
 */

#ifndef SENSOR_PROCESSING_H_
#define SENSOR_PROCESSING_H_

#include "log_data.h"
#include "sensor.h"

/**
 * @brief 
 *      Holds the processed data from the sensors and the timestamp if available
 *
 */
typedef struct sensor_t {
	int acc_x;		// accelerometer x data
	int acc_x_t;	// time of accelerometer x data

	int acc_y;		// accelerometer y data
	int acc_y_t;	// time of accelerometer y data

	int acc_z;		// accelerometer z data
	int acc_z_t;	// time of accelerometer z data


	int gyr_x;		// gyroscope x data
	int gyr_x_t;	// time of gyroscope x data

	int gyr_y;		// gyroscope y data
	int gyr_y_t;	// time of gyroscope y data

	int gyr_z;		// gyroscope z data
	int gyr_z_t;	// time of gyroscope z data
} sensor_t;

/**
 * @brief 
 *      Processes the data from the sensors.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param raw_sensor_struct
 *      structure of the raw data from the sensors
 *
 * @param sensor_struct
 *      structure of the processed data from the sensors
 *
 * @return 
 *      error message
 *
 */
int sensor_processing(log_t* log_struct, raw_sensor_t* raw_sensor_struct, sensor_t* sensor_struct);

#endif /* SENSOR_PROCESSING_H_ */
