/*
 * timer.h
 *
 *  Created on: Feb 24, 2016
 *      Author: Amy Seibert
 */

#ifndef TIMER_H_
#define TIMER_H_

/**
 * @brief
 *      Initializes the items necessary for loop timing.
 *
 * @return
 *      error message
 *
 */
int timer_init();

/**
 * @brief
 *      Does processing of the loop timer at the beginning of the control loop.
 *
 * @return
 *      error message
 *
 */
int timer_start_loop();

/**
 * @brief
 *      Does processing of the loop timer at the end of the control loop.
 *
 * @return
 *      error message
 *
 */
int timer_end_loop();

#endif /* TIMER_H_ */
