/*
 * initialize_components.h
 *
 *  Created on: Nov 12, 2015
 *      Author: Amy Seibert
 */

#ifndef INITALIZE_COMPONENTS_H_
#define INITALIZE_COMPONENTS_H_

#include "timer.h"
#include "control_algorithm.h"

/**
 * @brief 
 *      Runs loops to make sure the quad is responding and in the correct state before starting.
 *
 * @return
 *      error message
 *
 */
int protection_loops();

/**
 * @brief
 *      Initializes the sensors, communication, and anything else that needs
 * initialization.
 *
 * @return 
 *      error message
 *
 */
int initializeAllComponents();

#endif /* INITALIZE_COMPONENTS_H_ */
