/*
 * log_data.h
 *
 *  Created on: Nov 11, 2015
 *      Author: Amy Seibert
 */

#ifndef LOG_DATA_H_
#define LOG_DATA_H_

/**
 * @brief 
 *      Holds the log data to be sent to the ground station. It may hold the 
 * timestamp of when a sensor's data was obtained.
 *
 */
typedef struct log_t {

} log_t;

/**
 * @brief 
 *      Logs the data obtained throughout the controller loop.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @return 
 *      error message
 *
 */
 int log_data(log_t* log_struct);

#endif /* LOG_DATA_H_ */
