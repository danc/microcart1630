/**
 * Converts Aero 4 channel signals to PWM signals
 * Aero channels are defined above
 */
void Aero_to_PWMS(int PWMs[], int aero[]) {
	// Throttle, pitch, roll, yaw as a percentage of their max - Range 0.0 - 100.0
	float throttle_100 = (aero[THROTTLE] - THROTTLE_MIN) / (THROTTLE_RANGE*1.0);
	float pitch_100    = (aero[PITCH]    - PITCH_MIN)    / (PITCH_RANGE*1.0);
	float roll_100     = (aero[ROLL]     - ROLL_MIN)     / (ROLL_RANGE*1.0);
	float yaw_100      = (aero[YAW]      - YAW_MIN)      / (YAW_RANGE*1.0);

	// This adds a +/- 300 ms range bias for the throttle
	int throttle_base = BASE + (int) 60000 * (throttle_100 - .5);
	// This adds a +/- 200 ms range bias for the pitch
	int pitch_base    =        (int) 60000 * (pitch_100    - .5);
	// This adds a +/- 200 ms range bias for the roll
	int roll_base     =        (int) 60000 * (roll_100     - .5);
	// This adds a +/- 75 ms range bias for the yaw
	int yaw_base      =        (int) 15000 * (yaw_100      - .5);

	int pwm0, pwm1, pwm2, pwm3;

	pwm1 = throttle_base + pitch_base/2 - roll_base/2 + yaw_base + motor1_bias;
	pwm3 = throttle_base + pitch_base/2 + roll_base/2 - yaw_base + motor3_bias;
	pwm0 = throttle_base - pitch_base/2 - roll_base/2 - yaw_base + motor0_bias;
	pwm2 = throttle_base - pitch_base/2 + roll_base/2 + yaw_base + motor2_bias;

	/**
	 * Boundary checks:
	 *
	 *  #define min 100000
	 *  #define max 200000
	 */

	if(pwm0 < min)
		pwm0 = min;
	else if(pwm0 > max)
		pwm0 = max;

	if(pwm1 < min)
		pwm1 = min;
	else if(pwm1 > max)
		pwm1 = max;

	if(pwm2 < min)
		pwm2 = min;
	else if(pwm2 > max)
		pwm2 = max;

	if(pwm3 < min)
		pwm3 = min;
	else if(pwm3 > max)
		pwm3 = max;

	PWMs[0] = pwm0;
	PWMs[1] = pwm1;
	PWMs[2] = pwm2;
	PWMs[3] = pwm3;

	// the array PWMs is then written directly to the PWM hardware registers
	// the PWMs are in units of clock cycles, not percentage duty cycle
	// use pwm/222,222 to get the duty cycle. the freq is 450 Hz on a 100MHz clock

}

